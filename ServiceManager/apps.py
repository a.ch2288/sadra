from django.apps import AppConfig


class ServicemanagerConfig(AppConfig):
    name = 'ServiceManager'
