from django.db import models

# Create your models here.

class Service_Primary_Information(models.Model):

    slug = models.CharField(primary_key= True,max_length=255)
    persian_name = models.TextField()
    source_link = models.CharField(max_length=255)
    satisfaction = models.CharField(max_length = 6,default='۸۶.۹۸')

    def __str__(self):
        return self.slug

    def get_source_link_template():
        source_link_template = 'https://ostadkar.ir/_city/order/_slug'
        return source_link_template

    def get_wizard_link():
        source_link = 'https://ostadkar.ir/wizard/order'
        return source_link
