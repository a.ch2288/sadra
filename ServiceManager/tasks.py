from celery import shared_task
from .ModelManager import ServiceModelManager

service_manager = ServiceModelManager()


@shared_task
def update_all():
    service_manager.update_all()