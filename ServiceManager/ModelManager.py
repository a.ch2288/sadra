from .models import Service_Primary_Information

from ForeignDBConnection.QueryManager import Query
import pandas as pd
import os

class QueryManager(Query):
    pass

class ServiceModelManager:

    def __init__(self):

        self.query = QueryManager(bulk_size = 5000
            ,app_name = 'ServicePrimaryInformationManager'
            ,app_path = os.path.dirname(__file__))

        self.ostadkar_db_names = {
            'primary_data' : 'product',
            'satisfaction_data' : 'data'
        }

        self.queries_file_name = {
            'primary_data' : 'ServicePrimaryData.sql',
            'satisfaction_data' : 'ServiceSatisfactionData.sql'
        }

        self.source_link_template = Service_Primary_Information.get_source_link_template()

    def update_all(self):
        self.update_primary_data()
        self.update_satisfaction_data()
    
    def update_primary_data(self):

        query_file_name = self.queries_file_name['primary_data']
        db_name = self.ostadkar_db_names['primary_data']

        self.query.read_file(query_file_name=query_file_name)
        self.query.create_connection(
            product_name = 'ostadkar',
            connection_type = '',
            db_name = db_name)

        data = self.query.read_data()

        for i,row in data.iterrows():
            slug = row['slug']
            persian_name = row['persian_name']
            source_link = self.source_link_template.replace('_slug',slug)

            service = Service_Primary_Information.objects.filter(slug = slug)
            if len(service) == 0 :
                service = Service_Primary_Information(
                    slug = slug,
                    persian_name = persian_name,
                    source_link = source_link
                )
            else:
                service = service[0]
                service.persian_name = persian_name
                service.source_link = source_link
            
            service.save()
        
        print('- {} ({}) services has been added.'.format(data.shape[0],data.shape[0]))

        ostadkar = Service_Primary_Information.objects.filter(slug = 'ostadkar')
        if len(ostadkar) == 0:
            ostadkar = Service_Primary_Information(
                slug = 'ostadkar'
                ,persian_name = 'استادکار'
                ,source_link = Service_Primary_Information.get_wizard_link()
                )
            ostadkar.save()
        else:
            ostadkar.update(
                slug = 'ostadkar'
                ,persian_name = 'استادکار'
                ,source_link = Service_Primary_Information.get_wizard_link()
            )
    
    def update_satisfaction_data(self):

        db_name = self.ostadkar_db_names['satisfaction_data']
        query_file_name = self.queries_file_name['satisfaction_data']

        self.query.read_file(query_file_name=query_file_name)
        self.query.create_connection(
            product_name = 'ostadkar',
            connection_type = '',
            db_name = db_name)

        data = self.query.read_data()

        for i,row in data.iterrows():
            slug = row['slug']
            satisfaction = str(row['satisfaction'])[0:5]
            satisfaction = self.en_to_fa(satisfaction)
            service = Service_Primary_Information.objects.filter(slug = slug)
            if len(service) == 1:
                service = service[0]
                service.satisfaction = satisfaction
                service.save()
            else:
                print("- first please update services' primary data.")

        print('- satisfaction of {} ({}) services had been added.'.format(data.shape[0],data.shape[0]))

        # in ghalate! vali velesh kon :))
        count_services_with_satisfaction = 0
        sum_satisfaction = 0
        for i,row in data.iterrows():
            if pd.notnull(row['satisfaction']):
                sum_satisfaction = sum_satisfaction + row['satisfaction']
                count_services_with_satisfaction += 1
        
        avg_satisfication = sum_satisfaction/count_services_with_satisfaction

        ostadkar = Service_Primary_Information.objects.filter(slug = 'ostadkar')
        if len(ostadkar) != 0:
            ostadkar = ostadkar[0]
            satisfaction = str(avg_satisfication)[0:5]
            satisfaction = self.en_to_fa(satisfaction)
            ostadkar.satisfaction = satisfaction
            ostadkar.save()
            print('- average of {} services had been calculated.'.format(count_services_with_satisfaction))

    def en_to_fa(self,satisfaction_en):


        number_translator = {
            '1' : '۱',
            '2' : '۲',
            '3' : '۳',
            '4' : '۴',
            '5' : '۵',
            '6' : '۶',
            '7' : '۷',
            '8' : '۸',
            '9' : '۹',
            '0' : '۰',
            '.' : '.'
        }
        
        satisfaction_fa = ''
        for c in satisfaction_en:
            satisfaction_fa = satisfaction_fa + number_translator[c]
        
        return satisfaction_fa
     

def check_service_slug(service_slug):

    service = Service_Primary_Information.objects.filter(slug = service_slug)
    if len(service) == 1:
        return True
    
    service_slug_identifier_arguments = ['LastOrderService','empty','general']
    for argument in service_slug_identifier_arguments:
        if argument == service_slug:
            return True
    
    return False

def get_service_info(service_slug,customer_id = None):

    service = Service_Primary_Information.objects.filter(slug = service_slug)
    if len(service) == 1 :
        info = list(service.values())[0]

    elif service_slug == 'LastOrderService' and customer_id != None:
        from CustomerManager.ModelManager import get_customer_info
        service_slug = get_customer_info(customer_id = customer_id)['last_order_service']
        info = get_service_info(service_slug = service_slug)
    elif service_slug == 'LastOrderService' and customer_id == None:
        return False
    
    # try:
    #     return info
    # except:
    #     print(len(service) == 1)
    #     print(service == 'LastOrderService' and customer_id != None)
    #     print(service == 'LastOrderService' and customer_id == None)
    #     print(service_slug)
    #     print(customer_id)
    #     return info

    return info


