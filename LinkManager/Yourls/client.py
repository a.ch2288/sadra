# import urllib
# import urllib2
from requests import Session
import json


class YourlsError(Exception):
    '''Base exception for all Yourls exceptions'''
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message


class YourlsOperationError(YourlsError):
    '''Error during URL operations'''
    def __init__(self, url, message):
        self.url = url
        self.message = message

    def __str__(self):
        return repr('Error with url \'%s\' - %s' % (self.url, self.message))


class YourlsClient:
    def __init__(self, apiurl, username=None, password=None, token=None):
        """The use of a username/password combo or a signature token is required

        :param apiurl: The location of the api php file
        :param username: The username to login with (not needed with signature token)
        :param password: The password to login with (not needed with signature token)
        :param token: The signature token to use (not needed with username/password combo)
        :throws: YourlsError for incorrent parameters

        """
        self.data_format = 'json'
        self.session = Session()
        if not apiurl:
            raise YourlsError("An api url is required")
        self.apiurl = apiurl

        if not username or not password:
            if not token:
                raise YourlsError("username and password or signature token are required")
            else:
                print("TokenBased")
                self.std_args = {'signature': token}
        else:
            print("UsernameBased")
            self.username = username
            self.password = password
            self.std_args = {'username': self.username, 'password': self.password}
        self.std_args.update({'format': 'json'})

    def _send_request(self, args):
        """Encapsulates the actual sending of a request to a YOURLS instance

        :param args: The arguments to send to YOURLS

        """
        # urlargs = urllib.urlencode(self._make_args(args))
        # req = urllib2.Request(self.apiurl)
        # req.add_data(urlargs)
        # r = urllib2.urlopen(req)
        # data = r.read()
        param = self._make_args(args)
        r = self.session.get(self.apiurl, params=param)
        data = r.text
        return data

    def _make_args(self, new_args):
        """Convenience method for putting args into the proper format

        :param new_args: Dictionary containing the args to pass on

        """
        data = self.std_args.copy()
        data.update(new_args)
        return data

    def _base_request(self, args, url):
        """Encapsulates common code and error handling for the access methods

        :param args: The arguments to send to YOURLS
        :param url: The url (short or long) arg being used in the request
        :raises: YourlsOperationError

        """
        try:
            data = json.loads(self._send_request(args))
        except Exception as error:
            raise YourlsOperationError(url, str(error))

        if 'errorCode' in data:
            raise YourlsOperationError(url, data['message'])

        return data

    def shorten(self, url, custom = None, title = None):
        """Request a shortened URL from YOURLS with an optional keyword request

        :param url: The URL to shorten
        :type url: str
        :param custom: The custom keyword to request
        :type custom: str
        :param title: Use the given title instead of download it from the URL, this will increase performances
        :type title: str
        :returns: str -- The short URL
        :raises: YourlsOperationError

        """
        args = {'action':'shorturl','url':url}

        if custom:
            args['keyword'] = custom

        if title:
            args['title'] = title

        # shorten
        raw_data = self._base_request(args, url)

        # parse result
        if raw_data['status'] == 'fail' and raw_data['code'] == 'error:keyword':
            raise YourlsOperationError(url, raw_data['message'])

        if not 'shorturl' in raw_data:
            raise YourlsOperationError(url, 'Unknown error: %s' % raw_data['message'])

        return raw_data['shorturl']

    def expand(self, shorturl):
        """Expand a shortened URL to its original form

        :param shorturl: The URL to expand
        :returns: str -- The expanded URL
        :raises: YourlsOperationError

        """
        args = {'action' : 'expand', 'shorturl' : shorturl, 'format' : 'json'}

        raw_data = self._base_request(args, shorturl)

        if not 'longurl' in raw_data:
            raise YourlsOperationError(shorturl, raw_data['message'])

        return raw_data['longurl']

    def get_url_stats(self, shorturl):
        """Get statistics about a shortened URL

        :param shorturl: The URL to expand
        :returns: a list of stuff - FIXME, this isn't complete
        :raises: YourlsOperationError

        """

        args = {'action' : 'url-stats', 'shorturl' : shorturl, 'format' : 'json'}

        raw_data = self._base_request(args, shorturl)

        if raw_data['statusCode'] != 200:
            raise YourlsOperationError(shorturl, raw_data['message'])

        return raw_data['link']

    def get_short_link_clicks(self, shorturl):
        return self.get_url_stats(shorturl)['clicks']
