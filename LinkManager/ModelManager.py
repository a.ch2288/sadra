import os
import pandas as pd

from .models import *
from ScenarioManager.models import Scenario_Structure
from ForeignDBConnection.QueryManager import GoogleSheet

def check_link(link_type,basic_utm_id,message_utm,personal_utm):

    link_type,short_shortener = link_type.split('$')[0],link_type.split('$')[1:]
    if link_type == 'service_based' or link_type == 'empty' or Source_Link_Info.objects.filter(name = link_type).count():
        return True
    else:
        return False

class LinkModelManager:

    def __init__(self):

        self.file_name = ''

        self.Basic_UTM_Parameters_sheet_info = {
            'google_sheet_id' : '1DUiP--yPVsDp_nNI5FEzqKqZqKVNKlWTF63GQE2lsmY',
            'sheet_name' : 'UTM_Parameters',
        }

        self.link_manager_sheets_info = {
            'google_sheet_id' : '1DUiP--yPVsDp_nNI5FEzqKqZqKVNKlWTF63GQE2lsmY',
            'sheets_name' : {
                'basic_UTMs' : ['UTM_Parameters'],
                'source_links' : ['Landing_Pages']
                },
            'target_models' : {
                'basic_UTMs' : Basic_UTM_Parameters,
                'source_links' : Source_Link_Info
                },
            }

        self.foreign_data = {}
    
    def update_all(self):
        
        self.read_info()
        self.update_all_sheets()
        self.update_message_utm_parameters()
    
    def update_message_utm_parameters(self):

        messages_utm_parameters = Scenario_Structure.objects.all().values(
            'name',

            'scenario_id',
            'sub_scenario_part',
            'phase',
            'sub_phase_part',
            'group',
            'sub_group_part',
            'message_type',

            'message_service_slug',
            'message_promotion_code',
            'message_link_type'
            )
        
        created_counter = 0
        for message_utm_parameters in messages_utm_parameters:
            
            obj, created = Message_UTM_Parameters.objects.update_or_create(
                name = message_utm_parameters['name'],
                defaults = message_utm_parameters)
            created_counter += int(created)

        print('- {} ({}) message_utm_parameters had been updated.'.format(
            len(messages_utm_parameters),
            created_counter))   
        
    def update_basic_utm_parameters(self):

        google_sheet = GoogleSheet(
            google_sheet_id = self.Basic_UTM_Parameters_sheet_info['google_sheet_id'],
            sheet_name = self.Basic_UTM_Parameters_sheet_info['sheet_name']
            )
        model_pk,model_feilds_type = Basic_UTM_Parameters.get_fields_type()
        data = google_sheet.get_data(
            model_feilds_type = model_feilds_type,
            fillna = False
            )


        created_counter = 0
        counter = 0
        for i,row in data.iterrows():
            
            utm = Basic_UTM_Parameters(**row)
            name = utm.create_name()
            created_counter += int(0 == Basic_UTM_Parameters.objects.filter(name = name).count())
            utm.save()
            counter += 1
                

        print(f'- {counter} ({created_counter}) weekly_scenarios_structure had been update.')
    
    def read_info(self,targets = None):

        if targets is None :
            targets = ['basic_UTMs','source_links']
        else:
            if type(targets) is not list:
                targets = [targets]

        google_sheet_id = self.link_manager_sheets_info['google_sheet_id']

        for target in targets:
            sheets_name = self.link_manager_sheets_info['sheets_name'][target]
            target_model = self.link_manager_sheets_info['target_models'][target]
        
            for sheet_name in sheets_name:

                google_sheet = GoogleSheet(
                    google_sheet_id = google_sheet_id,
                    sheet_name = sheet_name
                    )

                model_pk,model_feilds_type = target_model.get_fields_type()

                self.foreign_data[sheet_name] = google_sheet.get_data(
                    model_feilds_type = model_feilds_type,
                    fillna = True
                    )

                if self.foreign_data[sheet_name].shape[0] == 0 :
                    print('Warning : data sheet {} is empty.'.format(sheet_name))
                    
                else:
                    print(f'- reading data in {sheet_name} sheet had been completed.')

    def update_all_sheets(self,targets = None):

        # Scenario.objects.all().delete()

        if len(self.foreign_data) == 0:
            self.read_info()
            if len(self.foreign_data) == 0 :
                pass
                # raise error
        
        if targets is None :
            targets = ['basic_UTMs','source_links']
        else:
            if type(targets) is not list:
                targets = [targets]
        
        for target in targets:

            target_model = self.link_manager_sheets_info['target_models'][target]
            sheets_name_list = self.link_manager_sheets_info['sheets_name'][target]
            
            for sheet_name in sheets_name_list:
                data = self.foreign_data[sheet_name]
                created_counter = 0
                for i,row in data.iterrows():

                    obj, created = target_model.objects.update_or_create(name = row['name'],defaults = row.to_dict())
                    created_counter += int(created)
                
                print('- {} ({}) {}s had been updated.'.format(data.shape[0],created_counter,sheet_name))


if __name__ == "__main__":
    manager = LinkModelManager()
