from celery import shared_task
from .ModelManager import LinkModelManager
from .Functions import update_short_links_clicked_at_with_time_limit
from datetime import datetime

check = True




@shared_task
def update_model():
    link_manager = LinkModelManager()
    link_manager.update_all()

@shared_task
def update_clicked_link(time_limit):
    global check

    if 20 < datetime.now().hour or datetime.now().hour < 3:
        print('rest time! :)')
        check = True
    else:

        if check == True:
            time_limit = 3*24*60
            check = False

        if time_limit is not None:
            update_short_links_clicked_at_with_time_limit(
                time_limit = time_limit
                )
        else:
            update_short_links_clicked_at_with_time_limit(
                time_limit = 1*24*60
                )
