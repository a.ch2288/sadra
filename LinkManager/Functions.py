from .models import *
from .Yourls.client import YourlsClient
from django.db.models import Q
import random, string
import datetime as DT
import time

api_url = 'http://ostd.ir/yourls-api.php'
sig = '05e2685fc7'
yourls = YourlsClient(api_url, token='05e2685fc7')

def string_random_generation(N=6):
    return ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase, k=N))
    
def get_short_link(long_url):
    short_link = None
    while short_link == None:
        try:
            c = string_random_generation(6)
            link = yourls.shorten(url=long_url, custom=c)

            # while int(get_link_count(link)) != 0:
            #     c = string_random_generation(6)
            #     link = yourls.shorten(url=long_url, custom=c)
            #     print('short link Error - {}'.format(link))
            #     time.sleep(20)

            short_link = link
        except Exception as e:
            print(e)
            time.sleep(20)
    
    return short_link

def get_link_count(short_link):

    click_count = None
    # while click_count == None:
    #     try:
    #         click_count = int(yourls.get_short_link_clicks(short_link))
    #     except Exception as e:
    #         print('--- {}'.format(e))
    #         time.sleep(20)

    try:
        click_count = int(yourls.get_short_link_clicks(short_link))
    except:
        print(f'--- Error in get_link_count:{short_link}')
        click_count = 0


    return click_count


class LinkCreator:
    def __init__(self,bulk_size):
        self.bulk_size = bulk_size
        self.short_links_list = []
        self.now_str = DT.datetime.strftime(DT.datetime.now(),'%Y-%m-%d %H:%M:%S')

    def create_short_link(self,
        message_name,
        link_type,
        basic_utm_id,
        message_utm,
        personal_utm,
        receiver_info,
        service_info,
        bulk_size = 1,
        date = True
        ):

        link_type,short_shortener = link_type.split('$')[0],link_type.split('$')[1:]

        if link_type == 'service_based':
            raw_url = service_info['source_link']
        else:
            raw_url = Source_Link_Info.objects.get(name = link_type).raw_url

        url = raw_url.replace('_city',receiver_info['city'])
        
        if len(short_shortener) == 0:
        
            if basic_utm_id != 'empty':
                
                basic_utm_parameters = Basic_UTM_Parameters.objects.get(name = basic_utm_id)
                url = basic_utm_parameters.insert_parameters(url = url)

            if message_utm != 'empty':
                if message_utm == 'auto_generation':
                    message_utm_parameters = Message_UTM_Parameters.objects.get(name = message_name)
                    url = message_utm_parameters.insert_parameters(url = url)
                else:
                    pass

            if personal_utm == True:

                if not '?' in url:
                    url += '?'

                if not 'mobile' in url:
                    url += '&mobile={}'.format(receiver_info['mobile'])


            if date:
                if not '?' in url:
                    url += '?'
                url += '&date={}'.format(self.now_str)
            url = url.replace(' ','_')
            short_link = get_short_link(long_url = url)
            name = '{}.{}.{}'.format(message_name,receiver_info['mobile'],self.now_str)
            short_link_info = Short_Link_Info(
                name = name,
                long_link = url,
                short_link = short_link
                )
            self.short_links_list.append(short_link_info)
        
        else:
            
            url = url.replace(' ','_')
            short_link = url
            name = '{}.{}.{}'.format(message_name,'public',DT.datetime.now().date())
            short_link_info = Short_Link_Info(
                name = name,
                long_link = url,
                short_link = short_link
                )
            if len(self.short_links_list) == 0 :
                self.short_links_list.append(short_link_info)

        return short_link 
            
    def archive_short_link(self):
        count = len(self.short_links_list)
        if count == 1:
            short_link = self.short_links_list[0]
            short_link.save()
        else:
            Short_Link_Info.objects.bulk_create(self.short_links_list)
        print(f'-- {count} short link has been archive.')
        self.short_links_list = []

        

def update_short_links_clicked_at_with_time_limit(message_name = None,time_limit = 10*24*60):
    short_links_info = Short_Link_Info.objects.filter(clicked_at__isnull = True)
    if time_limit is not None:
        date_limit = DT.datetime.now() - DT.timedelta(minutes=time_limit)
        q0 = Q(created_at__gte = date_limit)
        short_links_info = short_links_info.filter(q0)
    
    if message_name is not None:
        q1 = Q(name__startswith = message_name)
        short_links_info = short_links_info.filter(q1)
    
    print(f'-- updating of {short_links_info.count()} short links has been started.')
    clicked_short_links_list = []
    counter = 0
    for short_link_info in short_links_info:
        short_link = short_link_info.short_link
        link_count = get_link_count(short_link = short_link)
        if link_count != 0:
            short_link_info.clicked_at = DT.datetime.now()
            clicked_short_links_list.append(short_link_info)
        
        counter += 1
        if counter == 1000:
            counter = 0
            print(f'--- updating of 1000 short links has been completed.')
        
    Short_Link_Info.objects.bulk_update(clicked_short_links_list,['clicked_at'])
    print(f'-- {len(clicked_short_links_list)} short links has been clicked.')


if __name__ == "__main__":
    update_short_links_clicked_at_with_time_limit()