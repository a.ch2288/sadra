from django.db import models
from django.utils import timezone

# Create your models here.

class Source_Link_Info(models.Model):

    name = models.CharField(primary_key=True,max_length=255)
    raw_url = models.TextField(max_length=1000)

    def get_fields_type():
        
        pk = 'name'

        fields_type = {
            'name' : 'str',
            'raw_url' : 'str'

            }

        return pk,fields_type

class Short_Link_Info(models.Model):

    name = models.CharField(primary_key=True,max_length=255)
    long_link = models.TextField(max_length=500,null = True)
    short_link = models.CharField(max_length=500,null = True)
    clicked_at = models.DateTimeField(null = True)
    created_at = models.DateTimeField(default=timezone.now)

class Basic_UTM_Parameters(models.Model):

    name = models.CharField(primary_key=True,max_length=200)
    
    utm_source = models.CharField(max_length=200, default='SadRa')
    utm_medium = models.CharField(max_length=200, default='sms')
    utm_campaign = models.CharField(max_length=200, default='retention')
    utm_term = models.CharField(max_length=200, default='empty')
    utm_content = models.CharField(max_length=200, default='empty')

    def get_fields_type():
        
        pk = 'name'

        fields_type = {
            'name' : 'str',
            'utm_source' : 'str',
            'utm_medium' : 'str',
            'utm_campaign' : 'str',
            'utm_term' : 'str',
            'utm_content' : 'str',

            }

        return pk,fields_type

    def create_name(self):
        if self.name == None or self.name == 'empty' or self.name == '':
            name = '.'.join(
                [self.utm_source
                ,self.utm_medium
                ,self.utm_campaign
                ,self.utm_term
                ,self.utm_content]
                )
            self.name = name
        return self.name

    def insert_parameters(self,url):

        if not '?' in url:
            url += '?'

        if self.utm_source != 'empty' and not 'utm_source' in url:
            url += '&utm_source={}'.format(self.utm_source)
        
        if self.utm_medium != 'empty' and not 'utm_medium' in url:
            url += '&utm_medium={}'.format(self.utm_medium)
        
        if self.utm_campaign != 'empty' and not 'utm_campaign' in url:
            url += '&utm_campaign={}'.format(self.utm_campaign)
        
        if self.utm_term != 'empty' and not 'utm_term' in url:
            url += '&utm_term={}'.format(self.utm_term)

        if self.utm_content != 'empty' and not 'utm_content' in url:
            url += '&utm_content={}'.format(self.utm_content)
        
        return url

class Message_UTM_Parameters(models.Model):

    name = models.CharField(primary_key=True,max_length=200)

    scenario_id = models.CharField(max_length=255,null=True)
    sub_scenario_part = models.SmallIntegerField(default= 1)
    phase = models.SmallIntegerField(null= True,default= 1)
    sub_phase_part = models.SmallIntegerField(default= 1)
    group = models.SmallIntegerField(null= True,default= 1)
    sub_group_part = models.SmallIntegerField(default= 1)
    message_type = models.SmallIntegerField(default= 0)

    message_service_slug = models.CharField(max_length=100, null=True)
    message_promotion_code = models.CharField(max_length=100, null=True)
    message_link_type = models.CharField(default= 'empty',max_length=255, null=True)

    def get_fields_type():
        
        pk = 'name'

        fields_type = {
            'name' : 'str',

            'scenario_id' : 'str',
            'sub_scenario_part' : 'int',
            'phase' : 'int',
            'sub_phase_part' : 'int',
            'group' : 'int',
            'sub_group_part' : 'int',
            'message_type' : 'int',

            'message_promotion_code' : 'str',
            'message_service_slug' : 'str',
            
            'message_link_type' : 'str'

            }

        return pk,fields_type

    def create_name(self):
        
        name = '.'.join(
            [str(self.scenario_id)
            ,str(self.sub_scenario_part)
            ,str(self.phase)
            ,str(self.sub_phase_part)
            ,str(self.group)
            ,str(self.sub_group_part)
            ,str(self.message_type)]
            )
        
        self.name = name
        return name

    def insert_parameters(self,url):

        if not '?' in url:
            url += '?'

        if self.message_service_slug != 'empty' and not 'message_service_slug' in url:
            url += '&message_service_slug={}'.format(self.message_service_slug)

        if self.message_promotion_code != 'empty' and not 'message_promotion_code' in url:
            url += '&message_promotion_code={}'.format(self.message_promotion_code)

        if self.scenario_id != 'empty' and not '&scenario_id=' in url:
            url += '&scenario_id={}'.format(self.scenario_id)

        if self.sub_scenario_part != 'empty' and not 'sub_scenario_part' in url:
            url += '&sub_scenario_part={}'.format(self.sub_scenario_part)

        if self.phase != 'empty' and not '&phase=' in url:
            url += '&phase={}'.format(self.phase)

        if self.sub_phase_part != 'empty' and not 'sub_phase_part' in url:
            url += '&sub_phase_part={}'.format(self.sub_phase_part)

        if self.group != 'empty' and not '&group=' in url:
            url += '&group={}'.format(self.group)

        if self.sub_group_part != 'empty' and not 'sub_group_part' in url:
            url += '&sub_group_part={}'.format(self.sub_group_part)
        
        if self.message_type != 'empty' and not 'message_type' in url:
            url += '&message_type={}'.format(self.message_type)

        return url
