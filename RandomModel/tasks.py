from celery import shared_task
from .ModelManager import update_all


@shared_task
def update_all_customers_informations():
    update_all()