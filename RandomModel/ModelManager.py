from .models import Segment,Customer_Information
from CustomerManager.models import Customer_Identification_Information,Customer_Login_Information
from CustomerManager.ModelManager import read_customers_list_with_time_limit
import random
import datetime

def dividing_list(primary_list,parts_len = None,parts_count = None ):

    random.shuffle(primary_list)
    primary_list_len = len(primary_list)

    if parts_count == 0 or primary_list_len == 0:
        return [[]]

    if not parts_len:
        parts_len = int(primary_list_len/parts_count) + int(primary_list_len%parts_count != 0)
    

    divided_list = [primary_list[i:i+parts_len] for i in range(0,primary_list_len,parts_len)]
    
    
    return (divided_list)

def create_segments(count):
    count = int(count)
    for i in range(count):
        name = f'RandomSegment{i}'
        Segment(name = name).save()


def initial_insertion():
    Customer_Information.objects.all().delete()
    
    segments = list(Segment.objects.all())
    random.shuffle(segments)

    time_limit= datetime.datetime.now() - datetime.timedelta(days=6*30)

    target_customers_mobile_list = list(Customer_Login_Information.objects.exclude(last_login__gte = time_limit).values_list('customer_id',flat=True))

    divided_target_customers_mobile_list = dividing_list(target_customers_mobile_list,parts_count=len(segments))

    for i in range(len(segments)):
        target_customers_mobile_list = divided_target_customers_mobile_list[i]
        segment = segments[i]

        print(f'- insertion of {len(target_customers_mobile_list)} customer has been started.({segment.name})')

        customer_infotmation_list = []
        
        for mobile in target_customers_mobile_list:

            customer_infotmation_list.append(Customer_Information(mobile = mobile,segment = segment))
            
        
        print(f'- {len(customer_infotmation_list)} customer has been inserted.({segment.name})')
        Customer_Information.objects.bulk_create(customer_infotmation_list)

def update_all():

    active_customers_mobile_list = read_customers_list_with_time_limit(time_limit=6*30*24*60,time_limit_on='last_login')

    Customer_Information.objects.filter(mobile__in = active_customers_mobile_list).delete()

    print('- RandomModel has been updated.')


