from .models import Segment,Customer_Information
from CustomerManager.models import Customer_Identification_Information,Customer_Login_Information
from ForeignDBConnection.QueryManager import Query

import random
import datetime
import os


def dividing_list(primary_list,parts_len = None,parts_count = None ):

    random.shuffle(primary_list)
    primary_list_len = len(primary_list)

    if parts_count == 0 or primary_list_len == 0:
        return [[]]

    if not parts_len:
        parts_len = int(primary_list_len/parts_count) + int(primary_list_len%parts_count != 0)
    

    divided_list = [primary_list[i:i+parts_len] for i in range(0,primary_list_len,parts_len)]
    
    
    return (divided_list)

def create_segments():
    segments_names = ['install_app_test','install_app_base','install_app_target']
    for segment_name in segments_names:
        Segment(name = segment_name).save()
    
    print('- Lazarus Segments have been updated.')

def Initial_Insertion():

    query = Query(bulk_size = 5000
            ,app_name = 'Initial_Insertion'
            ,app_path = os.path.dirname(__file__))

    Customer_Information.objects.all().delete()
    print('Customer_Information have been cleaned.')

    query_file_name = 'all_Customers.sql'
    query.read_file(query_file_name=query_file_name)
    query.create_connection(
        product_name = 'local',
        connection_type = '',
        db_name = 'sadradna_01')
    
    data = query.read_data()

    a1 = [
        '2019-05',
        '2019-06'
        ]
    a2 = [
        '2019-07',
        '2019-08',
        '2019-09',
        '2019-10',
        '2019-11',
        '2019-12',
        '2020-01',
        '2020-02',
        '2020-03',
        '2020-04',
        '2020-05',
        '2020-06'
        ]
    a3 = [
        '2020-07',
        '2020-08',
        '2020-09',
        '2020-10',
        '2020-11',
        '2020-12',
        '2021-01'
        ]
    
    segments_info = {'install_app_test':a1,'install_app_base':a2,'install_app_target':a3}

    for segment_name in segments_info:
        print(f'- insetion customers to segement {segment_name} has been started.')
        df = data[data['last_login'].isin(segments_info[segment_name])]
        customer_list = []
        counter = 0
        for i,row in df.iterrows():

            ci = Customer_Information(segment_id = segment_name,mobile = row['mobile'])
            customer_list.append(ci)
            counter += 1
        
        Customer_Information.objects.bulk_create(customer_list)
        print(f'- {counter} customers have been inseted in {segment_name}.')

def Rewrite_All_Segments():
    segments = Segment.objects.all()
    print('Segments distribution befor rewriting.')
    for segment in segments:
        print('-',segment.name,':',Customer_Information.objects.filter(segment = segment).count())
    Customer_Information.objects.all().delete()
    print('All segments have been cleaned.')
    Initial_Insertion()