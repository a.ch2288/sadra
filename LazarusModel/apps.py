from django.apps import AppConfig


class LazarusmodelConfig(AppConfig):
    name = 'LazarusModel'
