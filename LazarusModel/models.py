from django.db import models
from django.utils import timezone

# Create your models here.

class Segment(models.Model):
    name = models.CharField(primary_key=True,max_length=20)
    created_at = models.DateTimeField(default=timezone.now , null = True)

    def __str__(self):
        return self.name
    
    def get_fields_type():
        
        pk = 'name'

        fields_type = {
            'name' : 'str',
            'submitted_time_limit' : 'int64',
            }

        return pk,fields_type

class Customer_Information(models.Model):
    
    segment = models.ForeignKey(Segment, on_delete = models.CASCADE,null = True)
    mobile = models.CharField(primary_key=True,max_length=11,default='')
    created_at = models.DateTimeField(default=timezone.now , null = True)