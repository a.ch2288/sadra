-- All Customer RFM Information (sale DB)
with
customers_age as (
    select cii.mobile,
           substring(date_trunc('month', case
                                             when cli.last_login is not null then last_login
                                             else cii.created_at end)::text FROM 1 FOR 7)::text as last_login,
           case
               when cli.last_login is not null then last_login
               else cii.created_at end                                                          as login_date

    from "CustomerManager_customer_identification_information" as cii
             left join "CustomerManager_customer_login_information" as cli on cli.customer_id = cii.mobile
)
select
    mobile,
    last_login
from customers_age as ca
where login_date < '2021-01-23'
order by 2 desc