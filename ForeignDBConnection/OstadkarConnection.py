import os
import psycopg2
from sshtunnel import SSHTunnelForwarder
from configparser import ConfigParser
from threading import Thread
import time


def config(section , filename='OstadkarConnection.ini'):
    file_path = os.path.join(os.path.dirname(__file__),'files')
    file_path = os.path.join(file_path,filename)
    parser = ConfigParser()
    parser.read(file_path)
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            if param[1][0] == '(' and param[1][-1] == ')':
                p = param[1][1:-1].split(',')
                p[1] = int(p[1])
                p = tuple(p)
            else:
                p = param[1]
            db[param[0]] = p
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db

class OstadkarRemoteDB:

    def __init__(self,db_name,connection_type = None):

        self.expiry_time = 50
        self.conn = None
        self.tunnel = None
        self.r = False

        # action_thread = Thread(target = self.create_db_connection(db_name,connection_type))
        # action_thread.start()
        # action_thread.join(timeout=20)

        if self.get_host_name() == 'marketing':
            self.create_db_connection(db_name,connection_type)
        elif connection_type == 'ssh' or db_name == 'gateway':
            print('Conncetion to Ostadkar DB had not been created! I will check SSHtunnel and try again.')
            self.stop_ssh_tunnel()
            self.conn = None
            self.tunnel = None
            t = Thread(target = self.run_server) 
            t.start()
            time.sleep(20)
            self.create_db_connection(db_name,'ssh')
        else:
            print('you have to connect to data db.')
            self.create_db_connection('data','')
        
        if self.r == False :
            self.delete()

    def get_host_name(self):
        import socket
        hostname = socket.gethostname()
        # ip_address = socket.gethostbyname(hostname)
        # print(f"Hostname: {hostname}")
        # print(f"IP Address: {ip_address}")
        return(str(hostname))
                
    def run_server(self):
        
        try:
            params = config(section='OstadkarOperationSSHTunnel')
            self.tunnel = SSHTunnelForwarder(**params)
        except Exception as e:
            print ('Error craete_ssh_tunnel: {}'.format(e) )
            self.stop_ssh_tunnel()
        
        if not self.tunnel.is_active:
            self.start_ssh_tunnel()
            print('Tunnel had been activated successfully.')
        else:
            print('Tunnel is active already!')
        
        while self.expiry_time > 0:
            time.sleep(10)
            self.expiry_time = self.expiry_time - 1

        self.delete()

    def create_db_connection(self,db_name,connection_type):

        if db_name == 'data':
            section = 'OstadkarDataPostgresql'
        elif connection_type == 'ssh':
            section = 'OstadkarOperationSSHPostgresql'
        else:
            section = 'OstadkarOperationPostgresql'

        try:

            params = config(section=section)
            self.conn = psycopg2.connect(
                database=db_name,
                **params
            )
            
            self.r = True
        # except Exception as e:
        except:
            # print ('Error in create_operation_db_connection: {}'.format(e) )
            self.r = False
                                   
    def test_ssh_tunnel(self):
        try:

            db_name = 'gateway'
            self.create_db_connection(db_name = db_name, connection_type = 'ssh')
            self.test_conn()
            self.close_conn()
        
        except Exception as e:
            print ('Error in test_ssh_tunnel: {}'.format(e) )
    
    def start_ssh_tunnel(self):
        try:
            self.tunnel.start()
            if self.tunnel.is_active:
                self.test_ssh_tunnel()
            else:
                pass

        except Exception as e:
            print ('start_ssh_tunnel: {}'.format(e) )

    def test_conn(self):

        try:
            cur = self.conn.cursor()
            cur.execute('SELECT version()')
            db_version = cur.fetchone()
            # print(db_version)
            cur.close()
        except Exception as e:
            if self.conn:
                self.conn.close()
                self.conn = None
            print ('test_conn: {}'.format(e) )
    
    def stop_ssh_tunnel(self):
        self.close_conn()
        if self.tunnel:
            if self.tunnel.is_active:
                self.tunnel.stop()
            self.tunnel = None
        
    def close_conn(self):
        if self.conn:
            self.conn.close()
            self.conn = None

    def delete(self):
        self.expiry_time = 0
        self.stop_ssh_tunnel()

if __name__ == "__main__":
    c = OstadkarRemoteDB(db_name = 'sale',connection_type = 'ssh')
    # c.stop_ssh_tunnel()
    # c.test_conn()
    # time.sleep(200)
    # c.stop_ssh_tunnel()


