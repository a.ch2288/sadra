-- All Customers Login Infromations
with
    Login_desc as (
        select
            c.mobile,
            msg.created_at
        from messages as msg
        left join contacts c on msg.contact_id = c.id
        where msg.event_name = 'authentication_verification'
        order by
            1,2 desc
    )
    ,LastLogin as (
        select distinct on (mobile) *
        from Login_desc
    )
    ,Login as (
        select
            c.mobile,
            msg.created_at
        from messages as msg
        left join contacts c on msg.contact_id = c.id
        where msg.event_name = 'authentication_verification'
        order by
            1,2
    )
    ,FirstLogin as (
        select distinct on (mobile) *
        from Login
    )
    ,LoginCount as (
        select
            c.mobile,
            count(*) as Login_count
        from messages as msg
        left join contacts c on msg.contact_id = c.id
        where msg.event_name = 'authentication_verification'
        group by
            1

)

select
    L.mobile
    ,L.created_at as last_login
    ,F.created_at as first_login
    ,(case when C.Login_count is null then 1 else C.Login_count end )as login_count
from
    LastLogin as L
INNER JOIN
    FirstLogin as F using (mobile)
left join LoginCount as C using (mobile)
order by
    L.mobile