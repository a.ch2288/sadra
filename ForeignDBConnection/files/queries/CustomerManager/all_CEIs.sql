-- All Customer Extra Information (sale DB)
with all_orders_city as (
    select c.mobile
         , d.city_slug

    from orders as o
             left join deliveries d on o.delivery_id = d.id
             left join customers c on o.customer_user_id = c.user_id
    where o.delivery_id is not null
      and o.customer_user_id is not null
    )
    ,customers_city as (
    select mobile
         , city_slug
         , count(*) as "count"
    from all_orders_city
    where mobile <> '09353942996'
    group by 1, 2
    order by 3 desc
    )
    , most_probable_customers_city as (
    select distinct on (mobile) mobile
                              , city_slug
    from customers_city
    )
    , all_orders_id as (
    select distinct (oh.order_id)
    from order_history as oh
    where oh.order_status = 'Submitted'
    )
    , all_orders as (
    select o.id               as order_id
         , o.customer_user_id as user_id
         , o.created_at       as t2
         , o.status
         , o.service_slug
         , o.reason_id
         , o.voucher_id
    from orders as o
             right join all_orders_id as aoi on (aoi.order_id = o.id)
    where o.status in ('Done', 'WithFeedback', 'Finished','Canceled')
    order by
    o.customer_user_id,o.created_at
    )
    , sorted_all_orders AS (
    SELECT *
    FROM all_orders
    ORDER BY user_id, t2
    )
    , leaded_all_orders AS (
    SELECT *,
           LEAD(t2, 1) OVER ( PARTITION BY user_id ORDER BY user_id, t2 )      AS t1,
           LEAD(t2, 1) OVER ( PARTITION BY user_id ORDER BY user_id, t2 ) - t2 AS delta
    FROM sorted_all_orders
    )
	, filtered_orders AS (
    SELECT
        o.order_id
        ,o.user_id
        ,o.status
        ,o.service_slug
        ,o.reason_id
        ,o.voucher_id
        ,o.t2 as created_at
    FROM leaded_all_orders as o
    WHERE o.delta > INTERVAL '1 day'
       OR o.t1 IS NULL
       OR o.status IN ('Done', 'WithFeedback', 'Finished')
	order by
	    o.user_id, o.t2
    )
    , customer_numbered_orders as (
    SELECT ROW_NUMBER() OVER (PARTITION BY fo.user_id ORDER BY fo.created_at desc) AS r,
           fo.*
    FROM filtered_orders as fo
    order by
        fo.user_id,fo.created_at desc
    )
    , customer_last_5_orders as (
    SELECT c.mobile
         , string_agg(cno.service_slug, ',' order by cno.created_at desc ) as all_orders_service
         , string_agg(cno.service_slug, ',' order by cno.created_at desc ) filter ( where cno.status in ('Done', 'WithFeedback', 'Finished') ) as done_orders_service
         , string_agg(cno.service_slug, ',' order by cno.created_at desc ) filter ( where cno.status = 'Canceled') as canceled_orders_service
         , string_agg(to_char(cno.created_at, 'YYYY-MM-DD HH24:MI:SS'), ',' order by cno.created_at desc ) as all_orders_date
         , string_agg(to_char(cno.created_at, 'YYYY-MM-DD HH24:MI:SS'), ',' order by cno.created_at desc ) filter ( where cno.status in ('Done', 'WithFeedback', 'Finished') ) as done_orders_date
         , string_agg(to_char(cno.created_at, 'YYYY-MM-DD HH24:MI:SS'), ',' order by cno.created_at desc ) filter ( where cno.status = 'Canceled') as canceled_orders_date
         , string_agg(re.slug, ',' order by cno.created_at desc ) as canceled_orders_reason
         , string_agg(v.code, ',' order by cno.created_at desc ) as used_promotions
    FROM customer_numbered_orders as cno
    left join reasons as re on re.id = cno.reason_id
    left join customers as c using (user_id)
    left join vouchers as v on v.id = cno.voucher_id
    WHERE cno.r <= 5
    group by 1
    )
    , customer_last_order as (
    select
        c.mobile
        , cno.service_slug as last_order_service
        , cno.status as last_order_status
        , cno.created_at as last_order_date
    from customer_numbered_orders as cno
    left join customers as c using (user_id)
    where r = '1'
)
select
    o.*
    ,c.city_slug
    ,clo.last_order_status
    ,clo.last_order_date
    ,clo.last_order_service
from customer_last_5_orders as o
left join most_probable_customers_city as c using (mobile)
left join customer_last_order as clo using (mobile)
where
    mobile <> '09353942996'
order by
    1