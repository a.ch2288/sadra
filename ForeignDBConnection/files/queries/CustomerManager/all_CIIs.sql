-- All Customers Identification Information
select
    c.mobile
    ,c.user_name
    ,c.first_name
    ,c.last_name
    ,c.created_at
    ,(case when c.updated_at is null then c.created_at else c.updated_at end) as updated_at
from
    customers as c
where 
    c.mobile <> '09353942996'
order by
    c.mobile
