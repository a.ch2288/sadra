import os
import pandas as pd
import gspread
import gspread_dataframe as gd
from oauth2client.service_account import ServiceAccountCredentials
import uuid
class Query:

    def __init__(
        self,
        app_name = None,
        app_path = None,
        bulk_size = 5000
        ):
        
        self.source_app_path = app_path
        self.source_app_name = app_name
        self.text = """ """
        self.bulk_size = bulk_size
        self.remote_db = None
    
    def create_connection(self,product_name,connection_type,db_name):
        product_connection_switcher = {
            'ostadkar' : self.create_ostadkar_connection,
            'alich_pc' : self.create_alich_pc_connection,
            'local' : self.create_local_connection
        }
        try:
            product_connection_switcher[product_name](connection_type,db_name)
        except Exception as e:
            print('Error in Query.create_connection : {}'.format(e))

    def create_ostadkar_connection(self,connection_type,db_name):

        from .OstadkarConnection import OstadkarRemoteDB
        self.remote_db = OstadkarRemoteDB(db_name = db_name,connection_type = connection_type)

    def create_local_connection(self,connection_type,db_name):

        from .LocalConnection import Local_database
        self.remote_db = Local_database(db_name = db_name,connection_type = connection_type)

    def create_alich_pc_connection(self,connection_type,db_name):
        pass

    def delete_conncetion(self):
        if self.remote_db:
            self.remote_db.delete()
            self.remote_db = None
    
    def read_file(self,query_file_name):
        try:
            if self.source_app_path is None:
                query_path = os.path.join(os.path.dirname(__file__),'files')
            else:
                query_path = os.path.join(self.source_app_path,'files')
            query_path = os.path.join(query_path,'queries')
            if self.source_app_name is not None :
                query_path = os.path.join(query_path,self.source_app_name)
            query_path = os.path.join(query_path,query_file_name)
            f = open(query_path,'r')
            self.text = f.read()
        except Exception as e:
            print("Error in Query.read_file : {}".format(e))
        
    def read_big_data(self):
        start_at = 0
        conn = self.remote_db.conn
        if not 'offset {start_at}' in self.text:
            offset_extention = '\noffset {start_at} rows \nfetch next {bulk_size} rows only'
            source_text = self.text + offset_extention
        else:
            source_text = self.text
            
        while True :
            textt = source_text.format(start_at = start_at,bulk_size = self.bulk_size)
            data = pd.read_sql(textt,conn)
            if data.shape[0] == 0 :
                break

            yield data
            start_at = start_at + self.bulk_size
        
        self.delete_conncetion()
        yield data
    
    def read_data(self,arguments = {},delete_connection = True):
        source_text = self.text[:]
        for argument in arguments:
            source_text = source_text.replace(argument,arguments[argument])
        conn = self.remote_db.conn
        data = pd.read_sql(source_text,conn)
        if delete_connection:
            self.delete_conncetion()
        return data

    def get_host_name(self):
        import socket
        hostname = socket.gethostname()
        return(str(hostname))
        
def read_google_shit(GoogleSheetID,SheetName,fillna = True):
        
    URL = 'https://docs.google.com/spreadsheets/d/{0}/gviz/tq?tqx=out:csv&sheet={1}'.format(
        GoogleSheetID,
        SheetName
    )
    df = pd.read_csv(URL)
    if fillna:
        df = df.fillna(method='ffill')
    # print(df)
    return (df)

class GoogleSheet:

    def __init__(self,google_sheet_id,sheet_name):
        self.google_sheet_id = google_sheet_id
        self.sheet_name = sheet_name

    def get_data(self,model_feilds_type = None, fillna = True):
            
        URL = 'https://docs.google.com/spreadsheets/d/{0}/gviz/tq?tqx=out:csv&sheet={1}'.format(
            self.google_sheet_id,
            self.sheet_name
        )
        df = pd.read_csv(URL)

        if model_feilds_type == None:
            columns_name = df.fillna(method='ffill').columns[~df.fillna(method='ffill').isna().any()].tolist()
        else:
            sheet_columns_name = df.fillna(method='ffill').columns[~df.fillna(method='ffill').isna().any()].tolist()
            columns_name = []
            model_feilds_keys = model_feilds_type.keys()
            for sheet_column_name in sheet_columns_name:
                if sheet_column_name in model_feilds_keys:
                    columns_name.append(sheet_column_name)
                    
        df = df[columns_name]

        df = df[df.notnull().any(axis=1)]

        if fillna:
            df = df.fillna(method='ffill')
        
        if model_feilds_type != None:
            columns_type = {}
            for column_name in columns_name:
                columns_type[column_name] = model_feilds_type[column_name]
            df = df.astype(columns_type)
        
        return (df)
    
    def import_data_to(self,model):
        # data = self.get_data()
        # model_pk,model_feilds_type = model.get_fields_type()
        # feilds_name = list(model_feilds_type)
        # data = data[feilds_name]

        # data = data[data[model_pk].notnull()]
        # data = data.astype(model_feilds_type)

        # # old_objects_pk = model.objects.all().values_to(local_model_pk)
        # # old_data = data[data[local_model_pk].isin(old_objects_pk)]
        # # new_data = data[~data[local_model_pk].isin(old_objects_pk)]
        
        # for i,row in data.iterrows():
    
        #     model.objects.update_or_create(pk = row[model_pk],defaults = row.to_dict())

        # print('- {} {} had been imported.'.format(data.shape[0],model.name))
        pass

class GoogleSheetsConnection:

    def __init__(self,google_sheet_id,sheet_name = None):
        scope = [
            'https://www.googleapis.com/auth/spreadsheets',
            'https://www.googleapis.com/auth/drive'
            ]
        
        path = os.path.join(os.path.dirname(__file__),'files')
        path = os.path.join(path,'SadRa-b25b0828fe1c.json')
        creds = ServiceAccountCredentials.from_json_keyfile_name(path,scope)
        client = gspread.authorize(creds)
        self.spreadsheets = client.open_by_key(google_sheet_id)
        self.sheets = {}
        for sheet in self.spreadsheets.worksheets():
            self.sheets[sheet.title] = sheet

        self.data = None
        
        if sheet_name in self.sheets.keys():
            self.read_all_records(sheet_name)
            self.sheet = self.sheets[sheet_name]
    
    def read_all_records(self,sheet_name,raw = False, fillna = None,columns_type = None,model = None):
        
        if sheet_name not in self.sheets.keys():
            sheet = self.spreadsheets.add_worksheet(
                title=sheet_name, 
                rows="100", 
                cols="20"
                )
            self.sheets[sheet_name] = sheet

        self.sheet = self.sheets[sheet_name]
        
        df = pd.DataFrame(self.sheet.get_all_records(default_blank=None))

        if raw:
            self.data = df
        else:
            self.data = df[df.columns[df.notnull().any()]]
        
        if model is not None:
            columns_type = self.read_django_model_feilds_type(model)

        if columns_type is not None:
            self.modify_columns_type(columns_type)

        if fillna is not None:
            self.data = self.data.fillna(method='ffill')
        
        return(self.data)
    
    def modify_columns_type(self,columns_type):
        
        if self.data is None:
            #raise Error
            pass
        
        switcher = {
            'UUIDField': uuid.UUID,
            'CharField': str,
            'SmallIntegerField': int,
            'TextField': str,
            'FloatField': float,
            'ForeignKey': str,
            'int' :int,
            'str' :str,
            'bool' :bool

        }

        for column_name in columns_type:
            if column_name in self.data.columns.tolist() and columns_type[column_name] != 'DateTimeField' :
                self.data[column_name] = self.data[column_name].apply(lambda x: switcher[columns_type[column_name]](x))
    
    def read_django_model_feilds_type(self,model):
        columns_type = {}

        for f in model._meta.get_fields():
            columns_type[f.name] = f.get_internal_type()
        
        return columns_type
    
    def rewrite_source_sheet(self,sheet_name):
        # in khaily kharabe badan dorost she
        if sheet_name is None:
            pass
            # Error

        if sheet_name not in self.sheets.keys():
            sheet = self.spreadsheets.add_worksheet(
                title=sheet_name, 
                rows="100", 
                cols="20"
                )
            self.sheets[sheet_name] = sheet
        
        sheet = self.sheets[sheet_name]
        sheet.clear()
        gd.set_with_dataframe(sheet, self.data)
    
    def update_cells(self,column_name,index_range,value):

        column_number = self.data.columns.get_loc(column_name)
        google_sheet_column_name = chr(ord("A") + column_number)
        for i in index_range:
            self.sheet.update(f'{google_sheet_column_name}{i+2}',value)

    def update_cell(self,range_name, values=None):

        self.sheet.update(range_name, values)
    



if __name__ == "__main__":
    gsc = GoogleSheetsConnection(
        google_sheet_id = '1R4sY7ru6oKGTsyYFVJ6a8H3IMaSUm5Nl7L365g-3dLg',sheet_name = 'done')
    # print(gsc.data)
    # f = open('tt.txt','w')
    # for i,row in gsc.data.iterrows():
    #     order_number = row['Title']
    #     f.write(f"'{order_number}',\n")
    # f.close()
