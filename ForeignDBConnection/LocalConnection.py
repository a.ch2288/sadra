import os
import psycopg2
from sshtunnel import SSHTunnelForwarder
from configparser import ConfigParser
from threading import Thread
import time


def config(section , filename='LocalConnection.ini'):
    file_path = os.path.join(os.path.dirname(__file__),'files')
    file_path = os.path.join(file_path,filename)
    parser = ConfigParser()
    parser.read(file_path)
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            if param[1][0] == '(' and param[1][-1] == ')':
                p = param[1][1:-1].split(',')
                p[1] = int(p[1])
                p = tuple(p)
            else:
                p = param[1]
            db[param[0]] = p
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db

class Local_database:

    def __init__(self,db_name,connection_type = None):

        self.expiry_time = 50
        self.conn = None
        self.tunnel = None
        self.r = False

        if connection_type == 'ssh':
            print('Warning : There is no function for ssh connection_type in local connection.')

        action_thread = Thread(
            target = self.create_db_connection, 
            kwargs={'db_name' : db_name,'connection_type' : connection_type}
            )
        action_thread.start()
        action_thread.join(timeout=10)
        
        if  self.r == False :
            print('Error : Local_database had not been created!')

    def create_db_connection(self,db_name,connection_type):

        section = 'LocalPostgresql'

        try:

            params = config(section=section)
            self.conn = psycopg2.connect(
                database=db_name,
                **params
            )
            
            self.r = True
        except:
            self.r = False
        
        return None
    
    def test_conn(self):

        try:
            cur = self.conn.cursor()
            cur.execute('SELECT version()')
            db_version = cur.fetchone()
            # print(db_version)
            cur.close()
        except Exception as e:
            if self.conn:
                self.conn.close()
                self.conn = None
            print ('test_conn: {}'.format(e) )
    
    def stop_ssh_tunnel(self):
        self.close_conn()
        if self.tunnel:
            if self.tunnel.is_active:
                self.tunnel.stop()
            self.tunnel = None
        
    def close_conn(self):
        if self.conn:
            self.conn.close()
            self.conn = None

    def delete(self):
        self.expiry_time = 0
        self.stop_ssh_tunnel()

if __name__ == "__main__":
    c = Local_database(db_name = 'sale',connection_type = 'ssh')
    # c.stop_ssh_tunnel()
    # c.test_conn()
    # time.sleep(200)
    # c.stop_ssh_tunnel()


