import os
import psycopg2
from sshtunnel import SSHTunnelForwarder
from configparser import ConfigParser
from threading import Thread
import time
import pandas as pd


def config(section , filename='AllProductsConnections.ini'):
    file_path = os.path.join(os.path.dirname(__file__),'files')
    file_path = os.path.join(file_path,filename)
    parser = ConfigParser()
    parser.read(file_path)
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            if param[1][0] == '(' and param[1][-1] == ')':
                p = param[1][1:-1].split(',')
                p[1] = int(p[1])
                p = tuple(p)
            else:
                p = param[1]
            db[param[0]] = p
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db

class Connection:

    def __init__(self,section,db_name,connection_type = None):

        self.conn = None
        self.r = False
        self.tunnel = None
        self.expiry_time = 50

        if connection_type == 'ssh':
            self.tunnel = None
            t = Thread(target = self.run_server) 
            t.start()
            time.sleep(20)

        action_thread = Thread(
            target = self.create_db_connection, 
            kwargs={'db_name' : db_name,'section' : section}
            )
        action_thread.start()
        action_thread.join(timeout=10)
        
        if  self.r == False :
            print('Error : connection has not been created!')

    def create_db_connection(self,db_name,section):

        # print(section)
        try:
            params = config(section=section)
            self.conn = psycopg2.connect(
                database=db_name,
                **params
            )
            self.r = True
        except Exception as e:
            self.r = False
            # print('aaa')
            print(e)
        
        return None
    
    def test_conn(self):

        try:
            cur = self.conn.cursor()
            cur.execute('SELECT version()')
            db_version = cur.fetchone()
            # print(db_version)
            cur.close()
            return True
        except Exception as e:
            if self.conn:
                self.conn.close()
                self.conn = None
            print ('test_conn: {}'.format(e) )
            return False
        
    def run_server(self):
        
        try:
            params = config(section='OstadkarProductSSHTunnel')
            self.tunnel = SSHTunnelForwarder(**params)
        except Exception as e:
            print ('Error craete_ssh_tunnel: {}'.format(e) )
            if self.tunnel:
                if self.tunnel.is_active:
                    self.tunnel.stop()
                self.tunnel = None
            
            return None
        
        if not self.tunnel.is_active:
            try:
                self.tunnel.start()
                print('Tunnel had been activated successfully.')
            except Exception as e:
                print ('start_ssh_tunnel: {}'.format(e) )
            
        else:
            print('Tunnel is active already!')
        
        while self.expiry_time > 0:
            time.sleep(10)
            self.expiry_time = self.expiry_time - 1

        self.delete()
          
    def close_conn(self):
        if self.conn:
            self.conn.close()
            self.conn = None

    def delete(self):
        self.close_conn()
        self.expiry_time = 0
        if self.tunnel:
            if self.tunnel.is_active:
                self.tunnel.stop()
            self.tunnel = None
    
class Query:

    def __init__(
        self,
        app_name = None,
        app_path = None,
        bulk_size = 5000
        ):
        
        self.source_app_path = app_path
        self.source_app_name = app_name
        self.text = """ """
        self.bulk_size = bulk_size
        self.remote_db = None
    
    def create_connection(self,product_name,connection_type,db_name):
        
        try:
            section = self.identify_section(product_name,connection_type)
            self.remote_db = Connection(section = section,db_name = db_name)
            if not self.remote_db.test_conn() and product_name == 'ostadkar':
                self.remote_db.delete()
                print('Conncetion to Ostadkar DB had not been created! I will check SSHtunnel and try again.')
                connection_type = 'ssh'
                section = self.identify_section(product_name,connection_type)
                
                self.remote_db = Connection(section = section,db_name = db_name,connection_type = 'ssh')
                if not self.remote_db.test_conn():
                    self.remote_db.delete()
                    print('a')

        except Exception as e:
            print('Error in Query.create_connection : {}'.format(e))

    def identify_section(self,product_name,connection_type):
        switcher = {
            'ostadkar' : 'OstadkarProductSSHPostgresql' if connection_type == 'ssh' else 'OstadkarProductPostgresql',
            'ostadkar_data' : 'OstadkarDataPostgresql',
            'ostadkar_previous' : 'OstadkarPreviousPostgresql',
            'local' : 'LocalPostgresql'
        }
        return(switcher[product_name])

    def delete_conncetion(self):
        if self.remote_db:
            self.remote_db.delete()
            self.remote_db = None
    
    def read_file(self,query_file_name):
        try:
            if self.source_app_path is None:
                query_path = os.path.join(os.path.dirname(__file__),'files')
            else:
                query_path = os.path.join(self.source_app_path,'files')
            query_path = os.path.join(query_path,'queries')
            if self.source_app_name is not None :
                query_path = os.path.join(query_path,self.source_app_name)
            query_path = os.path.join(query_path,query_file_name)
            f = open(query_path,'r')
            self.text = f.read()
        except Exception as e:
            print("Error in Query.read_file : {}".format(e))
        
    def read_big_data(self):
        start_at = 0
        conn = self.remote_db.conn
        if not 'offset {start_at}' in self.text:
            offset_extention = '\noffset {start_at} rows \nfetch next {bulk_size} rows only'
            source_text = self.text + offset_extention
        else:
            source_text = self.text
            
        while True :
            textt = source_text.format(start_at = start_at,bulk_size = self.bulk_size)
            data = pd.read_sql(textt,conn)
            if data.shape[0] == 0 :
                break

            yield data
            start_at = start_at + self.bulk_size
        
        self.delete_conncetion()
        yield data
    
    def read_data(self,arguments = {},delete_connection = True):
        source_text = self.text[:]
        for argument in arguments:
            source_text = source_text.replace(argument,arguments[argument])
        conn = self.remote_db.conn
        data = pd.read_sql(source_text,conn)
        if delete_connection:
            self.delete_conncetion()
        return data

    def get_host_name(self):
        import socket
        hostname = socket.gethostname()
        return(str(hostname))


if __name__ == "__main__":
    # c = Connection(db_name = 'report_db',section = 'OstadKarPreviousPostgresql')
    # c.test_conn()
    # c.close_conn()

    q = Query()
    query_file_name = 'test_query.sql'
    q.read_file(query_file_name=query_file_name)
    q.create_connection('ostadkar','','sale')
    try:
        data = q.read_data()
    except:
        q.remote_db.delete()
    print(data)


