from celery import shared_task
from .Functions import Provider_Performance_Manager
from datetime import datetime

check = True

@shared_task
def update_ppm_sheet(services):
    ppm = Provider_Performance_Manager()
    ppm.run(services)

