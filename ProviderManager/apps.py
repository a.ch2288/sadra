from django.apps import AppConfig


class ProvidermanagerConfig(AppConfig):
    name = 'ProviderManager'
