-- Accepted Providers' Info (dispatch DB)
WITH
accepted_providers AS (
    SELECT ps.user_id as provider_id,
           ps.mobile,
           ps.user_name,
           ps.first_name || ' ' || ps.last_name AS name,
           ps.success_rate,
           ps.status,
           ps.gender,
           ps.national_code,
           ps.created_at,
           ps.updated_at,
           ps.deleted_at,
           round(EXTRACT(EPOCH FROM (current_timestamp) - ps.created_at) / (24.0 * 36)) / 100.0
                                                AS "RegistrationUntilNow(Day)"

    FROM providers ps
    where ps.status = 'Accepted'
)
select
    *
from accepted_providers