-- :)
SELECT
		provider_id,
		service_slug,

		SUM(final_cost) "ProviderIncome",

		COUNT((provider_id)) "TotalQuotes",

		COUNT((provider_id)) FILTER(WHERE seen_at IS NOT NULL) "SeenQuotes",
		COUNT((provider_id)) FILTER(WHERE accepted_at IS NOT NULL) "SelectedQuotes",
		COUNT((provider_id)) FILTER(WHERE finished_at IS NOT NULL) "DoneOrders",
		COUNT((provider_id)) FILTER(WHERE accepted_at IS NOT NULL AND
		                                  service_status = 'Canceled') "CanceledQuotes",

		COUNT((provider_id)) FILTER(WHERE quote_time > '{t1}') "TotalQuotes2Weeks",
		COUNT((provider_id)) FILTER(WHERE accepted_at IS NOT NULL AND
		                                  quote_time > '{t2}') "SelectedOrders2Weeks",
		COUNT((provider_id)) FILTER(WHERE finished_at IS NOT NULL AND
		                                  quote_time > '{t2}') "DoneOrders2Weeks",
		COUNT((provider_id)) FILTER(WHERE accepted_at IS NOT NULL AND
		                                  service_status = 'Canceled' AND quote_time > '{t2}') "CanceledOrders2Weeks"

FROM data
GROUP BY 1, 2