select *,
        case
            when "SelectedQuotes" is not null and "SelectedQuotes" <> 0 then "DoneOrders" * 1.0 / "SelectedQuotes"
            else 0
            end as "TDR",
        case
            when "SelectedOrders2Weeks" is not null and "SelectedOrders2Weeks" <> 0 then "DoneOrders2Weeks" * 1.0 / "SelectedOrders2Weeks"
            else 0
            end as "RDR"
from data