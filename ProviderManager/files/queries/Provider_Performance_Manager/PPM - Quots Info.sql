-- All Quotes (dispatch DB)
WITH
quotes_info AS (
	SELECT
		q.seen_at,
	    q.accepted_at,
	    q.finished_at,
        q.final_cost,
		q.created_at "quote_time",
		substring(date_trunc('week', q.created_at)::text FROM 1 FOR 10)::text AS quote_week,
		o.provider_id,
	    o.order_number


	FROM quotes q
	LEFT JOIN offers o ON q.offer_id = o.id
)
select
    *
from quotes_info

