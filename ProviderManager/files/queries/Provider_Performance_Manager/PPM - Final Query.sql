-- Data DB
WITH
quotes_info AS (
    SELECT
		q.seen_at,
	    q.accepted_at,
	    q.finished_at,
        q.final_cost,
		q.created_at "quote_time",
		substring(date_trunc('week', q.created_at)::text FROM 1 FOR 10)::text AS quote_week,
		o.provider_id,
	    o.order_number

	FROM quotes q
	LEFT JOIN offers o ON q.offer_id = o.id
),
orders_info as (
    select o.number as order_number,
           o.service_slug as order_service_slug,
           o.status as service_status
    from orders as o
),
quots_with_orders_info as (
    select
        q.*,
        o.*
    from quotes_info as q
    left join orders_info as o using(order_number)
),
providers_services AS (
    SELECT ps.user_id,
           (UNNEST(service_slug_list::text[])) service_slug
    FROM providers ps
),
filter_providers_services as (
    select *
    from providers_services as ps
             left join quots_with_orders_info as qwoi on (
            qwoi.provider_id = ps.user_id and
            qwoi.order_service_slug = ps.service_slug)
),
first_calculation AS (
    SELECT user_id as                                                                          provider_id,
           service_slug,

           SUM(final_cost)                                                                     "ProviderIncome",

           COUNT((user_id))                                                                    "TotalQuotes",

           COUNT((user_id)) FILTER (WHERE seen_at IS NOT NULL)                                 "SeenQuotes",
           COUNT((user_id)) FILTER (WHERE accepted_at IS NOT NULL)                             "SelectedQuotes",
           COUNT((user_id)) FILTER (WHERE finished_at IS NOT NULL)                             "DoneOrders",
           COUNT((user_id)) FILTER (WHERE accepted_at IS NOT NULL AND
                                          service_status = 'Canceled')                         "CanceledQuotes",

           COUNT((user_id)) FILTER (WHERE quote_time > current_timestamp - INTERVAL '14 days') "TotalQuotes2Weeks",
           COUNT((user_id)) FILTER (WHERE accepted_at IS NOT NULL AND
                                          quote_time > current_timestamp - INTERVAL '15 days') "SelectedOrders2Weeks",
           COUNT((user_id)) FILTER (WHERE finished_at IS NOT NULL AND
                                          quote_time > current_timestamp - INTERVAL '15 days') "DoneOrders2Weeks",
           COUNT((user_id)) FILTER (WHERE accepted_at IS NOT NULL AND
                                          service_status = 'Canceled' AND
                                          quote_time > current_timestamp - INTERVAL '15 days') "CanceledOrders2Weeks"

    FROM filter_providers_services
    GROUP BY 1, 2
),
second_calculation as (
    select *,
           case
               when "SelectedQuotes" is not null and "SelectedQuotes" <> 0 then "DoneOrders" * 1.0 / "SelectedQuotes"
               else 0
               end as "TDR",
           case
               when "SelectedOrders2Weeks" is not null and "SelectedOrders2Weeks" <> 0 then "DoneOrders2Weeks" * 1.0 / "SelectedOrders2Weeks"
               else 0
               end as "RDR"
    from first_calculation
),
accepted_providers AS (
    SELECT p.user_id as provider_id,
           p.mobile,
           p.user_name,
           p.first_name || ' ' || p.last_name AS name,
           p.success_rate,
           p.status,
           p.gender,
           p.national_code,
           p.created_at,
           p.updated_at,
           p.deleted_at,
           round(EXTRACT(EPOCH FROM (current_timestamp) - p.created_at) / (24.0 * 3600))
                                                AS "RegistrationUntilNow(Day)"
    FROM  providers as p
    where p.status = 'Accepted'
),
cluster as (
    select *,
           case
               when "RegistrationUntilNow(Day)" < 31 then 'orange'
               when "SelectedOrders2Weeks" > 0 and "RDR" >= 0.5 then 'green'
               when "SelectedOrders2Weeks" > 0 and "RDR" < 0.5 and "CanceledOrders2Weeks" > 2 then 'red'
               when "SelectedOrders2Weeks" > 0 then 'white'
               when "SelectedOrders2Weeks" = 0 and "TotalQuotes2Weeks" > 0 and
                    ("TDR" >= 0.3 or ("TDR" = 0 and "CanceledQuotes" < 3)) then 'blue'
               when "SelectedOrders2Weeks" = 0 and "TotalQuotes2Weeks" = 0 and
                    ("TDR" >= 0.3 or ("TDR" = 0 and "CanceledQuotes" < 3)) then 'yellow'
               when "SelectedOrders2Weeks" = 0 then 'violet'
               end as "provider_status",
           case
               when "RegistrationUntilNow(Day)" < 31 then '0'
               when "SelectedOrders2Weeks" > 0 and "RDR" >= 0.5 then '2'
               when "SelectedOrders2Weeks" > 0 and "RDR" < 0.5 and "CanceledOrders2Weeks" > 2 then '5'
               when "SelectedOrders2Weeks" > 0 then '3'
               when "SelectedOrders2Weeks" = 0 and "TotalQuotes2Weeks" > 0 and
                    ("TDR" >= 0.3 or ("TDR" = 0 and "CanceledQuotes" < 3)) then '1'
               when "SelectedOrders2Weeks" = 0 and "TotalQuotes2Weeks" = 0 and
                    ("TDR" >= 0.3 or ("TDR" = 0 and "CanceledQuotes" < 3)) then '4'
               when "SelectedOrders2Weeks" = 0 then '6'
               end as "provider_priority"
    from accepted_providers as ap
             left join second_calculation as sc using(provider_id)
)
select
    *
from cluster

