-- All Orders Info (sale DB)
with
orders_info as (
    select o.number as order_number,
           o.service_slug,
           o.status as service_status
    from orders as o
)
select
    *
from orders_info