select *,
       case
           
           when "SelectedOrders2Weeks" > 0 and "RDR" >= 0.5 then 'green'
           when "SelectedOrders2Weeks" > 0 and "RDR" < 0.5 and "CanceledOrders2Weeks" > 2 then 'red'
           when "SelectedOrders2Weeks" > 0 then 'white'
           when "SelectedOrders2Weeks" = 0 and "TotalQuotes2Weeks" > 0 and ("TDR" >= 0.3 or ("TDR" = 0 and "CanceledQuotes" < 3)) then 'blue'
           when "SelectedOrders2Weeks" = 0 and "TotalQuotes2Weeks" = 0 and ("TDR" >= 0.3 or ("TDR" = 0 and "CanceledQuotes" < 3)) then 'yellow'
           when "SelectedOrders2Weeks" = 0 then 'violet'
           end as "provider_status",
       case
           
           when "SelectedOrders2Weeks" > 0 and "RDR" >= 0.5 then '2'
           when "SelectedOrders2Weeks" > 0 and "RDR" < 0.5 and "CanceledOrders2Weeks" > 2 then '5'
           when "SelectedOrders2Weeks" > 0 then '3'
           when "SelectedOrders2Weeks" = 0 and "TotalQuotes2Weeks" > 0 and ("TDR" >= 0.3 or ("TDR" = 0 and "CanceledQuotes" < 3)) then '1'
           when "SelectedOrders2Weeks" = 0 and "TotalQuotes2Weeks" = 0 and ("TDR" >= 0.3 or ("TDR" = 0 and "CanceledQuotes" < 3)) then '4'
           when "SelectedOrders2Weeks" = 0 then '6'
           end as "provider_priority"

from data