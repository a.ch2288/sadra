-- All Providers Active Services (dispatch DB)
WITH
providers_services AS (
    SELECT ps.user_id as provider_id,
           (UNNEST(service_slug_list)) service_slug
    FROM providers ps
    where ps.status = 'Accepted'
)
select
    *
from providers_services
where service_slug is not null