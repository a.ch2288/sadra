import os 
# import sys
# sys.path.append(os.path.join(os.path.dirname(__file__), '../'))

from ForeignDBConnection.QueryManager import GoogleSheetsConnection
from ForeignDBConnection.ConnectionManager import Query
import pandas as pd
import pandasql as ps
import datetime as DT
import uuid
import time
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sadra.settings")
from django.conf import settings
# import django
# django.setup()


class Provider_Performance_Manager:
    
    def __init__(self):
        self.google_sheets_connection = GoogleSheetsConnection(settings.PROVIDER_PERFORMANCE_MANAGER_GOOGLE_SHEETS_ID)
        self.queries_info = {
            'file_name' : {
                'orders' : 'PPM - Orders Info.sql',
                'providers' : 'PPM - Accepted Providers Info.sql',
                'providers_services' : 'PPM - Providers Services.sql',
                'quots' : 'PPM - Quots Info.sql',
                'first_calculator' : 'PPM - Calculator Part1.sql',
                'second_calculator' : 'PPM - Calculator Part2.sql',
                'clusterization' : 'PPM - Clusterization.sql'
            },
            'database' : {
                'orders' : 'sale',
                'providers' : 'dispatch',
                'providers_services' : 'dispatch',
                'quots' : 'dispatch' 
            },
            'data' : {
                'orders' : None,
                'providers' : None,
                'providers_services' : None,
                'quots' : None 
            }
        }
        self.data = None
        self.query = Query(
            app_name = 'Provider_Performance_Manager',
            app_path = os.path.dirname(__file__))
    
    def run(self,services):
        self.get_data()
        self.clustering()
        self.upload_data(services = services)

    def get_data(self):
        
        for key in self.queries_info['data']:
            file_name = self.queries_info['file_name'][key]
            self.query.read_file(query_file_name=file_name)
            self.query.create_connection(
                product_name = 'ostadkar',
                connection_type = '',
                db_name = self.queries_info['database'][key])

            self.queries_info['data'][key] = self.query.read_data()

    def clustering(self):
        
        data = pd.merge(
            self.queries_info['data']['quots'], 
            self.queries_info['data']['orders'], 
            on="order_number",
            how="left"
            )
        
        data = pd.merge(
            self.queries_info['data']['providers_services'], 
            data, 
            on=["provider_id","service_slug"],
            how="left"
            )
        
        data = self.per_analysis(data = data)

        data = data.astype({'provider_id':str})
        
        for name in ['clusterization']:

            file_name = self.queries_info['file_name'][name]
            self.query.read_file(query_file_name=file_name)
            text = self.query.text
            data = ps.sqldf(text, locals())

        data['provider_id'] = data['provider_id'].apply(lambda x: uuid.UUID(x))

        data = pd.merge(
            data,
            self.queries_info['data']['providers'],  
            on="provider_id",
            how="left"
            )
        
        condition = data['RegistrationUntilNow(Day)'] < 31
        data.loc[condition,'provider_status'] = 'orange'
        data.loc[condition,'provider_priority'] = '0'

        self.data = data.sort_values(
            by=['service_slug','provider_priority','TDR'],
            ascending=[True,True,False])

    def upload_data(self,services):

        columns_name = ['mobile','name','gender','service_slug','provider_priority']

        self.google_sheets_connection.data = self.data[columns_name]
        self.google_sheets_connection.rewrite_source_sheet(
            sheet_name='AllData')

        services_slug = pd.unique(self.data['service_slug'])
        
        counter = 0
        for service_slug in services:

            updated_data = self.data[self.data['service_slug'] == service_slug]
            updated_data = updated_data[columns_name + ['TDR']]

            data = self.google_sheets_connection.read_all_records(
                sheet_name = service_slug,
                raw=True)
            
            data = data[data.columns[~data.columns.isin(['name','gender','service_slug','provider_priority'])]]
            
            try:
                updated_data = updated_data.astype({'mobile':int})
                data = data.astype({'mobile':int})
            except Exception as e:
                print(e)
            
            try:
                data = pd.merge(
                    updated_data, 
                    data, 
                    on="mobile",
                    how="left"
                    )
            except Exception as e:
                print(e)
                data = updated_data

            data = data.sort_values(
                by=['provider_priority','TDR'],
                ascending=[True,False])

            data = data[data.columns[data.columns != 'TDR']]

            self.google_sheets_connection.data = data
            self.google_sheets_connection.rewrite_source_sheet(
                sheet_name=service_slug)
            counter += 1
            if counter % 10 == 0:
                print(f'- {counter} services have been updated.)')
                time.sleep(60)

    def per_analysis(self,data):

        t1 = DT.datetime.now() - DT.timedelta(days=14)
        t2 = DT.datetime.now() - DT.timedelta(days=15)

        # ProviderIncome
        ProviderIncome = data[['provider_id','service_slug','final_cost']].\
            groupby(['provider_id','service_slug'],as_index = False).sum()
        ProviderIncome = ProviderIncome.rename(columns={'final_cost':'ProviderIncome'})
        
        # TotalQuotes
        TotalQuotes = data[['provider_id','service_slug']]
        TotalQuotes.insert(loc=2, column='new_column', value=TotalQuotes['provider_id'])
        TotalQuotes = TotalQuotes.\
            groupby(['provider_id','service_slug'],as_index = False).count()
        TotalQuotes = TotalQuotes.rename(columns={'new_column':'TotalQuotes'})
        
        # SeenQuotes
        condition = data['seen_at'].notnull()
        SeenQuotes = data[condition]\
            [['provider_id','service_slug','seen_at']].\
                groupby(['provider_id','service_slug'],as_index = False).count()
        SeenQuotes = SeenQuotes.rename(columns={'seen_at':'SeenQuotes'})
        
        # SelectedQuotes
        condition = data['accepted_at'].notnull()
        SelectedQuotes = data[condition]\
            [['provider_id','service_slug','accepted_at']].\
                groupby(['provider_id','service_slug'],as_index = False).count()
        SelectedQuotes = SelectedQuotes.rename(columns={'accepted_at':'SelectedQuotes'})
        
        # DoneOrders
        condition = data['finished_at'].notnull()
        DoneOrders = data[condition]\
            [['provider_id','service_slug','finished_at']].\
                groupby(['provider_id','service_slug'],as_index = False).count()
        DoneOrders = DoneOrders.rename(columns={'finished_at':'DoneOrders'})
        
        # CanceledQuotes
        condition = ( data['accepted_at'].notnull() ) &\
             ( data['service_status'] == 'Canceled')
        CanceledQuotes = data[condition]\
            [['provider_id','service_slug','accepted_at']].\
                groupby(['provider_id','service_slug'],as_index = False).count()
        CanceledQuotes = CanceledQuotes.rename(columns={'accepted_at':'CanceledQuotes'})
        
        # TotalQuotes2Weeks
        condition = data['quote_time'] > (t1)
        TotalQuotes2Weeks = data[condition]\
            [['provider_id','service_slug','quote_time']].\
                groupby(['provider_id','service_slug'],as_index = False).count()
        TotalQuotes2Weeks = TotalQuotes2Weeks.rename(columns={'quote_time':'TotalQuotes2Weeks'})
        
        # SelectedOrders2Weeks
        condition = ( data['accepted_at'].notnull() ) &\
             ( data['quote_time'] > (t2) )
        SelectedOrders2Weeks = data[condition]\
            [['provider_id','service_slug','accepted_at']].\
                groupby(['provider_id','service_slug'],as_index = False).count()
        SelectedOrders2Weeks = SelectedOrders2Weeks.rename(columns={'accepted_at':'SelectedOrders2Weeks'})
        
        # DoneOrders2Weeks
        condition = ( data['finished_at'].notnull() ) &\
             ( data['quote_time'] > (t2) )
        DoneOrders2Weeks = data[condition]\
            [['provider_id','service_slug','finished_at']].\
                groupby(['provider_id','service_slug'],as_index = False).count()
        DoneOrders2Weeks = DoneOrders2Weeks.rename(columns={'finished_at':'DoneOrders2Weeks'})
        
        # CanceledOrders2Weeks
        condition = ( data['accepted_at'].notnull() ) &\
             ( data['service_status'] == 'Canceled' ) &\
                  ( data['quote_time'] > (t2) )
        CanceledOrders2Weeks = data[condition]\
            [['provider_id','service_slug','accepted_at']].\
                groupby(['provider_id','service_slug'],as_index = False).count()
        CanceledOrders2Weeks = CanceledOrders2Weeks.rename(columns={'accepted_at':'CanceledOrders2Weeks'})
        
        results_list = [
            ProviderIncome,
            SeenQuotes,
            SelectedQuotes,
            DoneOrders,
            CanceledQuotes,
            TotalQuotes2Weeks,
            SelectedOrders2Weeks,
            DoneOrders2Weeks,
            CanceledOrders2Weeks
            ]

        merged_result = TotalQuotes
        for result in results_list:
            merged_result = pd.merge(
                merged_result,
                result,
                on=['provider_id','service_slug'],
                how='outer'
                )


        condition = merged_result['SelectedQuotes'].isnull()
        merged_result.loc[condition,'SelectedQuotes'] = 0

        condition = merged_result['SelectedOrders2Weeks'].isnull()
        merged_result.loc[condition,'SelectedOrders2Weeks'] = 0

        merged_result['TDR'] = 0
        condition = (merged_result['SelectedQuotes'].notnull()) & (merged_result['SelectedQuotes'] != 0)
        merged_result.loc[condition,'TDR'] = merged_result[condition]['DoneOrders']*1.0/merged_result[condition]['SelectedQuotes']
        
        merged_result['RDR'] = 0
        condition = (merged_result['SelectedOrders2Weeks'].notnull()) & (merged_result['SelectedOrders2Weeks'] != 0)
        merged_result.loc[condition,'RDR'] = merged_result[condition]['DoneOrders2Weeks']*1.0/merged_result[condition]['SelectedOrders2Weeks']

        return merged_result.sort_values(by = ['provider_id','service_slug','TDR'])


if __name__ == "__main__":
    ppm = Provider_Performance_Manager()
    ppm.run(["carpetcleaning","buildingcleaning","boiler","housecleaning","officecleaning","moving","radiator"])
    # ["carpetcleaning","buildingcleaning","boiler","housecleaning","officecleaning","moving","radiator"]