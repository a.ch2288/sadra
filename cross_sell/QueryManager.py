import os
import pandas as pd

class Query:

    def __init__(
        self,
        app_name = None,
        app_path = None,
        bulk_size = 5000
        ):
        
        self.source_app_path = app_path
        self.source_app_name = app_name
        self.text = """ """
        self.bulk_size = bulk_size
        self.remote_db = None
    
    def create_connection(self,product_name,connection_type,db_name):

        from OstadkarOldDBConnection import OstadkarOldDB
        self.remote_db = OstadkarOldDB(db_name = db_name,connection_type = connection_type)

    def delete_conncetion(self):
        if self.remote_db:
            self.remote_db.delete()
            self.remote_db = None
    
    def read_file(self,query_file_name):
        try:
            if self.source_app_path == None:
                query_path = os.path.join(os.path.dirname(__file__),'files')
            else:
                query_path = os.path.join(self.source_app_path,'files')
            query_path = os.path.join(query_path,'queries')
            if not self.source_app_name == None :
                query_path = os.path.join(query_path,self.source_app_name)
            query_path = os.path.join(query_path,query_file_name)
            f = open(query_path,'r')
            self.text = f.read()
        except Exception as e:
            print("Error in Query.read_file : {}".format(e))
        
    def read_big_data(self):
        start_at = 0
        conn = self.remote_db.conn
        if not 'offset {start_at}' in self.text:
            offset_extention = '\noffset {start_at} rows \nfetch next {bulk_size} rows only'
            source_text = self.text + offset_extention
        else:
            source_text = self.text
            
        while True :
            textt = source_text.format(start_at = start_at,bulk_size = self.bulk_size)
            data = pd.read_sql(textt,conn)
            if data.shape[0] == 0 :
                break

            yield data
            start_at = start_at + self.bulk_size
        
        self.delete_conncetion()
        yield data
    
    def read_data(self,arguments = {},delete_connection = True):
        source_text = self.text[:]
        for argument in arguments:
            source_text = source_text.replace(argument,arguments[argument])
        conn = self.remote_db.conn
        data = pd.read_sql(source_text,conn)
        if delete_connection:
            self.delete_conncetion()
        return data



if __name__ == "__main__":
    pass
