with target_customers as (
    select customer_username,
           count(*)
    from tbl_orders
    group by 1
    having count(*) > 1
    )

select
    ROW_NUMBER() OVER (order by o.customer_username,o.order_date)  as general_row_number,
    ROW_NUMBER() OVER (PARTITION BY o.customer_username ORDER BY o.customer_username,o.order_date) AS internal_row_number,
    o.customer_username,
    o.service_category_slug,
    o.order_date
from tbl_orders as o
right join target_customers as tc using(customer_username)
order by
    o.customer_username,o.order_date
