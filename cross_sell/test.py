from QueryManager import Query
import datetime as dt
import pandas as pd
import os

base_path = os.path.join(os.path.dirname(__file__),'files')
t = dt.datetime.now()
query = Query()
query.read_file(query_file_name = 'OrdersStrings.sql')
query.create_connection(product_name = 'OldDB',connection_type='',db_name = 'report_db')
data = query.read_data()
data_path = os.path.join(base_path,'LocalData.csv')
data.to_csv(data_path,index=False)

# data = pd.read_csv(data_path)
print(dt.datetime.now() - t)
customers = data['customer_username'].unique()
relation_dic = {'source_service' : ['test'],
                'pend_service' : ['test'],
                'count' : [1]}
relations = pd.DataFrame(relation_dic)
counter = 0
for customer in customers:
    customer_data = data[data['customer_username'] == customer]
    customer_data = customer_data.sort_values(by=['general_row_number'])

    source_service_repe = None
    for i,row_i in customer_data.iterrows():
        source_service = row_i['service_category_slug']
        internal_row_number = row_i['internal_row_number']
        if source_service != source_service_repe:
            source_service_repe = source_service
            next_rows = customer_data[customer_data['internal_row_number'] > (internal_row_number)]
            pend_service_repe = None
            for j,row_j in next_rows.iterrows():
                pend_service = row_j['service_category_slug']
                if pend_service != pend_service_repe:
                    pend_service_repe = pend_service
                    if row_i['order_date'] + dt.timedelta(days = 14) < row_j['order_date']:
                        break
                    if pend_service != source_service:

                        test = (relations['source_service'] == source_service) & (relations['pend_service'] == pend_service)

                        if relations[test].shape[0] == 0:
                            relation_dic = {'source_service' : source_service,
                                            'pend_service' : pend_service,
                                            'count' : 1}
                            relations.loc[relations.shape[0]] = relation_dic
                            
                        else:
                            relations.loc[test,'count'] += 1

    # counter += 1
    # if counter > 5000:
    #     break

relations = relations.sort_values(by = ['count'],ascending=False,ignore_index=True)

# print(relations.loc[0:20])

print(dt.datetime.now() - t)


relations_data_path = os.path.join(base_path,'relations.csv')
relations.to_csv(relations_data_path,index=False)

# relations = pd.read_csv(relations_data_path)

services_category_slug = relations['source_service'].unique()

relation_map = pd.DataFrame(
    {
        'source_service' : [],
        '1' : [],
        '2' : [],
        '3' : [],
        '4' : [],
        '5' : []
    }
)
relation_slug = pd.DataFrame(
    {
        'source_service' : [],
        '1' : [],
        '2' : [],
        '3' : [],
        '4' : [],
        '5' : []
    }
)
relation_tbl = pd.DataFrame(
    {
        'source_service' : [],
        '1' : [],
        '2' : [],
        '3' : [],
        '4' : [],
        '5' : []
    }
)

number = 0
for service_category_slug in services_category_slug:
    related_services = relations[relations['source_service'] == service_category_slug]
    related_services = related_services.sort_values(by = ['count'],ascending=False,ignore_index=True)
    related_services = related_services[0:5]
    new_slug_row = {
        'source_service' : service_category_slug,
        '1' : None,
        '2' : None,
        '3' : None,
        '4' : None,
        '5' : None
    }
    new_map_row = {
        'source_service' : service_category_slug,
        '1' : None,
        '2' : None,
        '3' : None,
        '4' : None,
        '5' : None
    }
    for i,row in related_services.iterrows():
        new_slug_row[str(i+1)] = row['pend_service']
        new_map_row[str(i+1)] = row['count']

    relation_slug.loc[number] = new_slug_row
    relation_map.loc[number] = new_map_row
    relation_tbl.loc[number*2] = new_slug_row
    relation_tbl.loc[number*2 + 1] = new_map_row
    number += 1


    # if number > 1000:
    #     break



relation_slug.to_csv(os.path.join(base_path,'relation_slug.csv'),index=False)
relation_map.to_csv(os.path.join(base_path,'relation_map.csv'),index=False)
relation_tbl.to_csv(os.path.join(base_path,'relation_tbl.csv'),index=False)

print(dt.datetime.now() - t)