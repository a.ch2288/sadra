import os
import psycopg2
from sshtunnel import SSHTunnelForwarder
from configparser import ConfigParser
from threading import Thread
import time


def config(section , filename='OldDatabase.ini'):
    file_path = os.path.join(os.path.dirname(__file__),'files')
    file_path = os.path.join(file_path,filename)
    parser = ConfigParser()
    parser.read(file_path)
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            if param[1][0] == '(' and param[1][-1] == ')':
                p = param[1][1:-1].split(',')
                p[1] = int(p[1])
                p = tuple(p)
            else:
                p = param[1]
            db[param[0]] = p
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db

class TypicalConnection:

    def __init__(self,section,db_name):

        self.conn = None
        self.r = False

        action_thread = Thread(
            target = self.create_db_connection, 
            kwargs={'db_name' : db_name,'section' : section}
            )
        action_thread.start()
        action_thread.join(timeout=10)
        
        if  self.r == False :
            print('Error : OstadkarOldDB_connection had not been created!')

    def create_db_connection(self,db_name,section):

        section = section

        try:
            params = config(section=section)
            self.conn = psycopg2.connect(
                database=db_name,
                **params
            )
            self.r = True
        except:
            self.r = False
        
        return None
    
    def test_conn(self):

        try:
            cur = self.conn.cursor()
            cur.execute('SELECT version()')
            db_version = cur.fetchone()
            print(db_version)
            cur.close()
        except Exception as e:
            if self.conn:
                self.conn.close()
                self.conn = None
            print ('test_conn: {}'.format(e) )
        
    def close_conn(self):
        if self.conn:
            self.conn.close()
            self.conn = None

if __name__ == "__main__":
    c = TypicalConnection(db_name = 'report_db',section = 'OstadkarOldDB')
    c.test_conn()
    c.close_conn()


