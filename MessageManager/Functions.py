
from .models import SMS_Template
from ScenarioManager.ModelManager import get_scenario_structure_info
from ScenarioManager.ModelManager import get_scenario_info
from MessageManager.ModelManager import get_template_model
from CustomerManager.ModelManager import get_customer_info
from ServiceManager.ModelManager import get_service_info
from PromotionManager.ModelManager import get_promotion_info
from LinkManager.Functions import LinkCreator
import datetime as DT


# dorostesh ineke functon haye get ro az aval benevisam ke list behm tahvil bedan

def en_to_fa(en_number):

    en_number = str(en_number)
    number_translator = {
        '1' : '۱',
        '2' : '۲',
        '3' : '۳',
        '4' : '۴',
        '5' : '۵',
        '6' : '۶',
        '7' : '۷',
        '8' : '۸',
        '9' : '۹',
        '0' : '۰',
        '.' : '.'
    }
    
    fa_number = ''
    for c in en_number:
        fa_number = fa_number + number_translator[c]
    
    return fa_number

class MessageTextCreator:

    def __init__(self):
        pass

    def pre_initialization(self,
        structure_name,
        receivers_id_list,
        ):
        # noww = DT.datetime.now()
        self.bulk_size = len(receivers_id_list)
        self.link_creator = LinkCreator(bulk_size = self.bulk_size)

        structure_info = self.define_structure_info(
            structure_name = structure_name
        )

        self.template_text,self.template_arguments_list = self.define_template_info(
            template_type = structure_info['message_template_type'],
            template_id = structure_info['message_template_id']
            )

        self.receivers_info_list = self.define_receivers_info_list(
            receivers_id_list = receivers_id_list
            )
        

        if structure_info['message_service_slug'] != 'empty':
            self.services_info_list = self.define_services_info_list(
                receivers_id_list = receivers_id_list,
                service_slug = structure_info['message_service_slug']
                )
        else:
            self.services_info_list = ['empty' for i in range(len(receivers_id_list))]

        if structure_info['message_promotion_code'] != 'empty':
            self.promotions_info_list = self.define_promotions_info_list(
                promotion_code = structure_info['message_promotion_code'],
                receivers_id_list = receivers_id_list
                )
        else:
            self.promotions_info_list = ['empty' for i in range(len(receivers_id_list))]
        # print(DT.datetime.now() - noww)
        # noww = DT.datetime.now()
        if (structure_info['message_link_type']) != 'empty' and ('_link' in self.template_arguments_list):
            self.links_info_list = self.define_links_info_list(
                structure_info = structure_info
            )
        else:
            self.links_info_list = ['empty' for i in range(len(receivers_id_list))]
        # print(DT.datetime.now() - noww)
        
        self.replace_functions_switcher = {
            '_customer_name' : self.replace_customer_name,
            '_day' : self.replace_day,
            '_month' : self.replace_month,
            '_link' : self.replace_link,
            '_service_slug' : self.replace_service_slug,
            '_service_rate' : self.replace_service_rate,
            '_promotion_code' : self.replace_promotion_code,
            '_promotion_value' : self.replace_promotion_value,
            '_promotion_ceiling' : self.replace_promotion_ceiling,
            'empty' : self.replace_empty,
        }
    
    def define_structure_info(self,structure_name):
        structure_info = get_scenario_structure_info(structure_name = structure_name)
        scenario_info = get_scenario_info(scenario_name = structure_info['scenario_id'])
        structure_info['message_template_type'] = scenario_info['medium']
        return structure_info

    def define_template_info(self,template_type,template_id):
        
        model = get_template_model(template_type = template_type)
        template = model.objects.get(name = template_id)
        template_text = template.text[:]
        template_arguments_list = template.arguments.split('$')
        return template_text,template_arguments_list
    
    def define_receivers_info_list(self,receivers_id_list):
        receivers_info_list = []
        for receiver_id in receivers_id_list:
            receivers_info_list.append(get_customer_info(customer_id=receiver_id))
        return receivers_info_list

    def define_services_info_list(self,service_slug,receivers_id_list):
        service_info = get_service_info(service_slug=service_slug)
        if not service_info:
            services_info_list = []
            for receiver_id in receivers_id_list:
                services_info_list.append(get_service_info(service_slug = service_slug, customer_id = receiver_id))
        else:
            services_info_list = [service_info for i in range(len(receivers_id_list))]

        return services_info_list
    
    def define_promotions_info_list(self,promotion_code,receivers_id_list):
        promotion_info = get_promotion_info(code = promotion_code)
        promotions_info_list = [promotion_info for i in range(len(receivers_id_list))]
        return promotions_info_list

    def define_links_info_list(self,structure_info):
        links_info_list = []
        for i in range(len(self.receivers_info_list)):
            receiver_info = self.receivers_info_list[i]
            service_info = self.services_info_list[i]

            short_link = self.link_creator.create_short_link(
                message_name = structure_info['name'],

                link_type = structure_info['message_link_type'],
                basic_utm_id = structure_info['link_basic_utm_id'],
                message_utm = structure_info['link_message_utm'],
                personal_utm = structure_info['link_personal_utm'],

                receiver_info = receiver_info,
                service_info = service_info
            )
            links_info_list.append(short_link)
        
        self.link_creator.archive_short_link()
        return links_info_list

    def create_messages(self):
        # noww = DT.datetime.now()
        count = len(self.receivers_info_list)
        messages_text_list = []
        for i in range(count):
            message_text = self.template_text[:]
            for argument in self.template_arguments_list:
                try:
                    message_text = self.replace_functions_switcher[argument](message_text,i,argument)
                except:
                    print(message_text,i,argument)
                    exit()
            messages_text_list.append(message_text)
        # print(DT.datetime.now() - noww)
        return messages_text_list
        
    def replace_customer_name(self,message_text,number,arg):
        # print(self.receivers_info_list)
        # print(number)
        return message_text.replace('_customer_name',self.receivers_info_list[number]['first_name'])
    
    def replace_day(self,message_text,number,arg):

        receiver_info = self.receivers_info_list[number]
        last_login_day = (DT.datetime.now() - receiver_info['last_login']).days
        last_order_date_day = (DT.datetime.now() - receiver_info['last_order_date']).days

        en_day = max(last_login_day,last_order_date_day)
        if en_day < 1:
            en_day = 1

        fa_day = en_to_fa(en_number=en_day)

        return message_text.replace('_day',fa_day)

    def replace_month(self,message_text,number,arg):

        receiver_info = self.receivers_info_list[number]
        last_login_day = (DT.datetime.now() - receiver_info['last_login']).days
        last_order_date_day = (DT.datetime.now() - receiver_info['last_order_date']).days

        en_month = int(max(last_login_day,last_order_date_day)/30)
        if en_month < 1:
            en_month = 1

        fa_month = en_to_fa(en_number=en_month)

        return message_text.replace('_month',fa_month)

    def replace_link(self,message_text,number,arg):
        link = self.links_info_list[number]
        return message_text.replace('_link',link)
    
    def replace_empty(self,message_text,number,arg):
        # link = self.links_info_list[number]
        return message_text
    
    def replace_service_slug(self,message_text,number,arg):
        service_info = self.services_info_list[number]
        return message_text.replace('_service_slug',service_info['persian_name'])

    def replace_service_rate(self,message_text,number,arg):
        service_info = self.services_info_list[number]
        return message_text.replace('_service_rate',service_info['satisfaction'])

    def replace_promotion_code(self,message_text,number,arg):
        promotion_info = self.promotions_info_list[number]
        return message_text.replace('_promotion_code',promotion_info['code'])
    
    def replace_promotion_value(self,message_text,number,arg):
        promotion_info = self.promotions_info_list[number]
        en_value = promotion_info['value']
        fa_value = en_to_fa(en_value)
        return message_text.replace('_promotion_value',fa_value)
    
    def replace_promotion_ceiling(self,message_text,number,arg):
        promotion_info = self.promotions_info_list[number]
        en_ceiling = promotion_info['ceiling']
        fa_ceiling = en_to_fa(en_ceiling)
        fa_ceiling = fa_ceiling[0:-3]
        return message_text.replace('_promotion_ceiling',fa_ceiling)
