from django.apps import AppConfig


class MessagemanagerConfig(AppConfig):
    name = 'MessageManager'
