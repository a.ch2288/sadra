from django.db import models
import uuid

# Create your models here.

class SMS_Template(models.Model):
    
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50,unique=True,default = 'empty')
    number = models.SmallIntegerField(null=True,default = 1)
    text = models.TextField(null=True)
    subject_slugs = models.CharField(max_length=50,null=True)
    arguments = models.CharField(max_length=255,null=True)

    def __str__(self):
        return self.text

    def create_name(self):
        self.name = '{number}.{subject_slugs}'.format(
            subject_slugs = self.subject_slugs,
            number = self.number
            )
        return(self.name)
    
    def check_arguments(self):
        valid_arguments = [
            '_customer_name',
            '_day',
            '_month',
            '_link',
            '_service_slug',
            '_service_rate',
            '_promotion_code',
            '_promotion_value',
            '_promotion_ceiling',
            'empty'
            ]
        arguments_list = self.arguments.split('$')
        for argument in arguments_list:
            if not argument in valid_arguments and argument != 'empty':
                print('Error :{} is not valid.'.format(argument))
                return False
            elif not argument in self.text and argument != 'empty':
                print('Error :there is not {} in text.'.format(argument))
                return False
        
        return True

    def get_fields_type():
        
        pk = 'name'

        fields_type = {
            'name' : 'str',
            'number' : 'int64',
            'text' : 'str',
            'subject_slugs' : 'str',
            'arguments' : 'str'
            }

        return pk,fields_type
    
    def call_name():
        return('SMS_Template')

    class Meta:
        unique_together = ['number', 'subject_slugs']