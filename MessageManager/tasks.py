from celery import shared_task
from .ModelManager import MessageModelManager

@shared_task
def update_model(model_name,sheet_name):
    message_manager = MessageModelManager()
    message_manager.update_model(
        model_name=model_name,
        sheet_name=sheet_name)

@shared_task
def rewrite_model(model_name,sheet_name):
    message_manager = MessageModelManager()
    message_manager.rewrite_model(
        model_name=model_name,
        sheet_name=sheet_name)
