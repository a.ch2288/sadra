import os
import pandas as pd
from ForeignDBConnection.QueryManager import GoogleSheetsConnection
from .models import SMS_Template
from django.conf import settings

def get_template_model(template_type):

    model_switcher = {
        'sms' : SMS_Template
    }

    return model_switcher[template_type]

def check_template_id(template_id,template_type):
    model = get_template_model(template_type = template_type)
    if model.objects.filter(name = template_id).count() > 0:
        return True
    
    return False

def get_template_info(template_name,template_type):
    model = get_template_model(template_type = template_type)
    info = model.objects.filter(name = template_name).values('name','arguments')[0]
    return info

class MessageModelManager:

    def __init__(self):

        self.google_sheets_connection = GoogleSheetsConnection(settings.MESSAGE_MANAGER_GOOGLE_SHEETS_ID)

        # new_promotions = promotions[promotions['code'].isin(result['created_promotions'])]
        # model_field_name = [f.name for f in Promotions._meta.get_fields()]
   
    def read_all_records(self,sheet_name,model):

        data = self.google_sheets_connection.read_all_records(
            sheet_name = sheet_name,
            model = SMS_Template)

        if data.shape[0] == 0 :
            pass
            # Raise Error
        else:
            print(f'- reading data from {sheet_name} had been completed.')
        
        return data

    def get_model(self,model_name):
        switcher = {
            'SMS_Template' : SMS_Template
        }
        model = switcher[model_name]
        return model

    def rewrite_model(self,sheet_name,model_name = None,model = None):

        if model is None:
            model = self.get_model(model_name = model_name)
        
        print(f'- {model.objects.all().count()} records from {model._meta.object_name} has been deleted.')
        model.objects.all().delete()
        self.update_model(
            model = model,
            sheet_name = sheet_name)

    def update_model(self,sheet_name,model_name = None,model = None):

        if model is None:
            model = self.get_model(model_name = model_name)

        data = self.read_all_records(
            sheet_name = sheet_name,
            model = model
            )
        updated_counter = 0
        created_counter = 0
        model_field_name = [f.name for f in model._meta.get_fields()]
        data = data.loc[:,data.columns[data.columns.isin(model_field_name)]]
        for i,row in data.iterrows():
            try:
                obj, created = model.objects.get_or_create(**row)
                name = obj.create_name()
                if obj.check_arguments():
                    obj.save()
                    created_counter += int(created)
                    updated_counter += (1 - int(created))
                else:
                    obj.delete()
                    print(f'Error : wrong arguments in message {name}.')
            except Exception as e:
                print(e)
        print(f'- {created_counter} templates had been created.')
        print(f'- {updated_counter} templates had been updated.')
    


