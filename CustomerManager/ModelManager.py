from .models import *
from ForeignDBConnection.QueryManager import Query
import pandas as pd
import os
import datetime as DT
class CustomerQueryManager(Query):
    pass

class CustomerModelManager:
    
    def __init__(self):

        self.query = CustomerQueryManager(bulk_size = 5000
            ,app_name = 'CustomerManager'
            ,app_path = os.path.dirname(__file__))

        self.bulk_size = 5000

    def insert_new_customers_with_time_limit(self,time_limit_type = 'minutes',time_limit_value = 30):
        
        target_mobile_list = self.write_new_CIIs_with_time_limit(
            time_limit_type=time_limit_type,
            time_limit_value=time_limit_value)
        self.write_target_CLIs(target_mobile_list = target_mobile_list)
        self.write_target_CEIs(target_mobile_list = target_mobile_list)

    def rewrite_all(self):
        self.rewrite_all_CIIs()
        self.rewrite_all_CLIs()
        self.rewrite_all_CEIs()

    # basic functions

    def rewrite_all_PIIs(self):

        Provider_Identification_Information.objects.all().delete()
        print('Provider_Identification_Information has been cleaned.')
        query_file_name = 'all_PIIs.sql'
        self.query.read_file(query_file_name=query_file_name)
        self.query.create_connection(
            product_name = 'ostadkar',
            connection_type = '',
            db_name = 'dispatch')

        new_counter = 0
        for data in self.query.read_big_data():
            new_PIIs = []
            for i,row in data.iterrows():

                PII = Customer_Identification_Information(**row)
                new_PIIs.append(PII)

            Provider_Identification_Information.objects.bulk_create(new_PIIs)
            new_counter = new_counter + len(new_PIIs)
            print('- {} ({}) number of Providers have been added.'.format(len(new_PIIs),new_counter))
    
    def rewrite_all_CIIs(self):

        Customer_Identification_Information.objects.all().delete()
        print('Customer_Identification_Information has been cleaned.')
        query_file_name = 'all_CIIs.sql'
        self.query.read_file(query_file_name=query_file_name)
        self.query.create_connection(
            product_name = 'ostadkar',
            connection_type = '',
            db_name = 'sale')

        new_counter = 0
        for data in self.query.read_big_data():
            new_CIIs = []
            for i,row in data.iterrows():
                mobile = str(row['mobile'])
                user_name = str(row['user_name'])
                first_name = str(row['first_name'])
                last_name = str(row['last_name'])
                created_at = row['created_at']
                updated_at = row['updated_at']

                CII = Customer_Identification_Information(
                    mobile = mobile,
                    user_name = user_name,
                    first_name = first_name,
                    last_name = last_name,
                    created_at = created_at,
                    updated_at = updated_at
                )
                new_CIIs.append(CII)

            Customer_Identification_Information.objects.bulk_create(new_CIIs)
            new_counter = new_counter + len(new_CIIs)
            print('- {} ({}) number of customers has been added.'.format(len(new_CIIs),new_counter))

    def rewrite_all_CEIs(self):

        Customer_Extra_Information.objects.all().delete()
        print('Customer_Extra_Information has been cleaned.')
        
        query_file_name = 'all_CEIs.sql'
        
        self.query.read_file(query_file_name=query_file_name)
        self.query.create_connection(
            product_name = 'ostadkar',
            connection_type = '',
            db_name = 'sale')
        
        data = self.query.read_data()

        customers_mobile_list = list(Customer_Identification_Information.objects.\
            filter(mobile__in = data['mobile']).values_list('mobile',flat=True))
        
        data = data[data['mobile'].isin(customers_mobile_list)]
        total_count = data.count()[0]

        ceis_list = []
        counter = 0
        for i,row in data.iterrows():

            info = row.drop('mobile')
            cei = Customer_Extra_Information(customer_id = row['mobile'],**info)
            ceis_list.append(cei)
            counter += 1

            if counter > 999 :
                Customer_Extra_Information.objects.bulk_create(ceis_list)
                print(f'- {counter} (of {total_count}) customers have been added. (extra information)')
                ceis_list = []
                counter = 0
        
        Customer_Extra_Information.objects.bulk_create(ceis_list)
        print(f'- {counter} (of {total_count}) customers have been added. (extra information)')
        ceis_list = []
        counter = 0

    def rewrite_all_CLIs(self):

        Customer_Login_Information.objects.all().delete()
        print('Customer_Login_Information has been cleaned.')

        query_file_name = 'all_CLIs.sql'
        
        self.query.read_file(query_file_name=query_file_name)
        self.query.create_connection(
            product_name = 'ostadkar',
            connection_type = '',
            db_name = 'gateway')

        data = self.query.read_data()

        customers_mobile_list = list(Customer_Identification_Information.objects.\
            filter(mobile__in = data['customer_id']).values_list('mobile',flat=True))
        
        data = data[data['customer_id'].isin(customers_mobile_list)]
        total_count = data.count()[0]

        clis_list = []
        counter = 0
        for i,row in data.iterrows():

            cli = Customer_Login_Information(**row)
            clis_list.append(cli)
            counter += 1

            if counter > 999 :
                Customer_Login_Information.objects.bulk_create(clis_list)
                print(f'- {counter} (of {total_count}) customers have been added. (login information)')
                clis_list = []
                counter = 0
        
        Customer_Login_Information.objects.bulk_create(clis_list)
        print(f'- {counter} (of {total_count}) customers have been added. (login information)')
        clis_list = []
        counter = 0

    def write_new_CIIs_with_time_limit(self,time_limit_type = 'minutes',time_limit_value = 30):
        
        query_file_name = 'new_CIIs_with_time_limit.sql'
        self.query.read_file(query_file_name=query_file_name)
        query_modifying_text_args = {'_delta' : '{} {}'.format(time_limit_value,time_limit_type)}
        
        self.query.create_connection(
            product_name = 'ostadkar',
            connection_type = '',
            db_name = 'sale')

        data = self.query.read_data(arguments = query_modifying_text_args)

        customers_mobile_list = list(Customer_Identification_Information.objects.\
            filter(mobile__in = data['mobile']).values_list('mobile',flat=True))
        
        data = data[~data['mobile'].isin(customers_mobile_list)]
        
        new_customers_mobile_list = data['mobile']
        
        ciis_list = []
        for i,row in data.iterrows():

            cii = Customer_Identification_Information(**row)
            ciis_list.append(cii)

        Customer_Identification_Information.objects.bulk_create(ciis_list)
        print('- {} customers have been added. (Customer_Identification_Information)'.format(len(ciis_list)))

        # print('\n')

        return (new_customers_mobile_list)
    
    def write_target_CLIs(self,target_mobile_list,end = None):

        query_file_name = 'target_CLIs.sql'
        query_modifying_text_args = {'_target_mobile_list' : "','".join(target_mobile_list)}
        
        self.query.read_file(query_file_name=query_file_name)
        self.query.create_connection(
            product_name = 'ostadkar',
            connection_type = '',
            db_name = 'gateway')

        data = self.query.read_data(arguments = query_modifying_text_args)
        
        new_CLIs = []
        updated_CLIs = []
        for i,row in data.iterrows():

            pre_cli = Customer_Login_Information.objects.filter(customer_id = row['mobile'])

            info = row.drop('mobile')
            cli = Customer_Login_Information(customer_id = row['mobile'],**info)

            if len(pre_cli) == 0:
                new_CLIs.append(cli)
            elif len(pre_cli) == 1:
                updated_CLIs.append(cli)

        if end == None:
            end = len(new_CLIs)
                
        Customer_Login_Information.objects.bulk_create(new_CLIs)
        print('- {} ({}) customers have been added. (Login_Information)'.format(len(new_CLIs),end))

        Customer_Login_Information.objects.bulk_update(updated_CLIs,['first_login'
                                                                    ,'last_login'
                                                                    ,'login_count'])
        print('- {} ({}) customers have been updated. (Login_Information)'.format(len(updated_CLIs),end))
        
    def write_target_CEIs(self,target_mobile_list,end = None):
        
        query_file_name = 'target_CEIs.sql'
        query_modifying_text_args = {'_target_mobile_list' : "','".join(target_mobile_list)}
        
        self.query.read_file(query_file_name=query_file_name)
        self.query.create_connection(
            product_name = 'ostadkar',
            connection_type = '',
            db_name = 'sale')
        
        data = self.query.read_data(arguments = query_modifying_text_args)

        new_CEIs = []
        updated_CEIs = []

        for i,row in data.iterrows():

            pre_cii = Customer_Extra_Information.objects.filter(customer_id = row['mobile'])

            info = row.drop('mobile')
            cei = Customer_Extra_Information(customer_id = row['mobile'],**info)

            if len(pre_cii) == 0:
                new_CEIs.append(cei)
            else:
                updated_CEIs.append(cei)

        if end == None:
            end = len(new_CEIs)
            
        Customer_Extra_Information.objects.bulk_create(new_CEIs)
        print('- {0} ({1}) customers have been added. (Extra_Information)'.format(len(new_CEIs),end))
        Customer_Extra_Information.objects.bulk_update(updated_CEIs,['city'
                                                                    ,'all_orders_service'
                                                                    ,'done_orders_service'
                                                                    ,'canceled_orders_service'
                                                                    ,'all_orders_date'
                                                                    ,'done_orders_date'
                                                                    ,'canceled_orders_date'
                                                                    ,'canceled_orders_reason'
                                                                    ,'used_promotions'])
        print('- {0} ({1}) customers have been updated. (Extra_Information)'.format(len(updated_CEIs),end))

def read_customers_list_with_time_limit(time_limit = None,time_limit_on = 'last_login'):

    if time_limit == None:
        date_limit = DT.datetime.now() - DT.timedelta(minutes=6*30*24*60)
    else:
        date_limit = DT.datetime.now() - DT.timedelta(minutes=int(time_limit))

    if time_limit_on == 'last_login':
        customer_mobile = Customer_Login_Information.objects.filter(last_login__gte = date_limit).values_list('customer_id', flat=True)
        customer_mobile_list = list(customer_mobile)
    elif time_limit_on == 'first_login':
        customer_mobile = Customer_Login_Information.objects.filter(first_login__gte = date_limit).values_list('customer_id', flat=True)
        customer_mobile_list = list(customer_mobile)
    else:
        print('Error in User_manager.read_customers_list_with_time_limit : time_limit_on is wrong!')
        customer_mobile_list = []
    
    return customer_mobile_list

def get_customer_info(customer_id):

    info = list(Customer_Identification_Information.objects.filter(mobile = customer_id).values('mobile','first_name'))[0]
    try:
        last_login = list(Customer_Login_Information.objects.filter(customer_id = customer_id).values('last_login'))[0]
    except:
        last_login = { 'last_login' : DT.datetime.now()}
        
    extra_info = list(Customer_Extra_Information.objects.filter(customer_id = customer_id).values('city','last_order_date','last_order_service'))
    if len(extra_info) == 0 :
        extra_info = {
            'city' : 'tehran',
            'last_order_date' : DT.datetime.now(),
            'last_order_service' : 'ostadkar'
        }
    else:
        extra_info = extra_info[0]
        extra_info['last_order_date'] = DT.datetime.strptime(extra_info['last_order_date'], '%Y-%m-%d %H:%M:%S.%f')
    info.update(last_login)
    info.update(extra_info)
    
    return info