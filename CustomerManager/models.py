from django.db import models

# Create your models here.

class Provider_Identification_Information(models.Model):

    mobile = models.CharField(primary_key=True, unique=True, max_length=11)
    user_name = models.CharField(max_length=50, null = True)
    first_name = models.CharField(max_length=50, null = True)
    last_name = models.CharField(max_length=50, null = True)
    created_at = models.DateTimeField(null = True)

class Customer_Identification_Information(models.Model):
    mobile = models.CharField(primary_key=True, unique=True, max_length=11)
    user_name = models.CharField(max_length=50, null = True)
    first_name = models.CharField(max_length=50, null = True)
    last_name = models.CharField(max_length=50, null = True)
    created_at = models.DateTimeField(null = True)
    updated_at = models.DateTimeField(null = True)

class Customer_Login_Information(models.Model):
    customer = models.OneToOneField(
        Customer_Identification_Information, 
        on_delete = models.CASCADE, 
        primary_key = True
        )
    first_login = models.DateTimeField(null = True)
    last_login = models.DateTimeField(null = True)
    login_count = models.IntegerField(null = True)

class Customer_Extra_Information(models.Model):
    customer = models.OneToOneField(
        Customer_Identification_Information,
        on_delete = models.CASCADE, 
        primary_key = True
        )
        
    city = models.CharField(max_length=50, null = True)

    all_orders_service = models.CharField(max_length=255, null = True)
    done_orders_service = models.CharField(max_length=255, null = True)
    canceled_orders_service = models.CharField(max_length=255, null = True)

    all_orders_date = models.CharField(max_length=255, null = True)
    done_orders_date = models.CharField(max_length=255, null = True)
    canceled_orders_date = models.CharField(max_length=255, null = True)

    canceled_orders_reason = models.CharField(max_length=255, null = True)

    used_promotions = models.CharField(max_length=255, null = True)

    # all_orders_satisfiction = models.CharField(max_length=255, null = True)
    # done_orders_satisfiction = models.CharField(max_length=255, null = True)
    # canceled_orders_satisfiction = models.CharField(max_length=255, null = True)

    # last five orders will been saved
    # ex : 'moving','carpetcleaning','housecleaning','...','...'
    
    last_order_service = models.CharField(max_length=255, null = True)
    last_order_date = models.CharField(max_length=255, null = True)
    last_order_status = models.CharField(max_length=50, null = True)
