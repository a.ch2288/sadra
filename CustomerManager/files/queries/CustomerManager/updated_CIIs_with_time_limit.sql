-- Updated Customers Identification Information
select
    c.mobile
    ,c.user_name
    ,c.first_name
    ,c.last_name
    ,c.created_at
    ,c.updated_at
from
    customers as c
where 
    c.mobile <> '09353942996'
    and c.updated_at > current_timestamp - interval '_delta'

order by
    c.mobile
