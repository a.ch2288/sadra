-- All Provider Identification Information (dispatch)
select p.mobile::text
     , p.user_name::text
     , p.first_name::text
     , p.last_name::text
     , p.created_at
from providers as p
where p.mobile not in ('09353942996','.9120394148')
order by
    p.mobile
