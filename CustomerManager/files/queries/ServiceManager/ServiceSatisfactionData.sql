WITH
    customer_satisfactions AS (
	SELECT
	    o.service_slug
		,count(c.id) "HaveComment"
	    ,COUNT(c.id) FILTER(WHERE is_satisfied = 'true') "SatisfiedCount"
	FROM orders o
	RIGHT JOIN comments c ON o.number = c.order_number
	WHERE
	    o.created_at > '2020-01-01'
	    and c.is_satisfied is not null
	GROUP BY
	    o.service_slug
    )

SELECT
	cs.service_slug as slug,
    round((cs."SatisfiedCount"*1.0)/(cs."HaveComment")*100,2) as satisfaction

FROM customer_satisfactions cs

