from celery import shared_task
from .ModelManager import CustomerModelManager
from datetime import datetime

check = True

@shared_task
def insert_new_customers_with_time_limit(time_limit_type = None,time_limit_value = None):
    global check
    customer_manager = CustomerModelManager()
    if 20 < datetime.now().hour or datetime.now().hour < 3:
        print('rest time! :)')
        check = True
    else:

        if check == True:
            time_limit_type = 'days'
            time_limit_value = 1
            check = False

        if time_limit_type is not None and time_limit_value is not None:
            customer_manager.insert_new_customers_with_time_limit(
                time_limit_type = time_limit_type,
                time_limit_value = time_limit_value
                )
        else:
            pass

@shared_task
def rewrite_all_CEI_and_CLI():
    customer_managerr = CustomerModelManager()
    customer_managerr.insert_new_customers_with_time_limit(
        time_limit_type = 'days',
        time_limit_value = 2
        )
    customer_managerr.rewrite_all_CEIs()
    customer_managerr.rewrite_all_CLIs()