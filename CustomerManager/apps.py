from django.apps import AppConfig


class CustomermanagerConfig(AppConfig):
    name = 'CustomerManager'
