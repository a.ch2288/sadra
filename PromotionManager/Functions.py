import csv
import json
import requests
import random
import string
import sys
import os
import datetime 
from django.conf import settings
from .models import Promotions

class PromotionCreator:

    def __init__(self):
        pass  

    def create_promotions(self):
        
        auth_header = settings.BACK_OFFICE_AUTH_HEADER
        base_url = settings.BACK_OFFICE_BASE_URL
        url = base_url + '/vouchers'

        voucehr_time = self.get_voucher_time()

        promotions = list(Promotions.objects.filter(is_created = False))
        created_promotions_code = []
        for promotion in promotions:
            
            voucher = {
                "code": promotion.code,
                "title": promotion.title,
                "description": promotion.description,
                "message": promotion.message,
                "type": promotion.ptype,
                "value": promotion.value,
                "ceiling": promotion.ceiling,
                "budget": promotion.budget,
                "rules": promotion.rules,
                "start_at": '{}{}'.format(promotion.start_at,voucehr_time),
                "finish_at": '{}{}'.format(promotion.finish_at,'T19:30:00.000Z'),
                "total_use_count": promotion.total_use_count,
                "customer_use_count": promotion.customer_use_count,
                "is_enabled": True,
            }
            # print(voucher)

            response = requests.post(url, data=json.dumps(voucher), headers=auth_header)

            if response.status_code == 201:
                # print(f'{voucher["code"]} successfully entered!')
                created_promotions_code.append(promotion.code)
            else:
                print(response.status_code)
                print(response.text)
        
        Promotions.objects.filter(code__in = created_promotions_code).update(is_created = True)
        print(f'- {len(created_promotions_code)} (of {len(promotions)}) Promotions have been created.')

    def get_voucher_time(self):

        current_date = datetime.datetime.now()
        current_date +=  datetime.timedelta(hours=1)
        current_time = current_date.time()
        next_hour_time = current_time.replace(second=0, microsecond=0, minute=0, hour=current_time.hour)
        return(f'T{next_hour_time}Z')

    def update_all(self):

        self.create_promotions()


def get_token():
    mobile = '09190051230'
    base_url = settings.BACK_OFFICE_BASE_URL

    requests.post(f"{base_url}/authenticates", json={"mobile": mobile})

    code = input("Enter code: ")

    res = requests.post(f"{base_url}/authenticates",
                        json={"code": code, "mobile": mobile})
    auth_data = res.json()

    print(auth_data)

if __name__ == "__main__":
    
    a = PromotionCreator()
    a.update_all()

    