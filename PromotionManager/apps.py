from django.apps import AppConfig


class PromotionmanagerConfig(AppConfig):
    name = 'PromotionManager'
