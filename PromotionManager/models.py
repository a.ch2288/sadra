from django.db import models
from django.utils import timezone

# Create your models here.

class Promotions(models.Model):
    owner_choices = (
        ('GrowthTeam', 'GrowthTeam'),
        ('MarketingTeam', 'MarketingTeam'),
    )
    
    code = models.CharField(max_length=255,primary_key=True)
    title = models.CharField(max_length=255,null=True)
    description = models.CharField(max_length=255,null=True)
    message = models.CharField(max_length=255, default="Message",null=True)
    ptype = models.CharField(max_length=255,null=True)
    value = models.CharField(max_length=255,null=True)
    ceiling = models.CharField(max_length=255,null=True)
    budget = models.CharField(max_length=255,null=True)
    rules  = models.CharField(max_length=255 , default='{}',null=True)
    start_at = models.DateField(null=True)
    finish_at = models.DateField(null=True)
    total_use_count = models.IntegerField(null=True)
    customer_use_count = models.IntegerField(default=1,null=True)
    is_enabled = models.BooleanField(default=False)
    is_created = models.BooleanField(default=False)
    owner = models.CharField(max_length=50,choices=owner_choices)
    created_at = models.DateTimeField(default=timezone.now)

    def calls_name():
        return('Promotions')