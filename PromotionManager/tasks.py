from celery import shared_task
from .ModelManager import PromotionModelManager


@shared_task
def update_all():
    promotion_manager = PromotionModelManager()
    promotion_manager.update_all()
