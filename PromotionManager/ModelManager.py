import csv
import json
import requests
import random
import string
import sys
import os
import datetime 
import pandas as pd

from ForeignDBConnection.QueryManager import GoogleSheetsConnection
from django.conf import settings
from .models import Promotions

def check_promotion_code(code):

    if code == 'empty':
        return True
    
    if Promotions.objects.filter(code = code).count() != 0 :
        
        return True
    
    return False

def get_promotion_info(code,customer_id = None):
    if code == 'empty':
        return {}

    info = list(Promotions.objects.filter(code = code).values('code','value','ceiling'))[0]
    
    return info

class PromotionModelManager:

    def __init__(self,sheet_name = None):

        if sheet_name is None:
            sheet_name = 'Promotion'

        self.google_sheets_connection = GoogleSheetsConnection(settings.PROMOTION_MANAGER_GOOGLE_SHEETS_ID)
        self.read_all_records(sheet_name = sheet_name)

    def read_all_records(self,sheet_name):

        columns_type = {
            'value' : 'str',
            'ceiling' : 'str',
            'budget' : 'str',
            'total_use_count': 'int',
            'customer_use_count': 'int',
            'is_enabled' : 'bool'}
        self.google_sheets_connection.read_all_records(
            sheet_name = sheet_name,
            columns_type = columns_type
            )
        
    def update_all(self):

        data = self.google_sheets_connection.data

        new_promotions = data[data['status'] == 'New']
        result = self.import_new_promotions(promotions = new_promotions)

        self.google_sheets_connection.update_cells(
            column_name = 'status',
            index_range = data.index[data['code'].isin(result['created_promotions'])].tolist(),
            value = 'Created')

        self.google_sheets_connection.update_cells(
            column_name = 'status',
            index_range = data.index[data['code'].isin(result['errors'])].tolist(),
            value = 'Error')
        
        changed_promotions = data[data['status'] == 'Changed']
        result = self.update_changed_promotions(promotions = changed_promotions)

        self.google_sheets_connection.update_cells(
            column_name = 'status',
            index_range = data.index[data['code'].isin(result['updated_promotions'])].tolist(),
            value = 'Updated')

        self.google_sheets_connection.update_cells(
            column_name = 'status',
            index_range = data.index[data['code'].isin(result['errors'])].tolist(),
            value = 'Permission denied')
        
        print('- PromotionModelManager: updating promotions have been completed.')
    
    def import_new_promotions(self,promotions):

        if promotions.shape[0] == 0:
            return ({'created_promotions' : [],'errors' : []})

        available_promotions = list(Promotions.objects.filter(code__in = promotions['code'].tolist())\
            .values_list('code',flat=True))
        
        new_promotions = promotions[~promotions['code'].isin(available_promotions)]

        result = self.create_new_promotions(promotions = new_promotions)
        result['errors'].extend(available_promotions)

        new_promotions = promotions[promotions['code'].isin(result['created_promotions'])]
        model_field_name  = [f.name for f in Promotions._meta.get_fields()]
        new_promotions = new_promotions.loc[:,new_promotions.columns.isin(model_field_name)]
        bulk_list =[]
        for i,row in new_promotions.iterrows(): 
            promotion = Promotions(**row)
            bulk_list.append(promotion)
        Promotions.objects.bulk_create(bulk_list)

        print(f'- {len(bulk_list)} (of {promotions.shape[0]}) Promotions have been imported to SadRa database.')

        return(result)

    def create_new_promotions(self,promotions):
        
        auth_header = settings.BACK_OFFICE_AUTH_HEADER
        base_url = settings.BACK_OFFICE_BASE_URL
        url = base_url + '/vouchers'

        voucehr_time = self.get_voucher_time()

        created_promotions = []
        errored_promotions = []
        for i,promotion in promotions.iterrows():
            
            voucher = {
                "code": promotion['code'],
                "title": promotion['title'],
                "description": promotion['description'],
                "message": promotion['message'],
                "type": promotion['ptype'],
                "value": promotion['value'],
                "ceiling": promotion['ceiling'],
                "budget": promotion['budget'],
                "rules": promotion['rules'],
                "start_at": '{}{}'.format(promotion['start_at'],voucehr_time),
                "finish_at": '{}{}'.format(promotion['finish_at'],'T19:30:00.000Z'),
                "total_use_count": promotion['total_use_count'],
                "customer_use_count": promotion['customer_use_count'],
                "is_enabled": promotion['is_enabled'],
            }

            response = requests.post(url, data=json.dumps(voucher), headers=auth_header)

            if response.status_code == 201:
                created_promotions.append(promotion['code'])
            else:
                print(response)
                errored_promotions.append(promotion['code'])
        
        print(f'- {len(created_promotions)} (of {promotions.shape[0]}) Promotions have been created.')

        result = {
            'created_promotions' : created_promotions,
            'errors' : errored_promotions
        }

        return(result)

    def get_voucher_time(self):

        current_date = datetime.datetime.now()
        current_date +=  datetime.timedelta(hours=1)
        current_time = current_date.time()
        next_hour_time = current_time.replace(second=0, microsecond=0, minute=0, hour=current_time.hour)
        return(f'T{next_hour_time}Z')

    def update_changed_promotions(self,promotions):

        if promotions.shape[0] == 0:
            return ({'updated_promotions' : [],'errors' : []})
        
        available_promotions = list(Promotions.objects.filter(code__in = promotions['code'].tolist())\
            .values_list('code',flat=True))
        
        non_existent_promotions = promotions[~promotions['code'].isin(available_promotions)]
        
        available_promotions = promotions[promotions['code'].isin(available_promotions)]

        result = self.update_promotions(promotions = available_promotions)
        result['errors'].extend(non_existent_promotions['code'].tolist())
        
        updated_promotions = promotions[promotions['code'].isin(result['updated_promotions'])]
        model_field_name  = [f.name for f in Promotions._meta.get_fields()]
        updated_promotions = updated_promotions.loc[:,updated_promotions.columns.isin(model_field_name)]
        bulk_list =[]
        for i,row in updated_promotions.iterrows(): 
            promotion = Promotions(**row)
            bulk_list.append(promotion)
        Promotions.objects.bulk_update(bulk_list,[
            "title",
            "description",
            "message",
            "ptype",
            "value",
            "ceiling",
            "budget",
            "rules",
            "start_at",
            "finish_at",
            "total_use_count",
            "customer_use_count",
            "is_enabled"]
            )

        print(f'- {len(bulk_list)} (of {promotions.shape[0]}) Promotions have been updated in SadRa database.')

        return(result)

    def update_promotions(self,promotions):

        auth_header = settings.BACK_OFFICE_AUTH_HEADER
        base_url = settings.BACK_OFFICE_BASE_URL
        url = base_url + '/vouchers'

        voucehr_time = self.get_voucher_time()

        updated_promotions = []
        errored_promotions = []
        for i,promotion in promotions.iterrows():

            promotion_code = promotion['code']
            url = f'{url}/{promotion_code}'

            voucher = {
                "code": promotion['code'],
                "title": promotion['title'],
                "description": promotion['description'],
                "message": promotion['message'],
                "type": promotion['ptype'],
                "value": promotion['value'],
                "ceiling": promotion['ceiling'],
                "budget": promotion['budget'],
                "rules": promotion['rules'],
                "start_at": '{}{}'.format(promotion['start_at'],voucehr_time),
                "finish_at": '{}{}'.format(promotion['finish_at'],'T19:30:00.000Z'),
                "total_use_count": promotion['total_use_count'],
                "customer_use_count": promotion['customer_use_count'],
                "is_enabled": promotion['is_enabled'],
            }


            response = requests.put(url, data=json.dumps(voucher), headers=auth_header)

            if response.status_code == 204:
                updated_promotions.append(promotion['code'])
            else:
                errored_promotions.append(promotion['code'])

        print(f'- {len(updated_promotions)} (of {promotions.shape[0]}) Promotions have been updated.')

        result = {
            'updated_promotions' : updated_promotions,
            'errors' : errored_promotions
        }

        return(result)