import os
from django.db.models import Q
import datetime as DT
import random
import time

from .models import *
from SMSManager.models import Receiver
from .FilterManager import Filter_manager

def read_target_people(scenario_name):

    scenario = Scenario.objects.get(name = scenario_name)
    target_people = scenario.target_people
    app_name,segment_type,segment_name = target_people.split('.')

    if app_name == 'RFMModel':
        if segment_type == 'Order_Base_Segment':
            from RFMModel.models import Order_Base_Segment
            segment = Order_Base_Segment.objects.filter(name = segment_name)
            if len(segment) == 0 :
                print('Warning : sgment_name = {} is not correct')
            else:
                segment = segment[0]
            target_people = segment.customer_rfm_information_set.all()

    elif app_name == 'LazarusModel':
        if segment_type == 'Segment':
            from LazarusModel.models import Segment
            segment = Segment.objects.filter(name = segment_name)
            if len(segment) == 0 :
                print('Warning : sgment_name = {} is not correct')
            else:
                segment = segment[0]
            target_people = segment.customer_information_set.all()

    elif app_name == 'CrossSellModel':
        if segment_type == 'Service_Base_Segment':
            from CrossSellModel.models import Service_Base_Segment
            segment = Service_Base_Segment.objects.filter(name = segment_name)
            if len(segment) == 0 :
                print('Warning : sgment_name = {} is not correct')
            else:
                segment = segment[0]
            target_people = segment.customer_crosssell_information_set.all()
            
    elif app_name == 'CustomerManager':
        if segment_type == 'RecentlyAcquired':
            from CustomerManager.models import Customer_Identification_Information
            time_limit = DT.datetime.now() - DT.timedelta(minutes=30)
            target_people = Customer_Identification_Information.objects.\
                filter(created_at__gte = time_limit)
    
    q0 = Q(mobile__in = Blocked_Member.objects.all().values_list('blocked_mobile'))
    target_people = target_people.exclude(q0).values_list('mobile',flat = True)
    target_people_mobile_set = set(list(target_people))
    return target_people_mobile_set

def dividing_list(primary_list,parts_len = None,parts_count = None ):

    random.shuffle(primary_list)
    primary_list_len = len(primary_list)

    if parts_count == 0 or primary_list_len == 0:
        return [[]]

    if not parts_len:
        parts_len = int(primary_list_len/parts_count) + int(primary_list_len%parts_count != 0)
    
    if primary_list_len < parts_len:
        return [primary_list]
        
    divided_list = [primary_list[i:i+parts_len] for i in range(0,primary_list_len,parts_len)]
    
    
    return (divided_list)

def select_scenario(name = None,structure_type = None,medium = None,is_active = None,sample = None):
    if sample:
        scenario_name = sample.split('.')[0]
        sample_scenario = Scenario.objects.get(name = scenario_name)
        if structure_type is not None:
            structure_type = sample_scenario.structure_type
        if medium is not None:
            medium = sample_scenario.medium
        if is_active is not None:
            is_active = sample_scenario.is_active
    
    scenarios = Scenario.objects.all()

    if name is not None:
        scenarios = scenarios.filter(name = name)

    if structure_type is not None:
        scenarios = scenarios.filter(structure_type = structure_type)
    
    if medium is not None:
        scenarios = scenarios.filter(medium = medium)
    
    if is_active is not None:
        scenarios = scenarios.filter(is_active = is_active)

    return scenarios

class MemberManager:

    def __init__(self):

        self.date = str(DT.datetime.now())[0:10]
        self.bulk_size = 5000
        self.filter_manager = Filter_manager()
        # self.service_manager = Service_manager()
        # self.k_conn = KConnection(settings.SENDER,settings.API_KEY)

    # updating scenario
    def update_scenario_members(self,scenario_name):

        # aval ezafe haro mirize door ( oonai ke alan dakhelesh hastan vali dakhele segment nistan)
        # bad harchi dakhele segment hast ro mirize too khodes
        print('- updating of {} had been started.'.format(scenario_name))
        self.update_scenario_output_members(scenario_name = scenario_name)

        self.update_scenario_members_action(scenario_name = scenario_name)

        target_people_mobile_set = read_target_people(scenario_name = scenario_name)
        self.update_members_scenario_with_mobile_set(
            scenario_name = scenario_name,
            members_mobile_set = target_people_mobile_set
            )
        
        self.update_scenario_members_action_need(scenario_name = scenario_name)

    def update_scenario_output_members(self,scenario_name):

        # member hAi ke dakhale scenario hastan vali dar segment nistan ro hazf mikonam
        print('scenario output deleting.')
        scenario_output_members_mobile_set = self.select_scenario_members_mobile_set(
            scenario_name = scenario_name,
            member_type = 'output')

        self.delete_members(
            members_mobile_set = scenario_output_members_mobile_set,
            layer_name = scenario_name,
            archive = True
            )

    def update_scenario_members_action(self,scenario_name):
        print(f"\n- updating members' action in {scenario_name}.")
        structures = Scenario_Structure.objects.filter(scenario_id = scenario_name).order_by('phase','name')
        for structure in structures:
            q0 = Q(status = structure)
            q1 = Q(action = False)
            members_mobile_list = list(Scenario_Member.objects.filter(q0 & q1).values_list('mobile',flat=True))
            
            q2 = Q(mobile__in = members_mobile_list)
            q3 = Q(message_name = structure.name)

            updated_members_mobile_list = list(Receiver.objects.filter(q2 & q3).values_list('mobile',flat=True))

            q4 = Q(mobile__in = updated_members_mobile_list)
            updated_members_mobile_count = Scenario_Member.objects.filter(q0 & q4).count()
            Scenario_Member.objects.filter(q0 & q4).update(action = True)
            print('-- {} members in {} had been updated (action).'.format(updated_members_mobile_count,structure.name))
    
    def update_scenario_members_action_need(self,scenario_name):
        print(f"\n- updating members' action_need in {scenario_name}.")
        structures = list(Scenario_Structure.objects.filter(scenario_id = scenario_name).order_by('phase','name'))
        for structure in structures:
            q0 = Q(status_id = structure.name)
            q1 = Q(action_need = False)
            members_name_set = set(list(Scenario_Member.objects.filter(q0 & q1).values_list('name',flat=True)))
            filter_name = structure.message_sending_filter
            if len(members_name_set) != 0:
                filtered_members_mobile_set = self.filter_manager.modifying_set(members_name_set,filter_name)
                # print(len(filtered_members_mobile_set))
                q2 = Q(mobile__in = list(filtered_members_mobile_set))
                q3 = Q(status_id = structure.name)
                Scenario_Member.objects.filter(q2 & q3).update(action_need = True)
            else:
                filtered_members_mobile_set = set([])
            print('-- {} (of {}) members in {} had been updated (action_need).'.format(
                len(filtered_members_mobile_set),
                len(members_name_set),
                structure.name)
                )
    
    # updating phase
    def update_phase_members(self,phase_name):
        # baraye waiting ha action ro update mikone
        # input ro moshakhas va ezafe mikone
        ## input phase 1 = input scenario va baraye baghiye input i  = output (i-1)

        self.update_phase_waiting_members(phase_name = phase_name)
        phase_state = phase_name.split('.')
        phase_number = int(phase_state[-1])
        if phase_number != 1 :
            self.update_phase_input_members(phase_name = phase_name)
        else:
            print('Warning : phase_number = 1 cant update in this way!')

    def update_phase_waiting_members(self,phase_name):
        
        phase_state = phase_name.split('.')
        scenario_name = phase_state[0]
        scenario = Scenario.objects.get(name = scenario_name)
        if scenario.medium == 'sms':

            phase_waiting_members_mobile_set = self.select_phase_members(phase_name=phase_name,member_type='waiting')
            phase_updated_members_mobile_set = self.filter_manager.modifying_set(
                initial_set = phase_waiting_members_mobile_set,
                filter_name = 'SMSReceivers$10'
                )
            condition0 = Q(mobile__in = list(phase_updated_members_mobile_set))
            condition1 = Q(scenario_id = scenario_name)
            phase_updated_members = Scenario_Member.objects.filter(condition0 & condition1)
            
            phase_updated_members.update(action = True)

    def update_phase_input_members(self,phase_name):

        phase_input_members_mobile_set = self.select_phase_members(
            phase_name  = phase_name,
            member_type = 'input',
            value_set = 'mobile')

        self.update_members_phase_with_mobile_set(
            members_mobile_set = phase_input_members_mobile_set
            ,target_phase_name = phase_name)

    # updating members 
    def update_members_scenario_with_mobile_set(self,scenario_name,members_mobile_set):
        print('- members scenario updating had been stared.')
        remained_members_mobile_set = set(members_mobile_set)

        sub_scenario_count = self.count_next_layer_structures(current_layer_name = scenario_name)
        for sub_scenario_number in range(1,1 + sub_scenario_count):

            sub_scenario_name = '{scenario_name}.{sub_scenario_number}'.format(
                scenario_name = scenario_name,
                sub_scenario_number = sub_scenario_number
                )
            
            remained_members_mobile_set = self.update_members_sub_scenario_with_mobile_set(
                target_sub_scenario_name = sub_scenario_name,
                members_mobile_set = remained_members_mobile_set
                )
        
        if len(remained_members_mobile_set) != 0 :
            print('Warning : remained_members_mobile_set is not empty!')

    def update_members_sub_scenario_with_mobile_set(self,target_sub_scenario_name,members_mobile_set):

        members_mobile_set = set(members_mobile_set)

        remained_members_mobile_set, sub_scenario_members_mobile_set = self.separate_sub_layer_members_mobile_set(
            sub_layer_name = target_sub_scenario_name,
            members_mobile_set = members_mobile_set
        )

        phase_structures_count = self.count_next_layer_structures(current_layer_name = target_sub_scenario_name)
        
        for phase_number in range(phase_structures_count,1,-1):

            phase_name = '{sub_scenario_name}.{phase_number}'.format(
                sub_scenario_name = target_sub_scenario_name,
                phase_number = phase_number
                )
            
            sub_scenario_members_mobile_set = self.update_members_phase_with_mobile_set(
                target_phase_name = phase_name,
                members_mobile_set = sub_scenario_members_mobile_set,
                input_check = True
            )

        phase_name = '{sub_scenario_name}.{phase_number}'.format(
            sub_scenario_name = target_sub_scenario_name,
            phase_number = 1
            )
            
        self.update_members_first_phase_with_mobile_set(
            target_first_phase_name = phase_name,
            members_mobile_set = sub_scenario_members_mobile_set
        )

        return remained_members_mobile_set

    def update_members_first_phase_with_mobile_set(self,target_first_phase_name,members_mobile_set):
        
        phase_current_members_mobile_set = self.select_phase_members(
            phase_name = target_first_phase_name,
            member_type = 'current',
            value_set = 'mobile'
            )
        # print(f'{len(phase_current_members_mobile_set)}')
        members_mobile_set = members_mobile_set - phase_current_members_mobile_set
        
        target_structure = Scenario_Structure.objects.\
            filter(name__startswith = target_first_phase_name).order_by('phase')[0]
        target_scenario = target_structure.scenario
        structure_type = target_scenario.structure_type
        
        if structure_type in ['weekly','online']:
            q0 = Q(structure_type = target_scenario.structure_type)
            q1 = Q(medium = target_scenario.medium)
            scenarios = Scenario.objects.filter(q0 & q1)
            structures = Scenario_Structure.objects.filter(scenario__in = scenarios)
        elif structure_type == 'cross_sell':
            structures = target_scenario.scenario_structure_set.all()

        q0 = Q(status__in = structures)
        q1 = Q(mobile__in = list(members_mobile_set))
        condition = Q(q1 & q0)

        old_members_mobile_set = set(list(Scenario_Member.objects.filter(condition).values_list('mobile',flat= True)))

        print('-- first_phase old members.')
        # if len(old_members_mobile_set) != 0 :
        #     print(old_members_mobile_set)
        self.delete_members(
            members_mobile_set = old_members_mobile_set,
            layer_name = target_first_phase_name,
            archive = True)
        print('-- first_phase new members.')
        
        self.update_members_phase_with_mobile_set(
            target_phase_name = target_first_phase_name,
            members_mobile_set = members_mobile_set,
            input_check = False)

    def update_members_phase_with_mobile_set(self,target_phase_name,members_mobile_set,input_check = True):
        print('- members phase updating had been stared.')

        phase_current_members_mobile_set = self.select_phase_members(
            phase_name = target_phase_name,
            member_type = 'current',
            value_set = 'mobile'
            )
        # print(f'{len(members_mobile_set)}')
        members_mobile_set = set(members_mobile_set) - phase_current_members_mobile_set
        # print(f'{len(phase_current_members_mobile_set)}-{len(members_mobile_set)}')
        if input_check:
            phase_input_members_mobile_set = self.select_phase_members(
                phase_name  = target_phase_name,
                member_type = 'input',
                value_set = 'mobile'
                )

            remained_members_mobile_set = members_mobile_set - phase_input_members_mobile_set
            phase_remained_members_mobile_set = members_mobile_set - remained_members_mobile_set
            
            # print(len(set(members_mobile_set)),len(phase_remained_members_mobile_set),len(phase_waiting_members_mobile_set),len(remained_members_mobile_set))
        else:
            remained_members_mobile_set = set()
            phase_remained_members_mobile_set = members_mobile_set

        sub_phase_structures_count = self.count_next_layer_structures(current_layer_name = target_phase_name)
        
        for sub_phase_number in range(1 , 1 + sub_phase_structures_count):

            sub_phase_name = '{target_phase_name}.{sub_phase_number}'.format(
                target_phase_name = target_phase_name, 
                sub_phase_number = sub_phase_number
                )
            phase_remained_members_mobile_set = self.update_members_sub_phase_with_mobile_set(
                target_sub_phase_name = sub_phase_name,
                members_mobile_set = phase_remained_members_mobile_set
                )
        
        if len(phase_remained_members_mobile_set) != 0 :
            print('Warning : phase_remained_members_mobile_set is not empty!')
        
        return remained_members_mobile_set
    
    def update_members_sub_phase_with_mobile_set(self,target_sub_phase_name,members_mobile_set):

        members_mobile_set = set(members_mobile_set)

        remained_members_mobile_set, sub_phase_members_mobile_set = self.separate_sub_layer_members_mobile_set(
            sub_layer_name = target_sub_phase_name,
            members_mobile_set = members_mobile_set
        )

        group_structures_count = self.count_next_layer_structures(current_layer_name = target_sub_phase_name)

        members_mobile_divided_list = dividing_list(
            primary_list = list(sub_phase_members_mobile_set),
            parts_count = group_structures_count
            )

        if len(members_mobile_divided_list) != group_structures_count:
            print('Warning : members_mobile_divided_list not equal with group_structures_count!')

        for group_number in range(1,1 + min(len(members_mobile_divided_list),group_structures_count)):

            group_name = '{sub_phase_name}.{group_number}'.format(
                sub_phase_name = target_sub_phase_name,
                group_number = group_number
                )
            group_members_mobile_set = set(members_mobile_divided_list[group_number-1])
            group_remained_members_mobile_set = self.update_members_group_with_mobile_set(
                target_group_name = group_name,
                members_mobile_set = group_members_mobile_set
            )

            if len(group_remained_members_mobile_set) != 0:
                print('Warning : There are members in group_remained_members_mobile_set')

        return remained_members_mobile_set

    def update_members_group_with_mobile_set(self,target_group_name,members_mobile_set):

        remained_members_mobile_set = set(members_mobile_set)

        sub_group_structures_count = self.count_next_layer_structures(current_layer_name = target_group_name)
        
        for sub_group_number in range(1 , 1 + sub_group_structures_count):

            sub_group_name = '{target_group_name}.{sub_group_number}'.format(
                target_group_name = target_group_name, 
                sub_group_number = sub_group_number
                )
            remained_members_mobile_set = self.update_members_sub_group_with_mobile_set(
                target_sub_group_name = sub_group_name,
                members_mobile_set = remained_members_mobile_set
                )

        return remained_members_mobile_set
    
    def update_members_sub_group_with_mobile_set(self,target_sub_group_name,members_mobile_set):

        members_mobile_set = set(members_mobile_set)

        remained_members_mobile_set, sub_group_members_mobile_set = self.separate_sub_layer_members_mobile_set(
            sub_layer_name = target_sub_group_name,
            members_mobile_set = members_mobile_set
        )

        active_message_types_list = self.get_active_message_types_list(sub_group = target_sub_group_name)
        random.shuffle(active_message_types_list)
        message_type_structures_count = len(active_message_types_list)

        members_mobile_divided_list = dividing_list(
            primary_list = list(sub_group_members_mobile_set),
            parts_count = message_type_structures_count
            )
        # print(len(members_mobile_divided_list))
        # print(message_type_structures_count)
        # print(sub_group_members_mobile_set)
        if (len(members_mobile_divided_list) != message_type_structures_count 
            and len(sub_group_members_mobile_set) != 0
            and message_type_structures_count < len(sub_group_members_mobile_set)):
            # print('Warning : members_mobile_divided_list ({}) not equal with message_type_structures_count ({})!'.format(
            #     len(members_mobile_divided_list),
            #     message_type_structures_count)
            #     )
            # print(sub_group_members_mobile_set)
            pass

        for number in range(min(len(members_mobile_divided_list),message_type_structures_count)):

            message_type = active_message_types_list[number]
            message_type_name = '{sub_group_name}.{message_type}'.format(
                sub_group_name = target_sub_group_name,
                message_type = message_type
                )
            message_type_members_mobile_set = set(members_mobile_divided_list[number])
            self.update_members_message_type_with_mobile_set(
                target_message_type_name = message_type_name,
                members_mobile_set = message_type_members_mobile_set
            )

        return remained_members_mobile_set

    def update_members_message_type_with_mobile_set(self,target_message_type_name,members_mobile_set):

        members_mobile_list = list(members_mobile_set)

        target_structure = Scenario_Structure.objects.get(name = target_message_type_name)
        target_scenario = target_structure.scenario
        structure_type = target_scenario.structure_type

        if structure_type in ['weekly','online']:
            q0 = Q(structure_type = target_scenario.structure_type)
            q1 = Q(medium = target_scenario.medium)
            scenarios = Scenario.objects.filter(q0 & q1)
            structures = Scenario_Structure.objects.filter(scenario__in = scenarios)
        elif structure_type == 'cross_sell':
            structures = target_scenario.scenario_structure_set.all()

        q0 = Q(status__in = structures)
        q1 = Q(status__isnull = True)
        q2 = Q(mobile__in = members_mobile_list)
        condition = Q((q0 | q1) & q2)
        currnet_members = Scenario_Member.objects.filter(condition)


        currnet_members.update(status_id = target_message_type_name)
        currnet_members.update(updated_at = DT.datetime.now())
        currnet_members.update(action = False)
        currnet_members.update(action_need = False)

        # test_tim = DT.datetime.now()
        # old_members_mobile_set = set(list(currnet_members.values_list('mobile',flat=True)))
        # new_members_mobile_set = members_mobile_set - old_members_mobile_set
        # print(DT.datetime.now() - test_tim)

        # test_tim = DT.datetime.now()
        currnet_members_mobile_list = list(currnet_members.values_list('mobile',flat=True))
        new_members_mobile_list = [mobile for mobile in members_mobile_list if mobile not in currnet_members_mobile_list]
        # a = set(new_members_mobile_list)
        # print(DT.datetime.now() - test_tim)

        # print('d:',a - new_members_mobile_set,new_members_mobile_set - a)

        self.create_members(
            members_mobile_list = new_members_mobile_list,
            structure_name = target_message_type_name)
        
        if structure_type != 'cross_sell':
            print('-- {} ({}) number of members had been updated to {}.'.format(
                len(members_mobile_set),
                len(new_members_mobile_list),
                target_message_type_name)
                )

    # extra functions
    def select_phase_members(self,phase_name,member_type = 'output',value_list = None,value_set = None):

        if member_type == 'input':
            # input (i) = output (i -1)

            phase_state = phase_name.split('.')
            phase_number = int(phase_state[-1])
            if phase_number == 1 :
                print('Error : first phase of scenario cant update its input!')
                return set()

            previous_phase_number = phase_number - 1
            previous_phase_state = phase_state[0:-1] + [str(previous_phase_number)]
            previous_phase_name = '.'.join(previous_phase_state)
            
            phase_name = previous_phase_name
            member_type = 'output'
        
        scenario_name,sub_scenario_part,phase = phase_name.split('.')[0:3]
        q1 = Q(scenario_id = scenario_name)
        q2 = Q(sub_scenario_part = sub_scenario_part)
        q3 = Q(phase = phase)
        phase_sub_structures = Scenario_Structure.objects.filter(q1 & q2 & q3)

        phase_members = Scenario_Member.objects.filter(status__in = phase_sub_structures)
        
        if member_type == 'output':
            q0 = Q(action = True)
            phase_members = phase_members.filter(q0)
        
        if member_type == 'waiting':
            q0 = Q(action = False)
            phase_members = phase_members.filter(q0)
        
        if value_set is not None:
            phase_members = set(list(phase_members.values_list(value_set,flat = True)))
        
        if value_list is not None:
            phase_members = list(phase_members.values_list(value_list,flat = True))
        
        return(phase_members)

    def select_scenario_members_mobile_set(self,scenario_name, member_type = 'current'):
        
        scenario_current_members_mobile_list = []
        
        scenario_sub_structures = Scenario_Structure.objects.filter(scenario_id = scenario_name)
        scenario_current_members_mobile_list = list(Scenario_Member.objects.filter(
            status__in = scenario_sub_structures).values_list('mobile',flat = True))
        # for structure in scenario_sub_structures:
        #     condition0 = Q(status_id = structure.name)
        #     mobile_list = list(Scenario_Member.objects.filter(condition0).values_list('mobile',flat = True))
        #     scenario_current_members_mobile_list.extend(mobile_list)

        if member_type == 'current':
            return set(scenario_current_members_mobile_list)

        elif member_type == 'input':
            target_people_mobile_set = read_target_people(scenario_name = scenario_name)
            scenario_current_members_mobile_set = set(scenario_current_members_mobile_list)
            scenario_input_members_mobile_set = target_people_mobile_set - scenario_current_members_mobile_set
            return(scenario_input_members_mobile_set)

        elif member_type == 'output':
            target_people_mobile_set = read_target_people(scenario_name = scenario_name)
            scenario_current_members_mobile_set = set(scenario_current_members_mobile_list)
            scenario_output_members_mobile_set = scenario_current_members_mobile_set - target_people_mobile_set
            return(scenario_output_members_mobile_set)

    def separate_sub_layer_members_mobile_set(self, sub_layer_name, members_mobile_set):
        
        filter_name = self.identify_sub_layer_filter(sub_layer_name)

        sub_layer_members_mobile_set = self.filter_manager.modifying_set(members_mobile_set,filter_name)
        remained_members_mobile_set = members_mobile_set - sub_layer_members_mobile_set

        return [remained_members_mobile_set, sub_layer_members_mobile_set]
    
    def identify_sub_layer_filter(self,sub_layer_name):

        sub_layer_state = sub_layer_name.split('.')

        if sub_layer_name[-1] != '.':
            sub_layer_name = sub_layer_name + '.'
        # print(sub_layer_name)
        structure = Scenario_Structure.objects.filter(name__startswith = sub_layer_name)[0]

        filters_names = {
             2: structure.sub_scenario_filter,
             4: structure.sub_phase_filter,
             6: structure.sub_group_filter,
        }

        filter_name = filters_names[len(sub_layer_state)]

        return(filter_name)

    def count_next_layer_structures(self,current_layer_name):

        current_layer_state = current_layer_name.split('.')

        if current_layer_name[-1] != '.':
            current_layer_name = current_layer_name + '.'
        
        structures = Scenario_Structure.objects.filter(name__startswith = current_layer_name).order_by('-phase','-name')
        last_structure_name = structures[0].name
        last_structure_state = last_structure_name.split('.')

        next_layer_count = int(last_structure_state[len(current_layer_state)])

        return next_layer_count
    
    def get_active_message_types_list(self,sub_group):

        sub_group_state = sub_group.split('.')

        condition0 = Q(scenario_id = sub_group_state[0])
        condition1 = Q(sub_scenario_part = sub_group_state[1])
        condition2 = Q(phase = int(sub_group_state[2]))
        condition3 = Q(sub_phase_part = int(sub_group_state[3]))
        condition4 = Q(group = int(sub_group_state[4]))
        condition5 = Q(sub_group_part = int(sub_group_state[5]))

        condition6 = Q(message_is_active = True)

        structures = Scenario_Structure.objects.filter(
            condition0 & 
            condition1 & 
            condition2 & 
            condition3 & 
            condition4 & 
            condition5 & 
            condition6
            )

        active_message_types_list = list(structures.values_list('message_type',flat = True))
        # print(active_message_types_list)

        return(active_message_types_list)

    def get_blocked_list(self):
        return ([])
        #in bayad kamel she

    # members' basic functions
    def reset_members_info(self,members_mobile_set,layer_name):
        # marhale aval archive kardane
        # marhale dovom delete kardane
        # marhale sevom sakhte raw_members

        self.archive_members(members_mobile_set = members_mobile_set,layer_name = layer_name)
        self.delete_members(members_mobile_set = members_mobile_set,layer_name = layer_name)
        self.create_members(members_mobile_list = list(members_mobile_set),structure_name = layer_name)
        print('-- {} ({}) number of members hab been rewrite.'.format(len(members_mobile_set),len(members_mobile_set)))
        
    def delete_members(self,members_mobile_set = None,layer_name = None,status = None,archive = False):
        # bar haseb noe vooroodi member haro pak mikone
        if archive and layer_name:
            self.archive_members(
                members_mobile_set = members_mobile_set,
                layer_name = layer_name
                )

        if members_mobile_set and layer_name:
            scenarios = select_scenario(
                structure_type = True,
                sample = layer_name
                )
            structures = Scenario_Structure.objects.filter(scenario__in = scenarios)
            q0 = Q(status__in = structures)
            q1 = Q(mobile__in = list(members_mobile_set))
            condition = Q(q1 & q0)
            
            Scenario_Member.objects.filter(condition).delete()
            print('-- {} ({}) number of members had been deleted.'.format(len(members_mobile_set),len(members_mobile_set)))

    def create_members(self,members_mobile_list,structure_name = None):
        # faghat member misaze va hamye field hasho null mizare

        members_divided_mobile_list = dividing_list(
            primary_list = members_mobile_list,
            parts_len = self.bulk_size)

        count = 0
        for mobile_list in members_divided_mobile_list:
            members_list = []
            for mobile in mobile_list:
                try:
                    member = Scenario_Member(
                        mobile = mobile,
                        status_id = structure_name)
                    name = member.create_name()
                    members_list.append(member)
                except:
                    print("member with id = {} already exist!".format(mobile))

            Scenario_Member.objects.bulk_create(members_list)
            count = count + len(members_list)
            print('-- {} ({}) number of raw_members had been created.'.format(len(members_list),count))
        
    def archive_members(self,members_mobile_set,layer_name):

        members_divided_mobile_list = dividing_list(
            primary_list = list(members_mobile_set),
            parts_len = self.bulk_size
            )
        
        count = 0
        for members_mobile_list in members_divided_mobile_list:

            scenarios = select_scenario(
                structure_type = True,
                sample = layer_name
                )
            structures = Scenario_Structure.objects.filter(scenario__in = scenarios)
            q0 = Q(status__in = structures)
            q1 = Q(mobile__in = members_mobile_list)
            condition = Q(q1 & q0)

            members = Scenario_Member.objects.filter(condition)
            if members.count() != len(members_mobile_list):
                print('warning : archive')
            archived_members_list = []

            for member in members:

                archived_member = Member_Scenario_History(
                    
                    last_status = member.status_id,
                    last_update = member.updated_at
                )

                archived_members_list.append(archived_member)

            Member_Scenario_History.objects.bulk_create(archived_members_list, ignore_conflicts=True)
            count = count + len(archived_members_list)
            print('-- {} ({}) number of members had been archived.'.format(len(archived_members_list),count))

    # run functions
    def update_all_scenarios(self):
        pass

    def update_all_weekly_scenarios(self):
        structure_type='weekly'
        self.update_scenarios_with_structure_type(structure_type)
        
    def update_all_cross_sell_scenarios(self):
        structure_type='cross_sell'
        self.update_scenarios_with_structure_type(structure_type)

    def update_all_online_scenarios(self):
        structure_type='online'
        self.update_scenarios_with_structure_type(structure_type)

    def update_scenarios_with_structure_type(self,structure_type):
        q1 = Q(is_active=True)
        q2 = Q(structure_type=structure_type)
        scenarios = Scenario.objects.filter(q1 & q2)
        scenarios_name = list(scenarios.values_list('name',flat=True))
        scenarios_name.sort()
        for scenario_name in scenarios_name:
            self.update_scenario_members(scenario_name = scenario_name)

def import_previous_db_members_info():
    import pandas as pd
    from ForeignDBConnection.QueryManager import Query
    query = Query(app_name = 'MemberManager',app_path = os.path.dirname(__file__))

    query_file_name = 'LastDBReceiversInfo.sql'
    query.read_file(query_file_name=query_file_name)
    
    query.create_connection(
        product_name = 'local',
        connection_type = '',
        db_name = 'sadradna')

    data = query.read_data()

    members_list = []
    for i,row in data.iterrows():
        member = Scenario_Member(
            mobile = row['mobile']
            ,scenario_id = row['status'].split('.')[0]
            ,status_id = row['status']
            ,updated_at = row['updated_at']
            ,action = row['action']
            ,created_at = row['created_at']
        )
        name = member.create_name()
        members_list.append(member)
    
    Scenario_Member.objects.bulk_create(members_list)
    print('- {} ({}) number of members had been imported.'.format(len(members_list),len(members_list)))

if __name__ == "__main__":
    pass
