with all_orders_service_as_array as  (
    SELECT
           customer_id
           ,string_to_array(all_orders_service, ',') as all_orders_service_array
    from "CustomerManager_customer_extra_information"
    )
select
    customer_id
from all_orders_service_as_array
where   all_orders_service_array[2] = all_orders_service_array[1]
