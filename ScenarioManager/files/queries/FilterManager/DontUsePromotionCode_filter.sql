with used_promotions_as_array as (
    SELECT customer_id
         , unnest(string_to_array(used_promotions, ',')) as used_promotions
    from "CustomerManager_customer_extra_information"
)
select
    distinct (customer_id) as customer_id
from used_promotions_as_array
where used_promotions in ('_used_promotions')