with  receivers_numbering as (
    SELECT ROW_NUMBER() OVER (PARTITION BY smr.object_id ORDER BY smr.created_at desc) AS r,
           smr.*
    FROM "SMSManager_receiver" as smr
    order by
        smr.object_id,smr.created_at desc
    )
    ,last_message_info as (
    select rn.object_id                                                 as mobile
         , rn.message_text_id                                           as status
         , (case when rn.kavenegar_id is null then FALSE else TRUE end) as action
         , rn.created_at                                                as updated_at
    from "receivers_numbering" as rn
    where rn.r = 1
    )
    ,receivers_numbering_reverse as (
    SELECT ROW_NUMBER() OVER (PARTITION BY smr.object_id ORDER BY smr.created_at) AS r,
           smr.*
    FROM "SMSManager_receiver" as smr
    order by
        smr.object_id,smr.created_at
    )
    ,first_message_info as (
    select rn.object_id  as mobile
         , rn.created_at as created_at
    from "receivers_numbering_reverse" as rn
    where rn.r = 1
)

select
    lmi.mobile
    ,array_to_string(string_to_array(lmi.status,'-'),'.') as status
    ,lmi.action
    ,lmi.updated_at
    ,fmi.created_at
from last_message_info as lmi
left join first_message_info as fmi using (mobile)
