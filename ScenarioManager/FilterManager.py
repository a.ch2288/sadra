import os
from django.db.models import Q
from CustomerManager.models import Customer_Extra_Information
from ForeignDBConnection.QueryManager import Query
import pandas as pd
import datetime as DT



class Filter_manager:

    def __init__(self):

        self.query = Query(app_name = 'FilterManager',app_path = os.path.dirname(__file__))
        
        self.switcher = {
            'None' : self.Ineffective_filter,
            'ComplementOfAll' : self.Ineffective_filter,
            'DontUse' : self.DontUsePromotionCode_filter,
            'SameOrders' : self.SameOrders_filter,
            'LastOrderCanceled' : self.LastOrderCanceled_filter,
            'Reason' : self.LastOrderCanceledReason_filter,
            'SMSReceivers' : self.SeparateSMSReceivers_filter,
            'FreezingTime' : self.FreezingTime_filter,
            'SourceServiceStatus' : self.SourceServiceStatus_filter
        }

    def modifying_set(self,initial_set,filter_name):

        filters = filter_name.split('&')
        modified_set = initial_set
        for filter_name in filters:
            filter_name ,filter_arguments = filter_name.split('$')[0],filter_name.split('$')[1:]
            filter_function = self.switcher[filter_name]
            modified_set = filter_function(modified_set,filter_arguments)

        return modified_set

    def Ineffective_filter(self,initial_set,filter_arguments):
        return initial_set

    def DontUsePromotionCode_filter(self,initial_set,promotions_list):

        query_file_name = 'DontUsePromotionCode_filter.sql'
        self.query.read_file(query_file_name=query_file_name)
        
        self.query.create_connection(
            product_name = 'local',
            connection_type = '',
            db_name = 'sadradna_01')

        query_arguments = {'_used_promotions' : "','".join(promotions_list)}

        data = self.query.read_data(arguments=query_arguments)

        if data.shape[0] == 0:
            who_used_promotions_mobile_set = set()
        else:
            who_used_promotions_mobile_set = set(data['customer_id'].to_list())

        final_set = initial_set - who_used_promotions_mobile_set

        return(final_set)
    
    def SameOrders_filter(self,initial_set,filter_arguments):

        query_file_name = 'SameOrders_filter.sql'
        self.query.read_file(query_file_name=query_file_name)
        
        self.query.create_connection(
            product_name = 'local',
            connection_type = '',
            db_name = 'sadradna_01')

        data = self.query.read_data()

        if data.shape[0] == 0:
            same_order_users_set = set()
        else:
            same_order_users_set = set(data['customer_id'].to_list())

        final_set = initial_set - (initial_set - same_order_users_set)

        return(final_set)

    def LastOrderCanceled_filter(self,initial_set,filter_arguments):
        
        LastOrderCanceled_mobile_list = Customer_Extra_Information.objects.filter(last_order_status='Canceled').values_list('customer_id', flat=True)

        LastOrderCanceled_mobile_set = set(list(LastOrderCanceled_mobile_list))
        
        final_set = initial_set - (initial_set - LastOrderCanceled_mobile_set)

        return final_set

    def LastOrderCanceledReason_filter(self,initial_set,filter_arguments):
        
        reason_slug = {
            'Null' : 'Null',
            'Out' : 'customer_out',
            'Test' : 'customer_test',
            'Other' : 'customer_other',
            'NegativeComments' : 'customer_negative_comments',
            'Time' : 'customer_time',
            'Personal' : 'customer_personal',
            'Fee' : 'customer_fee',
            'NotAnswer' : 'customer_not_answer'
        }

        LastOrderCanceledReason_mobile_list = []

        for argument in filter_arguments:
            reason = reason_slug[argument]
            mobiles_list = Customer_Extra_Information.objects.filter(last_order_status='Canceled').filter(canceled_orders_reason__startswith = reason).values_list('customer_id', flat=True)
            LastOrderCanceledReason_mobile_list.extend(list(mobiles_list))

        LastOrderCanceledReason_mobile_set = set(LastOrderCanceledReason_mobile_list)
        
        final_set = initial_set - (initial_set - LastOrderCanceledReason_mobile_set)

        return final_set

    def SeparateSMSReceivers_filter(self,initial_set,filter_arguments):

        initial_list = list(initial_set)
        from SMSManager.models import Receiver
        time_limit = filter_arguments[0]

        condition0 = Q(mobile__in = initial_list)
        condition1 = Q(sent_at__gt = DT.datetime.now() - DT.timedelta(days=time_limit))
        receivers_mobile_list = list(Receiver.objects.filter(condition0 & condition1).values_list('mobile',flat = True))

        return set(receivers_mobile_list)


    # sending time filter
    def FreezingTime_filter(self,initial_set,filter_arguments):
        from ScenarioManager.models import Scenario_Member
        
        q0 = Q(name__in = list(initial_set))
        time_limit = float(filter_arguments[0])
        q1 = Q(updated_at__lte = DT.datetime.now() - DT.timedelta(days=time_limit))
        target_members_mobile_list = list(Scenario_Member.objects.filter(q0 & q1).values_list('mobile',flat = True))

        return set(target_members_mobile_list)

    def SourceServiceStatus_filter(self,initial_set,filter_arguments):

        target_status = filter_arguments[0]

        order_statuses = {
            'Submitted' : 1,
            'NeedQuote' : 2,
            'WithQuote' : 3,
            'QuoteAccepted' : 4,
            'WithInvoice' : 5,
            'Started' : 6,
            'Finished' : 7,
            'Done' : 8,
            'WithFeedback' : 9
        }

        target_status_number = order_statuses[target_status]

        target_statuses_list = []
        for status in order_statuses:
            if order_statuses[status] >= target_status_number:
                target_statuses_list.append(status)
        
        from CrossSellModel.models import Customer_CrossSell_Information,Service_Base_Segment
        from ScenarioManager.models import Scenario_Member,Scenario_Structure

        initial_list = list(initial_set)
        sample = initial_list[0]
        scenario_structure_id = '.'.join(sample.split('.')[0:-1])
        scenario_structure = Scenario_Structure.objects.get(name = scenario_structure_id)
        scenario = scenario_structure.scenario
        scenario_target_people = scenario.target_people
        service_base_segment_id = scenario_target_people.split('.')[-1]
        service_base_segment = Service_Base_Segment.objects.get(name = service_base_segment_id)
        

        mobile_list = list(Scenario_Member.objects.filter(name__in = initial_list).values_list('mobile',flat=True))
        q1 = Q(mobile__in = mobile_list)
        q2 = Q(order_status__in = target_statuses_list)

        target_customers = service_base_segment.customer_crosssell_information_set.filter(q1 & q2)
        target_members_mobile_list = list(target_customers.values_list('mobile',flat = True))

        return set(target_members_mobile_list)


def check_filters_name(filters_name_list):

    filter_manager = Filter_manager()
    all_filters_name_list = filter_manager.switcher.keys()
    all_filters_name = ','.join(all_filters_name_list)

    for raw_name in filters_name_list:
        name = raw_name.split('$')[0]
        if not (name in all_filters_name):
            return False
    
    return True





if __name__ == "__main__":
    filter_manager  = Filter_manager()
    initial_set = set(['Housecleaning_CrossSell.1.1.1.1.1.1.09190051230'])
    filter_name = 'SourceServiceStatus$Submitted'
    a = filter_manager.modifying_set(initial_set,filter_name)
    print(a)

