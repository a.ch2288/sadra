from celery import shared_task
from .ModelManager import ScenarioModelManager
from .Functions import MemberManager
from datetime import datetime


@shared_task
def update_all_scenarios_model():
    scenario_model_manager = ScenarioModelManager()
    scenario_model_manager.update_all()


@shared_task
def update_cross_sell_members():
    if 20 < datetime.now().hour or datetime.now().hour < 3:
        print('rest time! :)')
    else:
        cross_sell_member_manager = MemberManager()
        cross_sell_member_manager.update_all_cross_sell_scenarios()

@shared_task
def update_weekly_members():
    weekly_member_manager = MemberManager()
    weekly_member_manager.update_all_weekly_scenarios()

@shared_task
def update_online_members():
    if 20 < datetime.now().hour or datetime.now().hour < 3:
        print('rest time! :)')
    else:
        online_member_manager = MemberManager()
        online_member_manager.update_all_online_scenarios()
