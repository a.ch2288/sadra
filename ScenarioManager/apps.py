from django.apps import AppConfig


class ScenariomanagerConfig(AppConfig):
    name = 'ScenarioManager'
