from django.db import models
from django.utils import timezone
import uuid
# Create your models here.


class Scenario(models.Model):

    name = models.CharField(primary_key=True,max_length=255)
    target_people = models.CharField(max_length=255,null=True)
    medium = models.CharField(max_length=255,null = True)
    structure_type = models.CharField(max_length=50,null = True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateField(auto_now_add=True)

    def get_fields_type():

        pk = 'name'

        fields_type = {
            'name' : 'str',
            'target_people' : 'str',
            'medium' : 'str',
            'structure_type' : 'str',
            'is_active' : 'bool'
            # 'created_at' : 'datetime64[ns]',
            }

        return pk,fields_type

class Scenario_Structure(models.Model):

    name = models.CharField(primary_key= True,max_length=255)

    scenario = models.ForeignKey(Scenario, on_delete = models.CASCADE)
    sub_scenario_part = models.SmallIntegerField(default= 1)
    sub_scenario_filter = models.TextField(default= 'None')

    phase = models.SmallIntegerField(null= True,default= 1)
    sub_phase_part = models.SmallIntegerField(default= 1)
    sub_phase_filter = models.TextField(default= 'None')
    waiting_days = models.SmallIntegerField(default=1000,null=True)

    group = models.SmallIntegerField(null= True,default= 1)
    sub_group_part = models.SmallIntegerField(default= 1)
    sub_group_filter = models.TextField(default= 'None')

    message_type = models.SmallIntegerField(default= 0)
    message_template_id = models.CharField(default= 'empty',max_length = 255)
    message_promotion_code = models.CharField(default= 'empty',null= True,max_length=255)
    message_service_slug = models.CharField(default= 'empty',null= True,max_length=255)
    message_is_active = models.BooleanField(default=True)

    message_link_type = models.CharField(default= 'service_based',max_length=255, null=True)
    link_basic_utm_id = models.CharField(default= 'SadRa.retention.sms',max_length=255, null=True)
    link_message_utm = models.CharField(default= 'auto_generation',max_length=255, null=True)
    link_personal_utm = models.BooleanField(default=True)
    message_sending_filter = models.CharField(default= 'empty',max_length = 255)
    message_sending_time = models.CharField(max_length=255, null=True)

    def get_fields_type():
        
        pk = 'name'

        fields_type = {
            'name' : 'str',

            'scenario_id' : 'str',
            'sub_scenario_part' : 'int',
            'sub_scenario_filter' : 'str',
            
            'phase' : 'int',
            'sub_phase_part' : 'int',
            'sub_phase_filter' : 'str',
            'waiting_days' : 'int',

            'group' : 'int',
            'sub_group_part' : 'int',
            'sub_group_filter' : 'str',

            'message_type' : 'int',
            'message_template_id' : 'str',
            'message_promotion_code' : 'str',
            'message_service_slug' : 'str',
            'message_is_active' : 'bool',


            'message_link_type' : 'str',
            'link_basic_utm_id' : 'str',
            'link_message_utm' : 'str',
            'link_personal_utm' : 'bool',

            'message_sending_filter' : 'str',
            'message_sending_time' : 'str',

            }

        return pk,fields_type

    def create_name(self):
        
        name = '.'.join(
            [str(self.scenario_id)
            ,str(self.sub_scenario_part)
            ,str(self.phase)
            ,str(self.sub_phase_part)
            ,str(self.group)
            ,str(self.sub_group_part)
            ,str(self.message_type)]
            )
        
        self.name = name
        return name
    
    def __str__(self):
        
        return('Scenario_Structure')

class Scenario_Member(models.Model):

    name = models.CharField(primary_key= True,max_length=255)

    mobile = models.CharField(null= True,max_length=15)
    scenario = models.ForeignKey(Scenario, on_delete = models.CASCADE,null= True)
    status = models.ForeignKey(Scenario_Structure, on_delete = models.CASCADE,null= True)
    created_at = models.DateField(default=timezone.now, null = True)
    updated_at = models.DateField(default=timezone.now, null = True)
    action = models.BooleanField(default=False,null=True)
    action_need = models.BooleanField(default=False,null=True)

    def create_name(self):
        name = '{}.{}'.format(self.status_id,self.mobile)
        self.name = name
        return name

class Member_Scenario_History(models.Model):

    id = models.UUIDField(primary_key = True,default=uuid.uuid4, editable=False)
    last_update = models.DateField(null = True)
    last_status = models.CharField(null = True, max_length = 255)
    

class Blocked_Member(models.Model):

    blocked_mobile = models.CharField(primary_key= True,max_length=15)
    created_at = models.DateField(default=timezone.now, null = True)

    def get_fields_type():

        pk = 'blocked_mobile'

        fields_type = {
            'blocked_mobile' : 'str'
            }

        return pk,fields_type