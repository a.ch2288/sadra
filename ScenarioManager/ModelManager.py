import os
import pandas as pd

from .models import *
from ForeignDBConnection.QueryManager import GoogleSheet
from ServiceManager.ModelManager import check_service_slug
from MessageManager.ModelManager import check_template_id,get_template_info
from PromotionManager.ModelManager import check_promotion_code
from SMSManager.ModelManager import check_sending_time
from ScenarioManager.FilterManager import check_filters_name
from LinkManager.ModelManager import check_link

def select_scenario_members(scenario_name = None,structure_name = None,action = None,action_need = None,value_list = None,value_set = None):
    members = Scenario_Member.objects.all()
    if scenario_name:
        members = members.filter(scenario_id = scenario_name)
    if structure_name:
        members = members.filter(status_id = structure_name)
    if action is not None:
        members = members.filter(action = action)
    if action_need is not None:
        members = members.filter(action_need = action_need)
    if value_list:
        members = list(members.values_list(value_list,flat=True))
    if value_set:
        members = set(list(members.values_list(value_set,flat=True)))
    return members

def select_scenarios(structure_type = None,medium = None,is_active = True):
    target_scenarios = Scenario.objects.all()
    if structure_type:
        target_scenarios = target_scenarios.filter(structure_type = structure_type)
    if medium:
        target_scenarios = target_scenarios.filter(medium = medium)
    if is_active is not None:
        target_scenarios = target_scenarios.filter(is_active = is_active)

    return target_scenarios

def get_scenario_info(scenario_name):
    try:
        info = Scenario.objects.filter(name = scenario_name).values()[0]
    except Exception as e:
        print(f'Error : {e}')
        print(scenario_name)
        exit(0)
    return info

def get_scenario_structure_info(structure_name):
    info = Scenario_Structure.objects.filter(name = structure_name).values()[0]
    return info

class ScenarioModelManager:

    def __init__(self):

        self.file_name = 'ScenarioManager.xlsx'

        self.scenario_manager_sheets_info = {
            'google_sheet_id' : '1wgvr9OeVLCbEVsCjHKPi5dImeQZF_v-2G9vs63knyCc',
            'sheets_name' : {
                'scenarios' : ['Scenarios'],
                'structures' : ['Weekly_Scenarios_Stracture',
                                'CrossSell_Scenarios_Structure',
                                'TeleOrderingTest',
                                'Lazarus_Scenario_Stracture',
                                'Online_Scenarios_Stracture'],
                'blocked_members' : ['Blocked_Members']
                },
            'target_models' : {
                'scenarios' : Scenario,
                'structures' : Scenario_Structure,
                'blocked_members' : Blocked_Member
                },
        }

        
        self.foreign_data = {}
    
    def update_all(self):
        
        self.read_info()
        self.update_scenarios()
        self.update_scenarios_structuer()
        self.update_blocked_members()

    def update_scenarios_structuer(self):

        # Scenario_Structure.objects.all().delete()
        
        if len(self.foreign_data) == 0:
            self.read_info()
            if len(self.foreign_data) == 0 :
                pass
                # raise error
        
        target_model = self.scenario_manager_sheets_info['target_models']['structures']
        sheets_name_list = self.scenario_manager_sheets_info['sheets_name']['structures']
        
        for sheet_name in sheets_name_list:
            data = self.foreign_data[sheet_name]
            created_counter = 0
            valid_counter = 0
            for i,row in data.iterrows():
                
                ss = target_model(**row)
                name = ss.create_name()
                # print(name)

                if self.validate_scenario_structure(row = row,name = name):

                    created_counter += int(0 == target_model.objects.filter(name = name).count())
                    valid_counter += 1
                    ss.save()
                

            print(f'- {valid_counter} ({created_counter}) {sheet_name} had been update.')

    def validate_scenario_structure(self,row,name):

        
        scenario_name = row['scenario_id']
        if Scenario.objects.filter(name = scenario_name).count() == 0:
            print(f'Error : Cant find matched Scenario. {scenario_name}')
            return False

        scenario_info = Scenario.objects.filter(name = scenario_name).values()[0]
        t1 = (row['message_type'] != 0)
        
        scenario_medium = scenario_info['medium']
        t2 = check_template_id(
            template_id = row['message_template_id'],
            template_type = scenario_medium)
        if t2 :
            t3 = True
            template_info = get_template_info(
                template_name = row['message_template_id'],
                template_type = scenario_medium
                )
            if ('service' in template_info['arguments']
                and row['message_service_slug'] == 'empty'):
                t3 = False
            if ('promotion' in template_info['arguments']
                and row['message_promotion_code'] == 'empty'):
                t3 = False
        else:
            t3 = False

        t4 = check_service_slug(row['message_service_slug'])
        t5 = check_promotion_code(row['message_promotion_code'])
               
        if row['message_link_type'] != 'empty':
            t6 = check_link(
                link_type = row['message_link_type'],
                basic_utm_id= row['link_basic_utm_id'],
                message_utm = row['link_message_utm'],
                personal_utm = row['link_personal_utm']
                )
        else:
            t6 = True
        

        t7 = check_sending_time(
            sending_time = row['message_sending_time']
            )
        
        filters_name = []
        for index_name in row.index:
            if 'filter' in index_name:
                filters_name.append(row[index_name])
        t8 = check_filters_name(filters_name)
        
        if t1 and t2 and t3 and t4 and t5 and t6 and t7 and t8 :
            return True
        else:
            check_result = '-'.join([str(t1),str(t2),str(t3),str(t4),str(t5),str(t6),str(t7),str(t8)])
            print('Error {0} in {1}!'.format(check_result,name))
            return(False)
        
    def update_scenarios(self):

        # Scenario.objects.all().delete()

        if len(self.foreign_data) == 0:
            self.read_info()
            if len(self.foreign_data) == 0 :
                pass
                # raise error

        target_model = self.scenario_manager_sheets_info['target_models']['scenarios']
        sheets_name_list = self.scenario_manager_sheets_info['sheets_name']['scenarios']
        
        for sheet_name in sheets_name_list:
            data = self.foreign_data[sheet_name]
            created_counter = 0
            for i,row in data.iterrows():

                obj, created = target_model.objects.update_or_create(name = row['name'],defaults = row.to_dict())
                created_counter += int(created)
            
            print('- {} ({}) {}s had been updated.'.format(data.shape[0],created_counter,sheet_name))
    
    def update_blocked_members(self):

        if len(self.foreign_data) == 0:
            self.read_info()
            if len(self.foreign_data) == 0 :
                pass
                # raise error

        target_model = self.scenario_manager_sheets_info['target_models']['blocked_members']
        sheets_name_list = self.scenario_manager_sheets_info['sheets_name']['blocked_members']
        
        for sheet_name in sheets_name_list:
            data = self.foreign_data[sheet_name]
            created_counter = 0
            for i,row in data.iterrows():

                obj, created = target_model.objects.update_or_create(
                    blocked_mobile = '0{}'.format(row['blocked_mobile'])
                    )
                created_counter += int(created)
            
            print('- {} ({}) {}s had been updated.'.format(data.shape[0],created_counter,sheet_name))

    def validate_scenarios(self):
        pass
      
    def read_info(self,targets = None):

        if targets is None :
            targets = ['scenarios','structures','blocked_members']
        else:
            if type(targets) is not list:
                targets = [targets]

        google_sheet_id = self.scenario_manager_sheets_info['google_sheet_id']

        for target in targets:
            sheets_name = self.scenario_manager_sheets_info['sheets_name'][target]
            target_model = self.scenario_manager_sheets_info['target_models'][target]
        
            for sheet_name in sheets_name:

                google_sheet = GoogleSheet(
                    google_sheet_id = google_sheet_id,
                    sheet_name = sheet_name
                    )

                model_pk,model_feilds_type = target_model.get_fields_type()

                self.foreign_data[sheet_name] = google_sheet.get_data(
                    model_feilds_type = model_feilds_type,
                    fillna = True
                    )

                if self.foreign_data[sheet_name].shape[0] == 0 :
                    print('Warning : data sheet {} is empty.'.format(sheet_name))
                    
                else:
                    print(f'- reading data in {sheet_name} sheet had been completed.')


if __name__ == "__main__":
    S_manager = ScenarioModelManager()
    # S_manager.update_all()