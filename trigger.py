#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys

from datetime import datetime as DT

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sadra.settings')
import django
django.setup()
from django.conf import settings

def runner(manager_funcs,func_name,args):

    if len(args) == 0:
        manager_funcs[func_name]()
    else:
        manager_funcs[func_name](**args)

def customer_manager(manager,func_name,args):
    if manager == 'CustomerModelManager':
        from CustomerManager.ModelManager import CustomerModelManager

        manager = CustomerModelManager()

        manager_funcs = {
            'rewrite_all' : manager.rewrite_all,
            'rewrite_all_CIIs' : manager.rewrite_all_CIIs,
            'rewrite_all_PIIs' : manager.rewrite_all_PIIs,
            'rewrite_all_CLIs' : manager.rewrite_all_CLIs,
            'rewrite_all_CEIs' : manager.rewrite_all_CEIs,
            'update_customers_with_time_limit' : manager.insert_new_customers_with_time_limit
        }
    
    runner(manager_funcs,func_name,args)

def service_manager(manager,func_name,args):

    if manager =='ServiceModelManager':
        from ServiceManager.ModelManager import ServiceModelManager

        manager = ServiceModelManager()

        manager_funcs = {
            'update_all' : manager.update_all
        }
    
    runner(manager_funcs,func_name,args)

def rfm_model(manager,func_name,args):

    if manager == 'SegmentModelManager':

        from RFMModel.ModelManager import SegmentModelManager

        manager = SegmentModelManager()

        manager_funcs = {
            'update_all' : manager.update_all
        }
    
    elif manager == 'UserModelManager':
        from RFMModel.ModelManager import UserModelManager

        manager = UserModelManager()

        manager_funcs = {
            'rewrite_customers_rfm_info' : manager.rewrite_customers_rfm_info,
            'update_new_customers_rfm_info_with_time_limit' : manager.update_new_customers_rfm_info_with_time_limit
        }

    runner(manager_funcs,func_name,args)
   
def scenario_manager(manager,func_name,args):
    if manager == 'ScenarioModelManager':
        from ScenarioManager.ModelManager import ScenarioModelManager

        manager = ScenarioModelManager()

        manager_funcs = {
            'update_all' : manager.update_all
        }
    
    elif manager == 'MemberManager':
        from ScenarioManager.Functions import MemberManager

        manager = MemberManager()

        manager_funcs = {
            'update_all_scenarios' : manager.update_all_scenarios,
            'update_all_weekly_scenarios' : manager.update_all_weekly_scenarios,
            'update_all_cross_sell_scenarios' : manager.update_all_cross_sell_scenarios,
            'update_all_online_scenarios' : manager.update_all_online_scenarios
        }

    runner(manager_funcs,func_name,args)

def sms_manager(manager,func_name,args):
    if manager == 'ReceiverManager':
        from SMSManager.Functions import ReceiverManager

        manager = ReceiverManager()

        manager_funcs = {
            'update_scenarios_with_target_structure_type' : manager.update_scenarios_with_target_structure_type
        }
    
    runner(manager_funcs,func_name,args)

def message_manager(manager,func_name,args):
    if manager == 'MessageModelManager':
        from MessageManager.ModelManager import MessageModelManager
        manager = MessageModelManager()

        manager_funcs = {
            'update_all' : manager.update_model,
            'rewrite_all' : manager.rewrite_model
        }
    
    runner(manager_funcs,func_name,args)

def promotion_manager(manager,func_name,args):
    if manager == 'PromotionModelManager':
        from PromotionManager.ModelManager import PromotionModelManager
        manager = PromotionModelManager()

        manager_funcs = {
            'update_all' : manager.update_all
        }
    
    runner(manager_funcs,func_name,args)

def link_manager(manager,func_name,args):
    if manager == 'LinkModelManager':
        from LinkManager.ModelManager import LinkModelManager
        manager = LinkModelManager()

        manager_funcs = {
            'update_all' : manager.update_all
        }
    
    runner(manager_funcs,func_name,args)

def cross_sell_model(manager,func_name,args):

    if manager == 'SegmentModelManager':

        from CrossSellModel.ModelManager import SegmentModelManager

        manager = SegmentModelManager()

        manager_funcs = {
            'update_all' : manager.update_all
        }
    
    elif manager == 'UserModelManager':

        from CrossSellModel.ModelManager import UserModelManager

        manager = UserModelManager()

        manager_funcs = {
            'update_users_with_time_limit' : manager.update_users_with_time_limit,
            'delete_old_users' : manager.delete_old_users,
            'rewrite_all_users' : manager.rewrite_all_users
        }

    
    runner(manager_funcs,func_name,args)

if __name__ == '__main__':

    all_apps = {
        'CustomerManager' : customer_manager,
        'RFMModel' : rfm_model,
        'ScenarioManager' : scenario_manager,
        'SMSManager' : sms_manager,
        'ServiceManager' : service_manager,
        'MessageManager' : message_manager,
        'PromotionManager' : promotion_manager,
        'LinkManager' : link_manager,
        'CrossSellModel' : cross_sell_model
    }

    start_time = DT.now()

    if sys.argv[1]:
        app_name = sys.argv[1]
        manager = sys.argv[2]
        func = sys.argv[3]
        args_dict = {}
        if len(sys.argv) > 4:
            args_list = sys.argv[4].split(',')
            # print(args_list)
            for arg in args_list:
                args_dict[arg.split("=")[0]] = arg.split("=")[1]

        all_apps[app_name](manager,func,args_dict)
    
    end_time = DT.now()

    print(end_time - start_time)


    
    #test