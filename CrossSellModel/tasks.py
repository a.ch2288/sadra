from celery import shared_task
from .ModelManager import UserModelManager,SegmentModelManager
from datetime import datetime


check = True

@shared_task
def update_all_segments_model_info():
    segment_manager = SegmentModelManager()
    segment_manager.update_all()


@shared_task
def update_users_with_time_limit(time_limit_value=None,time_limit_type=None):
    global check
    user_manager = UserModelManager()
    if 20 < datetime.now().hour or datetime.now().hour < 3:
        print('rest time! :)')
        check = True
    else:
        if check == True:
            time_limit_value = 1
            time_limit_type = 'days'
            check = False

        if time_limit_value is not None and time_limit_type is not None:
            user_manager.update_users_with_time_limit(
                time_limit_value=20,
                time_limit_type='minutes'
                )
        else:
            user_manager.update_users_with_time_limit()

@shared_task
def rewrite_customers_cross_sell_info():
    user_managerr = UserModelManager()
    user_managerr.rewrite_all_users()
