from .models import Customer_CrossSell_Information,Service_Base_Segment
from ForeignDBConnection.QueryManager import GoogleSheet,Query
from django.db.models import Q
import datetime as DT
import uuid
import os
from CustomerManager.models import Customer_Identification_Information
from django.utils import timezone


class SegmentModelManager:

    def __init__(self):

        self.google_sheet_id = '1vFxcoD6rq5ux4nAlcDG7uFUzVGCJ4s2dF6z8thONEeA'
        self.sheet_name = 'service_base_segment'
        self.local_file_name = ''

        self.foreign_data = None
   
    def read_info(self):

        google_sheet = GoogleSheet(
            google_sheet_id = self.google_sheet_id,
            sheet_name = self.sheet_name)

        model_pk,model_feilds_type = Service_Base_Segment.get_fields_type()
        self.foreign_data = google_sheet.get_data(model_feilds_type = model_feilds_type,fillna = False)

        if self.foreign_data.shape[0] == 0 :
            pass
            # Raise Error
        else:
            print('- reading data had been completed.')

    def rewrite_all(self):
        
        print('- {} service_base_segment have been deleted.'.format(Service_Base_Segment.objects.all().count()))
        Service_Base_Segment.objects.all().delete()

        self.update_all()

    def update_all(self):

        self.read_info()
        self.update_segment_info()
    
    def update_segment_info(self):

        for i,row in self.foreign_data.iterrows():
            
            sbs = Service_Base_Segment(**row)
            sbs.save()
        
        print(f'- {self.foreign_data.shape[0]} segments in CrossSellModel have been updated.')

class CrossSellModelQueryManager(Query):
    pass

class UserModelManager:
    
    def __init__(self):

        self.bulk_size = 1000
        self.query = CrossSellModelQueryManager(bulk_size = 5000
            ,app_name = 'UserModelManager'
            ,app_path = os.path.dirname(__file__))

    def update_users_with_time_limit(self,
        time_limit_value = 30,
        time_limit_type = 'minutes'
        ):

        query_file_name = 'updated_CCSM_with_time_limit.sql'
        query_modifying_text_args = {'_delta' : f'{time_limit_value} {time_limit_type}'}
        self.query.read_file(query_file_name=query_file_name)
        self.query.create_connection(
            product_name = 'ostadkar',
            connection_type = '',
            db_name = 'sale')
        
        data = self.query.read_data(arguments = query_modifying_text_args)
        
        try:
            data['id'] = data['id'].apply(lambda x: uuid.UUID(x))
        except:
            pass

        current_customers_id = list(Customer_CrossSell_Information.objects.\
            filter(id__in = data['id']).values_list('id',flat=True))
        
        updated_users = data[data['id'].isin(current_customers_id)]
        self.update_users(updated_users = updated_users)
        
        new_users = data[~data['id'].isin(current_customers_id)]
        self.insert_users(new_users = new_users)
        

        self.delete_old_users()
    
    def delete_old_users(self):
        now = timezone.now()
        q1 = Q(order_status__in = ['Done','WithFeedback','Finished'])
        q4 = Q(order_status = 'Canceled')
        total_count = Customer_CrossSell_Information.objects.all().count()
        counter = 0
        for sbs in list(Service_Base_Segment.objects.all()):
            done_date_limitation_value = int(sbs.done_date_limitation.split(' ')[0])
            q2 = Q(updated_at__lte = (now - DT.timedelta(days=done_date_limitation_value)))
            submitted_date_limitation_value = int(sbs.submitted_date_limitation.split(' ')[0])
            q3 = Q(updated_at__lte = (now - DT.timedelta(days=submitted_date_limitation_value)))
            ccsi = sbs.customer_crosssell_information_set.filter((q1 & q2) | q3 | q4)
            counter += ccsi.count()
            ccsi.delete()
        
        print(f'- {counter} (of {total_count}) users in CrossSellModel have been deleted.')

    def insert_users(self,new_users):
        ccsi_list = []
        segments = Service_Base_Segment.objects.all().order_by('name')
        for segment in segments:
            
            segment_user_mobile = list(segment.customer_crosssell_information_set.\
                filter(mobile__in = new_users['mobile']).values_list('mobile',flat=True))

            condition = (new_users['service_base_segment_id'] == segment.name) &\
                (~new_users['mobile'].isin(segment_user_mobile))

            df = new_users[condition]

            for i,row in df.iterrows():
                ccsi_list.append(Customer_CrossSell_Information(**row))
        
        Customer_CrossSell_Information.objects.bulk_create(ccsi_list)
        print(f'- {len(ccsi_list)} (of {new_users.shape[0]}) users in CrossSellModel have been created.')
    
    def update_users(self,updated_users):
        column_name = ['id','order_status','updated_at']
        updated_users = updated_users[column_name]
        ccsi_list = []
        for i,row in updated_users.iterrows():
            ccsi_list.append(Customer_CrossSell_Information(**row))
        
        Customer_CrossSell_Information.objects.bulk_update(ccsi_list,['order_status','updated_at'])
        print(f'- {len(ccsi_list)} users in CrossSellModel have been updated. (order_status,updated_at)')

    def rewrite_all_users(self):
        pass

    #     Customer_CrossSell_Information.objects.all().delete()
    #     print('- Customer_CrossSell_Information have been cleaned.')

    #     submitted_date_limitation_list = list(Service_Base_Segment.objects.all().values_list('submitted_date_limitation',flat=True))
    #     submitted_date_limitation_values_list = []
    #     for sdl in submitted_date_limitation_list:
    #         submitted_date_limitation_values_list.append(int(sdl.split(' ')[0]))
        
    #     max_submitted_date_limitation = max(submitted_date_limitation_values_list)

    #     query_file_name = 'all_CCSM.sql'
    #     query_modifying_text_args = {'_submitted_date_limitation' : f'{max_submitted_date_limitation} days'}
    #     self.query.read_file(query_file_name=query_file_name)
    #     self.query.create_connection(
    #         product_name = 'ostadkar',
    #         connection_type = '',
    #         db_name = 'sale')
        
    #     data = self.query.read_data(arguments = query_modifying_text_args)

    #     service_base_segment_list = list(Service_Base_Segment.objects.all().\
    #         values_list('source_service',flat=True))
        
    #     data = data[data['service_base_segment_id'].isin(service_base_segment_list)]

    #     customers_mobile_list = list(Customer_Identification_Information.objects.\
    #         filter(mobile__in = data.mobile.unique()).values_list('mobile',flat=True))
        
    #     data = data[data['mobile'].isin(customers_mobile_list)]

    #     ccsi_list = []
    #     for i,row in data.iterrows():
    #         ccsi_list.append(Customer_CrossSell_Information(**row))
        
    #     Customer_CrossSell_Information.objects.bulk_create(ccsi_list)
    #     print('- {} users have been added to CrossSellModel.'.format(len(ccsi_list)))

    #     self.delete_old_users()