select
    c.mobile || '.' || o.id as name,
    c.mobile,
    o.id as order_id,
    o.status as order_status,
    o.created_at,
    o.updated_at,
    o.service_slug as service_base_segment_id
from order_history as oh
left join orders o on oh.order_id = o.id
left join customers c on o.customer_user_id = c.user_id
where oh.order_status = 'Submitted'
    and o.created_at > current_timestamp - interval '_submitted_date_limitation'
    and o.created_at > '2021-01-24'