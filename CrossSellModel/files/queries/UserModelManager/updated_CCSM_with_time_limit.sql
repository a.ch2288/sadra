select
    c.mobile,
    o.id as id,
    o.status as order_status,
    o.created_at,
    o.updated_at,
    o.service_slug as service_base_segment_id
--    c.mobile || '.' || o.id as name,
--    o.status as order_status,
--    o.updated_at
from order_history as oh
left join orders o on oh.order_id = o.id
left join customers c on o.customer_user_id = c.user_id
where oh.order_status = 'Submitted'
    and o.updated_at > current_timestamp - interval '_delta'
