from django.db import models
from django.utils import timezone
import uuid

# Create your models here.

class Service_Base_Segment(models.Model):
    name = models.CharField(primary_key=True,max_length=20)
    source_service = models.CharField(max_length=50,null=True)
    submitted_date_limitation = models.CharField(null=True,max_length=20)
    done_date_limitation = models.CharField(null=True,max_length=20)
    created_at = models.DateTimeField(default=timezone.now , null = True)

    def __str__(self):
        return self.name
    
    def get_fields_type():
        
        pk = 'name'

        fields_type = {
            'name' : 'str',
            'submitted_date_limitation' : 'str',
            'done_date_limitation' : 'str',
            'source_service' : 'str'

            }

        return pk,fields_type

class Customer_CrossSell_Information(models.Model):
    
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    mobile = models.CharField(max_length=11, null = True)
    order_status = models.CharField(max_length=255, null=True)
    service_base_segment = models.ForeignKey(Service_Base_Segment, on_delete = models.CASCADE,null = True)
    created_at = models.DateTimeField(default=timezone.now , null = True)
    updated_at = models.DateTimeField(default=timezone.now , null = True)


    def create_name(self):
        name = '{}.{}'.format(self.mobile,self.id)
        return name 