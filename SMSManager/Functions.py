import os
from django.db.models import Q
from django.conf import settings
import datetime as DT
import numpy as np
import random
from threading import Thread
import time

from .KavenegarConnection import KConnection,unix_tehran_to_date

from .models import Receiver
from ScenarioManager.models import Scenario_Member
# from RFMModel.models import *

from ScenarioManager.FilterManager import Filter_manager
from ScenarioManager.ModelManager import select_scenario_members
from ScenarioManager.models import Scenario,Scenario_Structure,Scenario_Member
from MessageManager.Functions import MessageTextCreator

def search(source_str,first_marker,second_marker):

    if (first_marker in source_str) and (second_marker in source_str):
        start = source_str.find(first_marker) + 1
        end = source_str.find(second_marker)
    else:
        return None
    
    return (source_str[start:end])

def dividing_list(primary_list,parts_len = None,parts_count = None ):

    random.shuffle(primary_list)
    primary_list_len = len(primary_list)

    if parts_count == 0 or primary_list_len == 0:
        return [[]]

    if (parts_len is None) and (parts_count is not None):
        parts_len = int(primary_list_len/parts_count) + int(primary_list_len%parts_count != 0)
    elif (parts_len is None) and (parts_count is None):
        return [[]]
    
    # divided_list = [primary_list[i:i+parts_len] for i in range(0,primary_list_len,parts_len)]
    # return (divided_list)

    for i in range(0,primary_list_len,parts_len):
        yield(primary_list[i:i+parts_len])

def get_receivers_set_with_time_limit(time_limit = None,value_list = None,value_set = None):

    target_receivers = Receiver.objects.all()

    if time_limit is not None:

        date_limit = DT.datetime.now() - DT.timedelta(minutes=time_limit)
        q0 = Q(created_at__gte = date_limit)
        target_receivers = target_receivers.filter(q0)
    
    if value_list is not None:
        target_receivers = list(target_receivers.values_list(value_list,flat=True))
    
    if value_set is not None:
        target_receivers = set(list(target_receivers.values_list(value_set,flat=True)))

    return target_receivers

class ReceiverManager:

    def __init__(self):

        self.bulk_size = 100
        self.sending_type = None
        self.minimum_time_step = {
            'weekly' : 3*24*60,
            'cross_sell' : 5*60,
            'online' : 0
            }
        # self.bulk_size = 1
        # self.k_conn = KConnection(settings.SENDER,settings.API_KEY)

    def update_scenarios_with_target_structure_type(self,structure_type,sending_type=None,using_threads = None):
        
        if sending_type is not None:
            self.sending_type = sending_type

        scenarios = Scenario.objects.filter(
            medium = 'sms',
            is_active=True,
            structure_type=structure_type
            )

        q1 = Q(scenario__in = scenarios)
        q2 = Q(message_is_active = True)
        structures = Scenario_Structure.objects.filter(q1 & q2).order_by('name')

        if using_threads:
            self.update_structures_using_threads(
                structures = structures,
                structure_type = structure_type
                )
        else:
            for structure in structures:
                self.update_structure(structure = structure,structure_type = structure_type)

    def update_structures_using_threads(self,structures,structure_type):

        my_threads = []
        threads_count_limit = 20        
        for structure in structures:

            action_thread = Thread(
                target = self.update_structure, 
                kwargs = {'structure' : structure,'structure_type' : structure_type}
                )
            action_thread.start()
            my_threads.append(action_thread)
            while len(my_threads) > (threads_count_limit - 1):
                thread_counter = threads_count_limit
                for t in my_threads:
                    if not t.is_alive():
                        thread_counter -= 1
                if thread_counter != threads_count_limit :
                    my_threads = [t for t in my_threads if t.is_alive()]
                time.sleep(1)
            
        for my_thread in my_threads:
            my_thread.join()
                
    def update_structure(self,structure,structure_type):

        time_step = self.minimum_time_step[structure_type] 
        date_limit = DT.datetime.now() - DT.timedelta(minutes=time_step)
        q0 = Q(created_at__gte = date_limit)
        recent_receivers = Receiver.objects.filter(q0)

        q0 = Q(action_need = True)
        q1 = Q(action = False)
        q2 = Q(status = structure)
        receivers_id_list = list(Scenario_Member.objects.filter(q0 & q1 & q2).\
            exclude(mobile__in = recent_receivers.values_list('mobile',flat=True)).\
                values_list('mobile',flat= True))

        if len(receivers_id_list) != 0:
            print(f'- creating message for {len(receivers_id_list)} recivers in {structure.name} has been started.')

        if len(receivers_id_list) > self.bulk_size:

            self.send_messages_with_dividing(
                receivers_id_primary_list = receivers_id_list,
                structure = structure,
                structure_name = structure.name
            )
        
        elif len(receivers_id_list) != 0:

            self.send_messages(
                structure = structure,
                structure_name = structure.name,
                receivers_id_list = receivers_id_list
                )

    def send_messages_with_dividing(self,receivers_id_primary_list,structure,structure_name):
        
        # receivers_id_primary_list = ['09190051230','09032542444','09366037615','09375892017','09176009818','09361178787','09369280094']

        receivers_id_divided_list = dividing_list(
            primary_list = receivers_id_primary_list,
            parts_len=self.bulk_size
            )

        my_threads = []
        threads_count_limit = 20 
        for receivers_id_list in receivers_id_divided_list:

            if len(receivers_id_list) != 0 :

                action_thread = Thread(
                    target = self.send_messages, 
                    kwargs={
                        'structure' : structure,
                        'structure_name' : structure_name,
                        'receivers_id_list' : receivers_id_list
                        }
                    )
                action_thread.start()
                my_threads.append(action_thread)
                while len(my_threads) > (threads_count_limit - 1):
                    thread_counter = threads_count_limit
                    for t in my_threads:
                        if not t.is_alive():
                            thread_counter -= 1
                    if thread_counter != threads_count_limit :
                        my_threads = [t for t in my_threads if t.is_alive()]
                    time.sleep(1)
                
        for my_thread in my_threads:
            my_thread.join()
                                    
    def send_messages(self,structure,structure_name,receivers_id_list):

        # if len(messages_text) == len(date) and len(messages_text) != 0:
        
        counter = 0
        if len(receivers_id_list) != 0:

            message_text_creator = MessageTextCreator()

            message_text_creator.pre_initialization(
                structure_name = structure_name,
                receivers_id_list = receivers_id_list
            )

            messages_text = message_text_creator.create_messages()

            date = self.create_sending_time_list(
                sending_type = structure.message_sending_time,
                count = 1
                )[0]

            if len(messages_text) != 0 :
                k_conn = KConnection(settings.SENDER,settings.API_KEY)
                response = k_conn.send_array(receivers_id_list,messages_text,date)
                receivers = []
                try:
                    for row in response:
                        kavenegar_id = row['messageid']
                        mobile = str(row['receptor'])
                        sent_at = unix_tehran_to_date(date_time=row['date'])

                        r = Receiver(
                            mobile = mobile,
                            message_name = structure_name,
                            sent_at = sent_at,
                            kavenegar_id = kavenegar_id)
                        
                        name = r.create_name()

                        receivers.append(r)
                    
                    Receiver.objects.bulk_create(receivers)
                    counter += len(receivers)
                    print('- {} ({}) of messages has been sent. ({})'.format(len(receivers),counter,structure_name))
                except Exception as e:
                    print(e)

            else:
                print('Error in message sender : messages list is empty!')
                
                
        # elif len(messages_text) != len(date):
        #     print('Error in message sender : messages list does not not equal to dates list!')
        # else:
        #     print('Error in message sender : messages list is empty!')

    def test_send(self,structure=None,receivers_id_list=None,sending_time = None):

        if receivers_id_list is None:
            receivers_id_list = ['09190051230',
                                '09032542444',
                                '09366037615',
                                '09375892017',
                                '09176009818',
                                '09361178787',
                                '09369280094',
                                '09309766630',
                                '09193143760']
        if structure is not None:
            structures_name = [structure.name]
        else:
            scenarios = Scenario.objects.filter(
                medium = 'sms',
                is_active = True,
                structure_type = 'weekly'
                )
            structures_name = []

            for scenario in scenarios:
                if 'Segment1' in scenario.name or 'Segment1' in scenario.name :
                    structures_name.extend(list(scenario.scenario_structure_set.filter(message_is_active = True).values_list('name',flat=True)))
        
        message_text_creator = MessageTextCreator()
        k_conn = KConnection(settings.SENDER,settings.API_KEY)

        for receiver in receivers_id_list:

            date = self.create_sending_time_list(
                sending_type = 'Online$5',
                count = 1
                )[0]

            structure_name = random.choice(structures_name)

            message_text_creator.pre_initialization(
                structure_name = structure_name,
                receivers_id_list = [receiver]
            )

            messages_text = message_text_creator.create_messages()

            response = k_conn.send_array([receiver],messages_text,date)
            # print(response)
            
    def create_sending_time_list(self,sending_type,count = 1):

        if self.sending_type is None:
            sending_type , sending_type_args = sending_type.split('$')[0],sending_type.split('$')[1:]
        else:
            sending_type , sending_type_args = self.sending_type.split('$')[0],self.sending_type.split('$')[1:]
        
        if sending_type == 'Normal':

            mu, sigma = float(sending_type_args[0]), 0.4
            sending_hour_list = np.random.normal(mu, sigma, count)

            working_time = DT.datetime.now()
            sending_day = working_time.replace(
                second=0, 
                microsecond=0, 
                minute=0,
                hour = 0
                )

            sending_time_list = []
            for sending_hour in sending_hour_list:

                sending_time = sending_day + DT.timedelta(hours=sending_hour)
                if sending_time < working_time:
                    sending_time = sending_time + DT.timedelta(hours=24)
                sending_time_list.append(sending_time)
            
            return sending_time_list
        
        elif sending_type == 'Fix':

            working_time = DT.datetime.now()
            sending_day = working_time.replace(
                second=0, 
                microsecond=0, 
                minute=0,
                hour = 0, 
                day=(working_time.day +int(working_time.hour > 12))
                )

            sending_hour_list = [float(sending_type_args[0])]

            sending_time_list = []
            for sending_hour in sending_hour_list:
                sending_time = sending_day + DT.timedelta(hours=sending_hour)
                sending_time_list.append(sending_time)

            return sending_time_list
        
        elif sending_type == 'Online':

            sending_time = DT.datetime.now() + DT.timedelta(minutes=float(sending_type_args[0]))
            
            sending_time_list = [sending_time]

            return sending_time_list
