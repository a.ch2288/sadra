from django.apps import AppConfig


class SmsmanagerConfig(AppConfig):
    name = 'SMSManager'
