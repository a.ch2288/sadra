import os
from django.conf import settings
import datetime as DT

# try:
#     from ._libs import *
# except:
#     from _libs import *

# from SMSManager.models import *
# from RFMModel.models import *
# from CustomerManager.models import *

# def dividing_list(primary_list,parts_len):
        
#     random.shuffle(primary_list)
#     primary_list_len = len(primary_list)
#     if primary_list_len != 0:
#         divided_list = [primary_list[i:i+parts_len] for i in range(0,primary_list_len,parts_len)]
#     else:
#         divided_list = [[]]
    
#     return (divided_list)


# class Sending_manager:
    
#     def __init__(self):
#         self.k_conn = KConnection(settings.SENDER,settings.API_KEY)
#         self.bulk_size = 100

#         self.text = """"""
#         self.arguments = None

#         self.today = None
#         self.sending_datetime = None

#         self.identify_working_day()
#         self.create_sending_datetime()

#     def create_sending_datetime(self):
#         today = DT.date.today()
#         d1 = today.strftime("%Y-%m-%d")
#         d1 = '{} 07:30'.format(d1)
#         datetime_object = DT.datetime.strptime(d1, '%Y-%m-%d %H:%M')
#         self.sending_datetime = datetime_object

#     def identify_working_day(self): 

#         WeekDay_int_to_str = {
#             0 : 'Mon',
#             1 : 'Tue',
#             2 : 'Wed',
#             3 : 'Thu',
#             4 : 'Fri',
#             5 : 'Sat',
#             6 : 'Sun'
#         }
#         WeekDay_int = int(DT.datetime.now().weekday())

#         self.today =WeekDay_int_to_str[WeekDay_int]
    
#     def send_all_message_text(self):

#         message_texts = Message_Text.objects.all().order_by('name')
#         for message_text in message_texts:
#             name = message_text.name
#             sending_day = message_text.sending_day
#             if sending_day == self.today :
#                 if 'Segment1' in name:
#                     self.test_send_sms_push(message_text)
#                 self.send_sms_push(message_text)
    
#     def replace_text(self,text_arguments,template_arguments):
#         txt = self.text[:]
#         if '_service_rate' in template_arguments:
#             template_arguments = '_service_rate${}'.format(template_arguments)

#         template_arguments = template_arguments.split('$')
        
#         for argument in template_arguments:
#             try:
#                 txt = txt.replace(argument,text_arguments[argument])
#             except:
#                 print(template_arguments,text_arguments)
#                 exit()

#         return txt

#     def test_send_sms_push(self,message_text):

#         template = message_text.template
#         self.text = template.text
#         template_arguments = template.arguments

#         text_arguments = {}

#         promotion = message_text.promotion
#         text_arguments['_code'] = promotion.code
#         text_arguments['_percent'] = promotion.percentage
#         text_arguments['_value'] = promotion.value

#         receivers_list = list(message_text.receiver_set.filter(kavenegar_id__isnull = True).order_by('name'))

#         divided_receivers_list = dividing_list(receivers_list,self.bulk_size)
#         for part in divided_receivers_list:
#             updated_receivers = []
#             txts = []
#             mobiles = []
#             count = 0
#             partt = part[0:1]
#             for receiver in partt:
#                 x = 1
#                 mobile = receiver.object_id
                
#                 text_arguments['_name'] = receiver.content_object.first_name
#                 text_arguments['_link'] = receiver.short_link

#                 if ('_service' in template_arguments) or ('_service_rate' in template_arguments):
#                     if message_text.service_slug == 'LastOrderService':

#                         UEI = User_Extra_Information.objects.filter(user_id = mobile)
#                         if len(UEI) != 0:
#                             UEI = UEI[0]
#                             service_slug = UEI.last_order_service_slug
#                             service = Service.objects.get(slug = service_slug)
#                             text_arguments['_service'] = service.persian_name
#                             text_arguments['_service_rate'] = service.satisfaction
#                         else:
#                             print('User_Extra_Information',mobile)
#                             x = 0
#                     elif message_text.service_slug != 'all':

#                         service = Service.objects.get(slug = message_text.service_slug)
#                         text_arguments['_service'] = service.persian_name
#                         text_arguments['_service_rate'] = service.satisfaction
                
#                 if ('_day' in template_arguments) or ('_month' in template_arguments):

#                     CLI = Customer_Login_Information.objects.filter(customer_id = mobile)
#                     if len(CLI) != 0:
#                         CLI = CLI[0]
#                         last_login = CLI.last_login
#                         day = (DT.datetime.now() - last_login).days
#                         month = str(int(day/30))
#                         day = str(day)
#                         number_translator = {
#                             '1' : '۱',
#                             '2' : '۲',
#                             '3' : '۳',
#                             '4' : '۴',
#                             '5' : '۵',
#                             '6' : '۶',
#                             '7' : '۷',
#                             '8' : '۸',
#                             '9' : '۹',
#                             '0' : '۰',
#                             '.' : '.'
#                         }

#                         p_day = ''
#                         for c in day:
#                             p_day = p_day + number_translator[c]
                        
#                         p_month = ''
#                         for c in month:
#                             p_month = p_month + number_translator[c]

#                         text_arguments['_day'] = p_day
#                         text_arguments['_month'] = p_month
#                     else:
#                         print('User_Extra_Information',mobile)
#                         x = 0

#                 if x == 1 :
#                     txt = self.replace_text(text_arguments,template_arguments)

#                     mobiles.append(mobile)
#                     txts.append(txt)
#             if len (txts) != 0:
#                 date = (self.sending_datetime)
#                 mobiles = ['09190051230']
#                 response = self.k_conn.send_array(mobiles,txts,date)
#                 print(response)
#                 break

#     def send_sms_push(self,message_text):

#         template = message_text.template
#         self.text = template.text
#         template_arguments = template.arguments

#         text_arguments = {}

#         promotion = message_text.promotion
#         text_arguments['_code'] = promotion.code
#         text_arguments['_percent'] = promotion.percentage
#         text_arguments['_value'] = promotion.value

#         receivers_list = list(message_text.receiver_set.filter(kavenegar_id__isnull = True).order_by('name'))

#         divided_receivers_list = dividing_list(receivers_list,self.bulk_size)
#         for part in divided_receivers_list:
#             updated_receivers = []
#             txts = []
#             mobiles = []
#             count = 0
#             for receiver in part:
#                 x = 1
#                 mobile = receiver.object_id
                
#                 text_arguments['_name'] = receiver.content_object.first_name
#                 text_arguments['_link'] = receiver.short_link

#                 if ('_service' in template_arguments) or ('_service_rate' in template_arguments):
#                     if message_text.service_slug == 'LastOrderService':

#                         UEI = User_Extra_Information.objects.filter(user_id = mobile)
#                         if len(UEI) != 0:
#                             UEI = UEI[0]
#                             service_slug = UEI.last_order_service_slug
#                             service = Service.objects.get(slug = service_slug)
#                             text_arguments['_service'] = service.persian_name
#                             if service.satisfaction != None:
#                                 text_arguments['_service_rate'] = service.satisfaction
#                             else:
#                                 text_arguments['_service_rate'] = '۸۰.۲۲'

#                         else:
#                             print('User_Extra_Information',mobile)
#                             x = 0
#                     elif message_text.service_slug != 'all':

#                         service = Service.objects.get(slug = message_text.service_slug)
#                         text_arguments['_service'] = service.persian_name
#                         text_arguments['_service_rate'] = service.satisfaction
                
#                 if ('_day' in template_arguments) or ('_month' in template_arguments):

#                     CLI = Customer_Login_Information.objects.filter(customer_id = mobile)
#                     if len(CLI) != 0:
#                         CLI = CLI[0]
#                         last_login = CLI.last_login
#                         day = (DT.datetime.now() - last_login).days
#                         month = str(int(day/30))
#                         day = str(day)
#                         number_translator = {
#                             '1' : '۱',
#                             '2' : '۲',
#                             '3' : '۳',
#                             '4' : '۴',
#                             '5' : '۵',
#                             '6' : '۶',
#                             '7' : '۷',
#                             '8' : '۸',
#                             '9' : '۹',
#                             '0' : '۰',
#                             '.' : '.'
#                         }

#                         p_day = ''
#                         for c in day:
#                             p_day = p_day + number_translator[c]
                        
#                         p_month = ''
#                         for c in month:
#                             p_month = p_month + number_translator[c]

#                         text_arguments['_day'] = p_day
#                         text_arguments['_month'] = p_month
#                     else:
#                         print('User_Extra_Information',mobile)
#                         x = 0


#                 if x == 1 :
#                     txt = self.replace_text(text_arguments,template_arguments)

#                     mobiles.append(mobile)
#                     txts.append(txt)


#             if len (txts) != 0:
#                 date = self.sending_datetime
#                 response = self.k_conn.send_array(mobiles,txts,date)
#                 updated_receivers = []
#                 for row in response:
#                     messageid = row['messageid']
#                     mobile = str(row['receptor'])
#                     r = Receiver.objects.filter(message_text = message_text).filter(object_id = mobile)
#                     if len(r) > 1:
#                         print(r)
#                     r = r[0]
#                     r.kavenegar_id = messageid
#                     r.sent_at = self.sending_datetime
#                     updated_receivers.append(r)
                
#                 Receiver.objects.bulk_update(updated_receivers, ['kavenegar_id','sent_at'])
#                 print('{} of messages had been sent'.format(len(updated_receivers)))



# if __name__ == "__main__":
#     s_manager = Sending_manager()
#     s_manager.send_all_message_text()