
from kavenegar import KavenegarAPI,APIException,HTTPException
import json
import pandas as pd

import time
import datetime
import pytz

### https://avilpage.com/2014/11/python-unix-timestamp-utc-and-their.html#:~:text=To%20convert%20Unix%20timestamp%20to%20UTC%20we%20can%20use%20utcfromtimestamp%20function.&text=To%20convert%20UTC%20time%20object,we%20can%20use%20strftime%20function.

def date_to_unix_tehran(date_time = '2020-07-25 01:23:0'):
    if type(date_time) is datetime.datetime:
        date_time_obj = date_time
    elif (type(date_time) is str) and (date_time != ''):
        date_time_obj = datetime.datetime.strptime(date_time, '%Y-%m-%d %H:%M:%S')
    elif (date_time == ''):
        return ''
    else:
        raise TypeError('date_to_unix_tehran: only strings or datetime.datetime objects are allowed')
    tehran_tz = pytz.timezone("Asia/Tehran")
    UTC_tz = pytz.timezone("UTC")
    UTC = UTC_tz.localize(date_time_obj)
    tehran = tehran_tz.normalize(UTC.astimezone(tehran_tz))
    tehran = tehran.replace(tzinfo=None)
    try:
        unixtime = time.mktime(tehran.timetuple())
        # print('1 - ',unixtime)
    except:
        unixtime = date_time.strftime("%s")
        # print('2 - ',unixtime)
    
    return(int(unixtime))

def unix_tehran_to_date(date_time = 1596396085):

    date_time = int(date_time)
    date_time = datetime.datetime.utcfromtimestamp(date_time)
    tehran_tz = pytz.timezone("Asia/Tehran")
    UTC_tz = pytz.timezone("UTC")
    tehran = tehran_tz.localize(date_time)
    UTC = UTC_tz.normalize(tehran.astimezone(UTC_tz))

    return(UTC)

class KConnection:

    def __init__(self,sender,api_key):
        self.sender = sender
        self.api_key = api_key
        self.create_kavenegar_api()

    def create_kavenegar_api(self):
        try:
            self.api = KavenegarAPI(self.api_key)
            print('Kavenegar Connection had been created')
            # in bayad kamel she vase vaghti vasl nemishe
        except APIException as e: 
            print(e)
        except HTTPException as e: 
            print(e)

    def send_single(self,receptor,message,date = ''):
        api = self.api
        sender = self.sender
        date = date_to_unix_tehran(date)
        
        try:
            params = {
                'sender': '{}'.format(sender),
                'receptor': '{}'.format(receptor),
                'message': '{}'.format(message),
                'date': '{}'.format(date),
            }

            response = api.sms_send(params)
            return(response)
            #seri ghabl ye halati pish oomad ke in khaili montazere moond dastan chi boode ??
        except APIException as e: 
            print(e)
        except HTTPException as e: 
            print(e)

    def send_array(self,receptor,message,date = ''):

        api = self.api
        sender = self.sender

        bulk_count = len(receptor)
        if bulk_count > 200:
            raise Exception('in send_array function bulk_count have to be less than 200')

        sender = [sender]*bulk_count
        
        if (type(message) is list) and (len(message) == 1):
            message = message*bulk_count
            # message = '","'.join(message)
        elif (type(message) is list) and (len(message) == bulk_count):
            message = message
        elif (type(message) is list) and (len(message) != bulk_count):
            raise Exception('send_array: message count is wrong')
        elif type(message) is str :
            message = [message]
            message = message*bulk_count
            # message = '","'.join(message)
        else:
            raise TypeError('send_array: message type isnt allowed')
        
        if type(date) is list and len(date) == bulk_count:
            date_in_unix = []
            for d in date:
                date_in_unix.append(date_to_unix_tehran(d))
            date = date_in_unix
        elif type(date) is list and len(date) != bulk_count:
            raise Exception('send_array: date count is wrong')
        else:
            date = date_to_unix_tehran(date)
        # print(date)
        if (type(receptor) is list) and (len(receptor) == bulk_count):
            receptor = receptor
        elif (type(receptor) is list) and (len(receptor) != bulk_count):
            raise Exception('send_array: receptor count is wrong')
        else:
            raise TypeError('send_array: receptor type isnt allowed')

        try:
            params = {
                'sender': '{}'.format(sender),
                'receptor': '{}'.format(receptor),
                'message': '{}'.format(message),
                'date': '{}'.format(date),
            }
            # print(params)
            response = api.sms_sendarray(params)
            return(response)
        except APIException as e: 
            print(e)
        except HTTPException as e: 
            print(e)

    def select_by_messageid(self,messageids):
        api = self.api

        if type(messageids) is list :
            messageids = ','.join(messageids)
        else:
            pass # badan ezafe she :))
        
        params = {
            'messageid':'{}'.format(messageids),
        }

        responses = api.sms_select(params)
        responses = pd.DataFrame(responses)

        return(responses)

    def select_by_sent_date(self,start_at,end_at):
        api = self.api
        sender = self.sender
        
        start_at_unix = date_to_unix_tehran(start_at)
        end_at_unix = date_to_unix_tehran(end_at)
        one_day = int(60*60*24)
        
        for day in range(start_at_unix,end_at_unix,one_day):

            while True:
                try:
                    params = {
                        'startdate' : '{}'.format(day),
                        'enddate' : '{}'.format(day + one_day),
                        'sender' : '{}'.format(sender),
                        }
                    
                    response = api.sms_selectoutbox(params)

                    if len(response) != 0:
                        response = pd.DataFrame(response)
                        yield(response)
                    else:
                        print('pekheeee')
                        break
                except APIException as e: 
                    print(e)
                except HTTPException as e: 
                    print(e)

    def countinbox_array(self,day_count):
        api = self.api
        sender = self.sender

        today = time.time()

        responses = []
        for j in [0,1]:
            for i in range(day_count):
                
                params = {
                    'startdate' : '{}'.format(int(today - (i + 1)*24*3600)),
                    'enddate' : '{}'.format(int(today - i *24*3600)),
                    'linenumber' : '{}'.format(sender),
                    'isread' : j,
                    }
                
                response = api.sms_countinbox(params)
                
                responses.append(response[0])

        responses = pd.DataFrame(responses)
        return(responses)

    def received_msg(self):

        api = self.api
        sender = self.sender

        responses = []
        for j in [0,1]:
                
            params = {
                'linenumber' : '{}'.format(sender),
                'isread' : j,
                }
            
            response = api.sms_receive(params)

            if response != None:
                responses.extend(response)

        responses = pd.DataFrame(responses)
        responses = responses.sort_values(by= ['date'], ascending= False)
        return(responses)

    def cancel_by_messageid(self,messageids):
        
        api = self.api

        if type(messageids) is list :
            messageids = ','.join(messageids)
        else:
            pass # badan ezafe she :))
        
        params = {
            'messageid':'{}'.format(messageids),
        }

        responses = api.sms_cancel(params)
        responses = pd.DataFrame(responses)

        return(responses)