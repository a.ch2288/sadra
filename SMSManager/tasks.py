from celery import shared_task
from .Functions import ReceiverManager
from datetime import datetime

receiver_manager = ReceiverManager()


@shared_task
def high_frequency_message_sending(structure_type,using_threads = True):
    if 20 < datetime.now().hour or datetime.now().hour < 3.5:
        print('rest time! :)')
        
    else:
        receiver_manager = ReceiverManager()
        receiver_manager.update_scenarios_with_target_structure_type(
            structure_type = structure_type,
            using_threads = using_threads)
