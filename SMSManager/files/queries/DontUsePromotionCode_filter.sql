with Vouchers_id as (
	SELECT 
		"id" as voucher_id
	FROM
		vouchers 
	where 
		code in ('_promotion_code')
)

SELECT 
	c.mobile
FROM 
	orders as o
RIGHT JOIN Vouchers_id as v using (voucher_id)
INNER JOIN customers as c on o.customer_user_id = c.user_id

ORDER BY
	o.voucher_id