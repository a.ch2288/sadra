import os

# Create your tests here.


# from .models import Receiver
# from RFMModel.models import User_Identification_Information

from datetime import datetime as dt
import pandas as pd
# def import_receivers():
    
#     scenario_name = {
#         'first_scenario':'Segment3_first scenario_Service introduction-1',
#         'second_scenario':'Segment6_first scenario_Ostadkar Characteristic-1'
#         }
#     phase_name = {
#         'a':'1-1-1-1',
#         'b':'2-1-1-1',
#         'c':'3-1-1-1',
#         'd':'4-1-1-1',
#         'e':'5-1-1-1',
#         'f':'6-1-1-1'
#         }

#     start = 0
#     bulk_count = 5000

#     while True:
#         print('{} - {} started'.format(start,start + bulk_count))
#         users = Receiver1.objects.all().order_by('user_id')[start:start + bulk_count]
        
#         if len(users) == 0:
#             break

#         new_receivers = []
#         for u in users:
#             name = '{mobile}-{created_at}'.format(mobile = u.user_id, created_at = u.sent_at)
#             name = name.replace(' ','-')
#             message_text_name = '{scenario}-{phase}-{typee}'.format(
#                 scenario = scenario_name[u.scenario_id],
#                 phase = phase_name[u.phase_id[-1]],
#                 typee = u.msg_id[-1]
#             )

#             message_text = Message_Text.objects.get(
#                 name = message_text_name
#             )

#             new_receiver = Receiver(
#                 name = name,
#                 message_text = message_text,
#                 short_link = u.short_link,
#                 clicked_at = u.clicked_at,
#                 sent_at = u.sent_at,
#                 kavenegar_id = u.kavenegar_id,
#                 kavenegar_status = u.kavenegar_status,
#                 archive = u.archive,
#                 content_object = u.user
#                 )

#             new_receivers.append(new_receiver)


#         Receiver.objects.bulk_create(new_receivers)
#         print('{} - {} ended'.format(start,start + bulk_count))
#         start = start + bulk_count

# def modified_receiver():

#     receivers = list(Receiver.objects.all().filter(user_id = 15))
#     for receiver in receivers:
#         mobile = receiver.object_id
#         user = User_Identification_Information.objects.get(mobile = mobile)
#         receiver.content_object = user
#         receiver.save()

def import_receivers_backup():
    file_name = 'Receiver.json'
    file_path = os.path.join(os.path.dirname(__file__), 'files')
    file_path = os.path.join(file_path,file_name)

    text = open(file_path,'r').read()
    text = text.replace('"user": 82,','"user": 24,')
    
    new_file = open('Receiver.json','w')
    new_file.write(text)
    new_file.close()





if __name__ == "__main__":
    # import_receivers()
    # modified_receiver()
    import_receivers_backup()
    
    
