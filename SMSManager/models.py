from django.db import models

from django.utils import timezone
# Create your models here.


class Receiver(models.Model):

    name = models.CharField(primary_key= True,max_length=255)
    mobile = models.CharField(null=True,max_length=255)
    message_name = models.CharField(null= True,max_length=255)
    sent_at = models.DateTimeField(null = True)
    kavenegar_id = models.IntegerField(null = True)
    kavenegar_status = models.IntegerField(null = True)
    created_at = models.DateTimeField(default=timezone.now)

    def create_name(self):
        name = '{}.{}.{}'.format(
            self.mobile,
            self.message_name,
            self.sent_at
        )
        self.name = name
        return name
