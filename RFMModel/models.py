from django.db import models

from django.utils import timezone

# from SMSManager.models import Scenario


# Create your models here.

class Order_Base_Segment(models.Model):
    name = models.CharField(primary_key=True,max_length=20)
    done_date_limitation  = models.CharField(max_length=20, null = True)
    submitted_date_limitation = models.CharField(max_length=20, null = True)
    submitted_count_limitation = models.CharField(max_length=20)
    done_count_limitation = models.CharField(max_length=20)
    created_at = models.DateTimeField(auto_now_add =True , null = True)
    # sms_scenario = GenericRelation(Scenario)

    def __str__(self):
        return self.name

class Customer_RFM_Information(models.Model):
    
    mobile = models.CharField(primary_key=True, unique=True, max_length=11)
    submitted_count = models.PositiveSmallIntegerField(null=True, default = 0)
    done_count = models.PositiveSmallIntegerField(null=True, default = 0)
    recency_time = models.FloatField(null=True)
    engaged_time = models.FloatField(null=True)
    retention_time = models.FloatField(null=True)
    age_time = models.FloatField(null=True)
    criterion = models.FloatField(null=True)
    order_base_segment = models.ForeignKey(Order_Base_Segment, on_delete = models.CASCADE,null = True)
    created_at = models.DateTimeField(default=timezone.now, null = True)
    
class Customer_Segment_History(models.Model):
    
    mobile = models.CharField(max_length=20, null=True)
    order_base_segment_name = models.CharField(max_length=20, null=True)
    created_at = models.DateTimeField(default=timezone.now, null = True)
