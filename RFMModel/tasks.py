from celery import shared_task
from .ModelManager import UserModelManager,SegmentModelManager
from datetime import datetime

user_manager = UserModelManager()
user_managerr = UserModelManager()
segment_manager = SegmentModelManager()
check = [True]

@shared_task
def rewrite_customers_rfm_info():
    user_managerr.rewrite_customers_rfm_info()


@shared_task
def insert_new_customers_to_rfm(time_limit = None):

    if 20 < datetime.now().hour or datetime.now().hour < 3:
        print('rest time! :)')
        check[0] = True
    else:
        if check[0] == True:
            time_limit = 10*60
            check[0] = False

        if time_limit is not None:
            user_manager.write_new_customer_rfm_info_with_time_limit(time_limit=int(time_limit))
        else:
            pass

@shared_task
def update_segment_model_info():
    segment_manager.update_all()