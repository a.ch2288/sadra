WITH all_orders AS (
	SELECT DISTINCT ON
		( order_history.order_id ) order_history.order_id,
		orders.customer_user_id AS user_id,
		order_history.created_at AS t2,
		orders.service_slug,
	    orders.status
	FROM
		order_history
		LEFT JOIN orders ON ( order_history.order_id = orders."id" )
	WHERE
		order_history.order_status = 'Submitted'
		AND order_history.created_at > ( CURRENT_TIMESTAMP - INTERVAL '6 months')
	)
	,sorted_all_orders AS (
	SELECT
	    *
	FROM
	    all_orders
	ORDER BY
	    user_id, t2
	)
	,leaded_all_orders AS (
	SELECT
		*,
		LEAD ( t2, 1 ) OVER ( PARTITION BY user_id ORDER BY user_id, t2 ) AS t1,
		LEAD ( t2, 1 ) OVER ( PARTITION BY user_id ORDER BY user_id, t2 ) - t2 AS delta
	FROM
		sorted_all_orders
	)
	,filtered_orders AS (
	SELECT
		*
	FROM
		leaded_all_orders
	WHERE
		delta > INTERVAL '1 day' OR t1 IS NULL OR status IN ( 'Done', 'WithFeedback', 'Finished' )
	order by
	    user_id,t2 desc
	)
    ,last_order_slug as (
    select distinct on (user_id)
        user_id
        ,service_slug
        ,t2
    from
        filtered_orders
    )
select
    c.mobile as mobile
    ,service_slug as service_slug
from
    last_order_slug as l
left join customers as c using (user_id)
