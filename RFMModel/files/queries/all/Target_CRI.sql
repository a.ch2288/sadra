-- All Target Customer RFM Information (sale DB)
WITH
    target_customers as (
    select
        *
    from customers as c
    where c.mobile in ('_mobiles_list')
    )
    , target_customers_orders as (
    select
        o.id as order_id
    from target_customers as tc
    left join orders as o on tc.user_id = o.customer_user_id
    )
    , all_orders_id as (
    select distinct (oh.order_id)
    from order_history as oh
    right join target_customers_orders as tco using (order_id)
    where oh.order_status = 'Submitted'
      and oh.created_at > (current_timestamp - INTERVAL '_submitted_date_limitation')
    )
    , all_orders as (
    select o.id               as order_id
         , o.customer_user_id as user_id
         , o.created_at       as t2
         , o.status
         , o.service_slug
    from orders as o
             right join all_orders_id as aoi on (aoi.order_id = o.id)
    where o.status in ('Done', 'WithFeedback', 'Finished','Canceled')
    order by
    o.customer_user_id,o.created_at
    )
	, leaded_all_orders AS (
    SELECT *
         , LEAD(t2, 1) OVER ( PARTITION BY user_id ORDER BY user_id, t2 )      AS t1
         , LEAD(t2, 1) OVER ( PARTITION BY user_id ORDER BY user_id, t2 ) - t2 AS delta
    FROM all_orders
    )
	, filtered_orders AS (
    SELECT *
    FROM leaded_all_orders
    WHERE delta > INTERVAL '1 day'
       OR t1 IS NULL
       OR status IN ('Done', 'WithFeedback', 'Finished')
    )
	, leaded_filtered_orders AS (
    SELECT user_id,
           t2,
           (LEAD(t2, 1) OVER ( PARTITION BY user_id ORDER BY user_id, t2 )) AS t1,
           (CASE
                WHEN (LEAD(t2, 1) OVER ( PARTITION BY user_id ORDER BY user_id, t2 )) IS NULL THEN CURRENT_TIMESTAMP
                ELSE (LEAD(t2, 1) OVER ( PARTITION BY user_id ORDER BY user_id, t2 ))
               END
               )                                                            AS age,
           status
    FROM filtered_orders
    ORDER BY user_id,
             t2
    )
	, filtered_orders_with_delta AS (
    SELECT user_id,
           status,
           t1 - t2  AS delta_engagement,
           age - t2 AS delta_age
    FROM leaded_filtered_orders
    )
    , done_and_submitted AS (
    SELECT user_id,
           COUNT(*)                               AS submitted_count,
           COUNT(*) filter ( where status IN ('Done', 'WithFeedback', 'Finished')) as Done_count,
           SUM(delta_engagement)                  AS engagement_time,
           SUM(delta_age)                         AS age_time,
           SUM(delta_engagement) / (COUNT(*) - 1) AS retention_time
    FROM filtered_orders_with_delta
    GROUP BY user_id
    ORDER BY COUNT(*) DESC
    )
    , users_RFM_info as (
    SELECT user_id,
           submitted_count,
           (CASE WHEN done_count IS NULL THEN 0 ELSE done_count END),
           (CASE WHEN engagement_time IS NULL THEN INTERVAL '0 day' ELSE engagement_time END),
           age_time,
           age_time / submitted_count AS criterion,
           retention_time
    FROM done_and_submitted
    ORDER BY submitted_count DESC
    )
    , all_users_info as (
    select user_id,
           mobile,
           (case when submitted_count is null then 0 else submitted_count end),
           (case when done_count is null then 0 else done_count end),
           EXTRACT(epoch FROM age_time) / (24 * 3600)                     AS age_time,
           EXTRACT(epoch FROM engagement_time) / (24 * 3600)              AS engaged_time,
           EXTRACT(epoch FROM (age_time - engagement_time)) / (24 * 3600) AS recency_time,
           EXTRACT(epoch FROM retention_time) / (24 * 3600)               AS retention_time,
           EXTRACT(epoch FROM criterion) / (24 * 3600)                    AS criterion
    from users_RFM_info
    right join target_customers using (user_id)
    where mobile <> '09353942996'
    )
select
	mobile,
	submitted_count,
	done_count,
	age_time,
	engaged_time,
	recency_time,
	retention_time,
	criterion
from
	all_users_info
