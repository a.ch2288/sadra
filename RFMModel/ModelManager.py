import os
import pandas as pd
import operator
import datetime as DT
import random
from django.db.models import Q

from .models import *

from ForeignDBConnection.QueryManager import Query
from CustomerManager.ModelManager import read_customers_list_with_time_limit
from CustomerManager.models import Customer_Identification_Information,Customer_Login_Information

def dividing_list(primary_list,parts_len):
        
    random.shuffle(primary_list)
    primary_list_len = len(primary_list)
    if primary_list_len != 0:
        divided_list = [primary_list[i:i+parts_len] for i in range(0,primary_list_len,parts_len)]
    else:
        divided_list = [[]]
    
    return (divided_list)

def identify_segment(submitted_count_str,done_count_str):
    operator_switcher = {
        '>' : operator.gt,
        '=' : operator.eq,
        '<' : operator.lt,
        '<=': operator.le,
        '>=': operator.ge
    }
    segments = Order_Base_Segment.objects.all()
    for segment in segments:

        done_operator_str,done_count_limit_str = segment.done_count_limitation.split(' ')
        done_operator = operator_switcher[done_operator_str]
        done_count_limit = int(done_count_limit_str)

        submitted_operator_str,submitted_count_limit_str = segment.submitted_count_limitation.split(' ')
        submitted_operator = operator_switcher[submitted_operator_str]
        submitted_count_limit = int(submitted_count_limit_str)

        if submitted_operator(int(submitted_count_str),submitted_count_limit) and done_operator(int(done_count_str),done_count_limit) :
            return segment.name
    
    return None

def classifying_customers(segment,data):

    done_operator_str,done_count_limit_str = segment.done_count_limitation.split(' ')
    done_count_limit = int(done_count_limit_str)
    done_operator_switcher = {
        '>' : data['done_count'].gt,
        '=' : data['done_count'].eq,
        '<' : data['done_count'].lt,
        '<=': data['done_count'].le,
        '>=': data['done_count'].ge
    }

    submitted_operator_str,submitted_count_limit_str = segment.submitted_count_limitation.split(' ')
    submitted_count_limit = int(submitted_count_limit_str)
    submitted_operator_switcher = {
        '>' : data['submitted_count'].gt,
        '=' : data['submitted_count'].eq,
        '<' : data['submitted_count'].lt,
        '<=': data['submitted_count'].le,
        '>=': data['submitted_count'].ge
    }

    data = data[done_operator_switcher[done_operator_str](done_count_limit) & \
        submitted_operator_switcher[submitted_operator_str](submitted_count_limit)]

    return(data)


def check_customer_segment_history(mobile,segment_type,current_segment_id):
    customer_segments_history = Customer_Segment_History.objects.filter(mobile = mobile).order_by('-created_at')
    if len(customer_segments_history) > 0:
        customer_last_segment_id = customer_segments_history[0].order_base_segment_name
        if customer_last_segment_id == current_segment_id:
            return False

    return True

class SegmentModelManager:

    def __init__(self):
        self.file_name = 'Segment_info.xlsx'
        self.GoogleSheetID = '1uCKjviIoI-059e_gLeOVIcKSrm-RXl3rFZ-W5UuY2pk'
        self.SheetsName = ['order_base_segment']

        self.segment_info = None
    
    def update_all(self):
        self.read_info()
        self.update_segment_info()

    def update_segment_info(self):
        
        for i,row in self.segment_info.iterrows():

            name = row['name']
            done_date_limitation  = row['done_date_limitation']
            submitted_date_limitation = row['submitted_date_limitation']
            submitted_count_limitation = row['submitted_count_limitation'][0:-1]
            done_count_limitation = row['done_count_limitation'][0:-1]

            order_base_segment = Order_Base_Segment.objects.filter(name = name)
            if len(order_base_segment) == 1 :
                order_base_segment = order_base_segment[0]
                order_base_segment.done_date_limitation = done_date_limitation
                order_base_segment.submitted_date_limitation = submitted_date_limitation
                order_base_segment.submitted_count_limitation = submitted_count_limitation
                order_base_segment.done_count_limitation = done_count_limitation
                order_base_segment.save()
            else:
                order_base_segment = Order_Base_Segment(
                    name = name,
                    done_date_limitation = done_date_limitation,
                    submitted_date_limitation = submitted_date_limitation,
                    submitted_count_limitation = submitted_count_limitation,
                    done_count_limitation = done_count_limitation
                )
                order_base_segment.save()

    def read_info(self):

        try:
            
            info = []
            for SheetName in self.SheetsName:
                info.append(self.read_google_shit(self.GoogleSheetID,SheetName))
                print('- {} sheet has been read'.format(SheetName))

            self.segment_info = info[0]
            
        except Exception as e:
            print(e)
            file_path = self.create_file_path()
            self.segment_info = pd.read_excel(file_path,sheet_name='segment')
            self.segment_info = self.segment_info.fillna(method='ffill')

    def read_google_shit(self,GoogleSheetID,SheetName):
        
        URL = 'https://docs.google.com/spreadsheets/d/{0}/gviz/tq?tqx=out:csv&sheet={1}'.format(
            GoogleSheetID,
            SheetName
        )
        df = pd.read_csv(URL)
        df = df.fillna(method='ffill')
        # print(df)
        return (df)
    
    def create_file_path(self):
        
        file_path = os.path.join(os.path.dirname(__file__), 'files')
        file_path = os.path.join(file_path, self.file_name)

        return file_path

class RFMModelQueryManager(Query):
    pass

class UserModelManager:
    
    def __init__(self):

        self.bulk_size = 1000
        self.query = RFMModelQueryManager(bulk_size = self.bulk_size
            ,app_name = 'UserModelManager'
            ,app_path = os.path.dirname(__file__))

    def update_new_customers_rfm_info_with_time_limit(self,time_limit=30):
        new_customers_mobile_list = self.write_new_customer_rfm_info_with_time_limit(time_limit=time_limit)
        self.update_customers_rfm_info(target_customers_mobile_list = new_customers_mobile_list)

    def write_new_customer_rfm_info_with_time_limit(self,time_limit=30):

        customers_mobile_list = read_customers_list_with_time_limit(
            time_limit = time_limit,
            time_limit_on='first_login'
            )
        
        old_customers_mobile_list = list(Customer_RFM_Information.objects.filter(mobile__in = customers_mobile_list).\
            values_list('mobile',flat=True))

        new_customers_mobile_list = list(set(customers_mobile_list) - set(old_customers_mobile_list))
        new_cris = []
        for customer_mobile in new_customers_mobile_list:
            
            cri = Customer_RFM_Information(
                mobile = customer_mobile,
                order_base_segment_id = 'segment1'
                )

            new_cris.append(cri)

        if len(new_cris) != 0:
            Customer_RFM_Information.objects.bulk_create(new_cris)
        print("- {} customers have been added to RFMModel".format(len(new_cris)))
        
        return(new_customers_mobile_list)
    
    def update_customers_rfm_info(self,target_customers_mobile_list):

        if type(target_customers_mobile_list) is list and len(target_customers_mobile_list) == 0 :
            print('- {} users in RFMModel have been updated.'.format(0))
            return None
            
        query_file_name = 'Target_CRI.sql'
        query_modifying_text_args = {'_mobiles_list' : '', '_submitted_date_limitation' : '6 months'}

        self.query.read_file(query_file_name=query_file_name)
        self.query.create_connection(
            product_name = 'ostadkar',
            connection_type = '',
            db_name = 'sale')

        mobiles_number_str = "','".join(target_customers_mobile_list)
        
        query_modifying_text_args['_mobiles_list'] = mobiles_number_str
        data = self.query.read_data(arguments = query_modifying_text_args)

        updated_CRIs = []
        CSHs = []

        for i,row in data.iterrows():

            mobile = row['mobile']
            order_base_segment_id = identify_segment(
                submitted_count_str = row['submitted_count'],
                done_count_str = row['done_count'])
            if order_base_segment_id == None:
                print('- Customer with mobile_number {} do not have segment.'.format(mobile))
            try:
                CRI = Customer_RFM_Information(**row,order_base_segment_id = order_base_segment_id)

                updated_CRIs.append(CRI)
                
                check = check_customer_segment_history(
                    mobile = mobile, 
                    segment_type = 'order_base_segment', 
                    current_segment_id = order_base_segment_id
                    )
                
                if check:
                    CSH = Customer_Segment_History(
                        mobile = mobile,
                        order_base_segment_name = CRI.order_base_segment_id
                    )
                    CSHs.append(CSH)
            except Exception as e:
                print(e)
                pass
                        

        Customer_RFM_Information.objects.bulk_update(
            updated_CRIs,
            ['order_base_segment_id'
            ,'submitted_count'
            ,'done_count'
            ,'recency_time'
            ,'engaged_time'
            ,'retention_time'
            ,'age_time','criterion']
            )
        Customer_Segment_History.objects.bulk_create(CSHs)

        print('- {} users in RFMModel have been updated.'.format(len(updated_CRIs)))
        print('- {} users in RFMModel have changed. (segment updating)'.format(len(CSHs)))
        
    def rewrite_customers_rfm_info(self):
        # Customer_Segment_History.objects.all().delete()
        Customer_RFM_Information.objects.all().delete()
        print('Customer_RFM_Information have been cleaned.')

        query_file_name = 'all_CRI.sql'
        query_modifying_text_args = {'_submitted_date_limitation' : '6 months'}
        self.query.read_file(query_file_name=query_file_name)
        self.query.create_connection(
            product_name = 'ostadkar',
            connection_type = '',
            db_name = 'sale')
        
        data = self.query.read_data(arguments = query_modifying_text_args)

        customers_mobile_list = list(Customer_Identification_Information.objects.\
            filter(mobile__in = data['mobile']).values_list('mobile',flat=True))
        
        data = data[data['mobile'].isin(customers_mobile_list)]

        obss = Order_Base_Segment.objects.all().order_by('name')
        for obs in obss:
            target_customers = classifying_customers(segment = obs,data = data)
            cris_list = []
            cshs_list = []
            cris_counter = 0
            changed_cris_counter = 0
            for i,row in target_customers.iterrows():

                try:
                    cri = Customer_RFM_Information(**row,order_base_segment_id = obs)
                    
                    mobile = row['mobile']
                    check = check_customer_segment_history(
                        mobile = mobile, 
                        segment_type = 'order_base_segment', 
                        current_segment_id = obs.pk
                        )
                    
                    if check:
                        csh = Customer_Segment_History(
                            mobile = mobile,
                            order_base_segment_name = obs.name
                        )
                        cshs_list.append(csh)
                        changed_cris_counter += 1

                except Exception as e:
                    print(e)
                else:
                    cris_list.append(cri)
                    cris_counter += 1

                if cris_counter % self.bulk_size == 0 :
                    Customer_RFM_Information.objects.bulk_create(cris_list)
                    cris_list = []
                    Customer_Segment_History.objects.bulk_create(cshs_list)

                    print(f'- {self.bulk_size} ({cris_counter}) users have been added to {obs.name} in RFMModel.')
                    print(f'- {len(cshs_list)} ({changed_cris_counter}) users in RFMModel have changed. (segment updating)')
                    cshs_list = []

            Customer_RFM_Information.objects.bulk_create(cris_list)
            Customer_Segment_History.objects.bulk_create(cshs_list)

            print(f'- {len(cris_list)} ({cris_counter}) users have been added to {obs.name} in RFMModel.')
            print(f'- {len(cshs_list)} ({changed_cris_counter}) users in RFMModel have changed. (segment updating)')

        date_limit = DT.datetime.now() - DT.timedelta(minutes=6*30*24*60)
        q1 = Q(last_login__gte = date_limit)
        q2 = Q(customer__in = Customer_Identification_Information.objects.\
            filter(mobile__in = data['mobile']))
        customer_mobile = list(Customer_Login_Information.objects.\
            filter(q1).exclude(q2).values_list('customer_id',flat = True))
        
        customers_mobile_lists = dividing_list(
            customer_mobile,
            self.bulk_size
            )
            
        count = 0
        for customers_mobile_list in customers_mobile_lists:
            CRIs = []
            for customer_mobile in customers_mobile_list:

                CRI = Customer_RFM_Information(
                    mobile = customer_mobile,
                    order_base_segment_id = 'segment1')
                CRIs.append(CRI)

            Customer_RFM_Information.objects.bulk_create(CRIs)
            count = count + len(CRIs)
            print("- {} ({}) customers have been added to segment1 in RFMModel".format(len(CRIs),count))
        